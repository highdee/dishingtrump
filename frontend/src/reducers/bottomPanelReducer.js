const init={
    contents:[],
    contentType:{
        videos:false,
        images:false,
        articles:false,
        documents:false
    },
    video:{title:'',rates:[],comments:[],currentUserRate:[false,false,false,false,false]},
    images:{title:'',rates:[]},
    article:{title:'',rates:[],comments:[],currentUserRate:[false,false,false,false,false]},
    document:{title:'',}
}

const BottomPanelReducer=(state=init,action)=>{
    switch (action.type) {
        case 'set_image_rate':
            return{
                ...state,
                images: {
                    ...state.images,
                    rates:[...state.images.rates , action.rate]
                }
        }

        case 'set_video_user_rate':
            if(!state.video.rates.find(rt=>rt.user == action.user)){
                state.video.rates.push({user:action.user,rate:0});
            }
            return{
                ...state,
                video: {
                    ...state.video,
                    rates:[
                        ...state.video.rates.map((rt)=>{
                            if(rt.user == action.user){
                                rt.rate=action.data;
                            }
                            return rt;
                        })
                    ]
                }
        }
        case 'set_article_user_rate':
            if(!state.article.rates.find(rt=>rt.user == action.user)){
                state.article.rates.push({user:action.user,rate:0});
            }
            return{
                ...state,
                article: {
                    ...state.article,
                    rates:[
                        ...state.article.rates.map((rt)=>{
                            if(rt.user == action.user){
                                rt.rate=action.data;
                            }
                            return rt;
                        })
                    ]
                }
            }
        case 'set_document':
            return{
                ...state,
                document: {
                    ...state.document,
                    ...action.document
                }
            }
        case 'set_article':
            return{
                ...state,
                article: {
                    ...state.article,
                    ...action.article
                }
        }
        case 'set_video':
            return{
                ...state,
                video: {
                    ...state.video,
                    ...action.video
                }
        }
        case 'set_image':
            return{
                ...state,
                images:action.images
            }
        case 'set_content_type':
            return{
                ...state,
                contentType:{
                    videos:false,
                    images:false,
                    articles:false,
                    documents:false,
                    [action.field]:action.value
                }
        }
        case 'set_content':
            return{
                ...state,
                contents: action.content
        }
        default: return state
    }
}

export default  BottomPanelReducer;