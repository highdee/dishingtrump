const initState={
    endpoint:'http://192.168.43.153:2000/',
    isTourActive:false,
    introvideoplaying:false
}

const GlobalReducer =(state=initState, actions)=>{
    switch(actions.type){
        case 'stop_intro_video':
            return{
                ...state,
                introvideoplaying:false
            }
        case 'endTour':
            return{
                ...state,
                isTourActive: false
            }
        case 'startTour':
            return{
                ...state,
                isTourActive: true
            }
            break;
    }
    return state;
}



export default GlobalReducer;