const init={
    rowbotton2:[],
    loaded:false
}

const bottomButtomReducer=(state=init,action)=>{
    switch (action.type) {
        case 'set_rowbuttons':
            return{
                ...state,
                rowbotton2: action.buttons,
                loaded:true
            }
    }
    return state;
}

export default bottomButtomReducer;