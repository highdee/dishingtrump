import { combineReducers } from 'redux';
import System_ui from './system_ui_reducer';
import leftpanelReducer  from './leftpanelReducer';
import GlobalReducer from './globalReducer';
import robotReducer from './robotReducer';
import bottomButtomReducer from "./bottomButtomReducer";
import BottomPanelReducer from "./bottomPanelReducer";
import OverlayerReducer from "./overlayersReducer";

const rootReducer=combineReducers({
    System_ui:System_ui,
    leftpanelReducer:leftpanelReducer,
    GlobalReducer:GlobalReducer,
    robotReducer:robotReducer,
    bottomButtomReducer:bottomButtomReducer,
    BottomPanelReducer:BottomPanelReducer,
    OverlayerReducer:OverlayerReducer
});


export default rootReducer;