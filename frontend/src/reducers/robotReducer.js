const init={
    chats:[

        {
            id:1,
            actions:[

                {
                    type:'text_voice',
                    text:"Welcome to Dishingtrump , my name is fritz. whats yours?.",
                },
                {
                    type:'ua',
                    ua:4,
                    text:"Welcome to Dishingtrump , my name is fritz. whats yours?.",
                }
            ],
            next:2
        },
        {
            id:2,
             actions:[
                {
                    type:'text_voice',
                    text:'Are you 18 years of age or older?.'
                },
                {
                    type:'ua',
                    ua:3,
                    decisions:{
                        YES:3,
                        NO:3
                    },
                    text:'Are you 18 years of age or older?.'
                }
             ],
             next:3
         },
         {
            id:3,
             actions:[
                {
                    type:'text_voice',
                    text:'Do you like Donald Trump?.'
                },
                {
                    type:'ua',
                    ua:3,
                    decisions:{
                        YES:4,
                        NO:5
                    },
                    text:'Do you like Donald Trump?.'
                }
             ],
             next:4
         },
         {
            id:4,
            actions:[
                {
                    type:'video',
                    video:'videotest.mp4'
                },
                {
                    type:'text_voice',
                    text:'Sorry but this isn’t some donald trump fan club website.  We don’t like Trump around here.  But here’s a consolation prize:'
                },
                {
                    type:'image',
                    image:'1008.gif',
                    time:5500
                },
                {
                    type:'text_voice',
                    text:'Till you flip, see you later.'
                },
            ],
            next:10
         } ,
         {
             id:5,
             actions:[
                 {
                     type:"text_voice",
                     text:"[|name|] , that’s great.  I don’t like cheezdoodle either."
                 },
                 {
                     type:'image',
                     image:'cheezdoodle.jpg',
                     time:2000
                 },
                 {
                     type:'text_voice',
                     text:"in fact, allow me to demonstrate how you can use this Website to learn all about cheezdoodle, aka ‘Donnytinyhands’."
                 },
                 {
                    type:'image',
                    image:'donnytinyhands.png',
                    time:2000
                },
                {
                    type:'text_voice',
                    text:"First, check out the mueller team button below.  Let’s click on it to see what happens."
                }
             ],
             next:6
         },
        {
            id:6,
            actions:[

                {
                    type:'ua',
                    ua:'5',
                    button:"5caa74788a8b07188400e024",
                    ui:'base'
                },
                {
                    type:'text_voice',
                    text:'As you can see, this causes the Trump matrix panel to appear. Currently, it features 9 of the 23 prosecutors who have at one time or another been involved in the investigation concerning cheezdoodle and russian collusion.  To view the remaining prosecutors, use the scroll down button.',
                    // text:'As you can see.',
                },
                {
                    type:'ua',
                    ua:'6',
                    auto:'lp_scroll'
                },
                {
                    type:'text_voice',
                    text:'Let’s click on Robert Mueller.'
                },
                {
                    type:'ua',
                    ua:'5',
                    button:"5caafa2c8418e82222c639f9",
                    ui:'lp'
                },
                {
                    type:'video',
                    video:'arrested.mp4'
                },
                {
                    type: 'text_voice',
                    text:'As you can see, clicking on Robert Mueller caused an intro video to play.  Even better, videos, articles, documents and photos related to Robert Mueller are now accessible.',
                    // text: 'As you can see, clicking on Robert .'
                },
                {
                    type:'text_voice',
                    text:'For example, let’s click on videos.',
                },
                {
                    type:'si',
                    si:'content_buttom',
                },
                {
                    type:'si',
                    si:'click_button',
                    button:'content_video'
                },
                {
                    type:'text_voice',
                    text:'This causes an overlay to appear which houses Robert Mueller related headlines.  If one is clicked on, that causes a video page to appear.'
                    // text:'This causes an overlay to appear which houses Robert Mueller.'
                },
                {
                    type:'route',
                    route:'video',
                    content:"video",
                    id:'Q248WH'
                },
                {
                    type:'text_voice',
                    text:'Featured on the video page will be the video clicked on from the overlay, as well as a video description, transcript, comments and related videos.  Currently, the matrix contains approximately 2000 Buttons. Through the use of automation , content is updated regularly.  But [|name|] if you’re more interested in what’s happening in the news now, check out the site’s right panel.',
                    // text:'Featured on the video page will be the video clicked on from the overlay .',
                },
                {
                    type:'route',
                    route:'/',
                    content:"page",
                },
                {
                    type:'si',
                    si:'lp'
                },
                {
                    type:'si',
                    si:'bp'
                },
                {
                    type:'si',
                    si:'rp'
                },
                {
                    type:'text_voice',
                    text:'Each day, content uploaded to the web within the last 24 hours, get added to this panel’s ‘Today’s New’s section.  This is where you’ll find the sort of up to date content that will make you an expert on our dolt 45.',
                    // text:'Each day, content uploaded to the web within the last 24 hours.'
                },
                {
                    type:'si',
                    si:'lp'
                },
                {
                    type:'text_voice',
                    text:'Which about sums up all that you can do with the free portion of this site.  Click on join to learn the benefits of becoming a member.  But until then, just start clicking. [username] trust me, it’s worth it.',
                    // text:'Which about sums up all that you can do with the free portion of this site.'
                },
                {
                    type:'si',
                    si:'lp',
                    status:'close'
                },
                {
                    type:'si',
                    si:'rp',
                    status:'close'
                },
            ],
            next:10
        },
         {
             id:10,
             actions:[{type:'text_voice',text:''}],
             next:'end'
         }

    ],
    chatAttributes:{
        active:0,
        active_action:0,
        chat:{},
        action:{},
        user:{ name:'' , age:false}
    },
    userinputs:{
        textbox:'',voice:'',booly:''
    }
};


const robotReducer=(state=init,action)=>{
    switch(action.type){
        case 'set_chat':
            return {
                ...state,
                chatAttributes:{
                    ...state.chatAttributes,
                    active:action.chat_index,
                    active_action:action.active_action,
                    chat:action.chat,
                    action:action.chat_active_action
                }
            } 
        case 'next_action':
            return{
                ...state,
                chatAttributes:{
                    ...state.chatAttributes, 
                    active_action:action.active_action, 
                    action:action.chat_active_action
                }
            }
        case 'setUserTextInput':
            return{
                ...state,
                userinputs:{
                    ...state.userinputs,
                    textbox:action.data
                },
                chatAttributes:{
                    ...state.chatAttributes,
                    user:{
                        ...state.chatAttributes.user,
                        name:action.data
                    }
                }
            }
        case 'setUserBoolInput':
            return{
                ...state,
                userinputs:{
                    ...state.userinputs,
                    booly:action.data
                },
                chatAttributes:{
                    ...state.chatAttributes,
                    user:{
                        ...state.chatAttributes.user,
                        age:action.data
                    }
                }
            }
        default :
    }
    return state;
}

export default robotReducer;