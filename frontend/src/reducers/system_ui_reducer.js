const init={
    leftPanel:{
        show:false
    },
    rightPanel:{
        show:false
    },
    bottomPanel:{
        show:false
    },
    popup:{
        show:false,
        message:''
    }
}

const System_ui = (state=init,action)=>{
     
    switch(action.type){
        case 'toggleleftPanel':
                return{
                    ...state,
                    rightPanel:{
                        ...state.rightPanel,
                        show:window.screen.width <= 559? false:state.rightPanel.show
                    },
                    leftPanel:{
                        ...state.leftPanel,
                        show:action.status != undefined ? action.status : !state.leftPanel.show
                    },
                   
                };
        case "toggleRightPanel":
                return{
                    ...state,
                    leftPanel:{
                        ...state.leftPanel,
                        show:window.screen.width <= 559? false:state.leftPanel.show
                    },
                    rightPanel:{
                        ...state.rightPanel,
                        show:!state.rightPanel.show
                    }
                } 
        case "toggleBottomPanel":
                return{
                    ...state,
                    bottomPanel:{
                        ...state.bottomPanel,
                        show:action.data
                    }
                }
        case "popup":
                return{
                    ...state,
                    popup:{
                        ...state.popup,
                        show:!state.popup.show,
                        message:action.message
                    }
                } 
         default:
            return state;        
         
    }
    return state;
}

export default System_ui;