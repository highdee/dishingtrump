const init={
    videoLayer:{
        show:false,
        data:{}
    },
    imageLayer:{
        show:false,
        data:{}
    }
}

const OverlayerReduer=(state=init , action)=>{
    switch (action.type) {
        case 'set_layer_video' :
            return {
                ...state,
                videoLayer: {
                    ...state.videoLayer,
                    data:action.data
                }
            }
        case 'set_layer_image' :
            return {
                ...state,
                imageLayer: {
                    ...state.imageLayer,
                    data:action.data
                }
            }
        case 'unset_videoLayer' :
            return {
                ...state,
                videoLayer: {
                    ...state.videoLayer,
                    show:false,
                    data:{}
                }
            }
        case 'unset_imageLayer' :
            return {
                ...state,
                imageLayer: {
                    show:false,
                    data:{}
                }
            }
        case 'set_videoLayer' :
            return {
                ...state,
                videoLayer: {
                    ...state.videoLayer,
                    show: action.status
                }
            }
        case 'set_imageLayer' :
            return {
                ...state,
                imageLayer: {
                    ...state.imageLayer,
                    show:action.status
                }
        }
        default : return state;
    }

}

export  default  OverlayerReduer;