const initState={
    mainButtons:[],
    loaded:false,
    currentButton:{
        video_status:false,
        article_status:false,
        image_status:false,
        document_status:false,
        // code:'',
        // filename:'',
        // url:''
        images:[],
        articles:[],
        id:0
    },
    mainButtonBackup:[],
    history:{
        // links:[],
        data:[],
        mainbuttons:[],
        mainbuttonHistory:[]
        // linknames:[]
    }
};


const leftpanelReducer=(state=initState,action)=>{ 
    switch(action.type){
        case 'set_btns':
            return {
                ...state,
                mainButtons:action.data,
                loaded:action.loaded ? action.loaded : action.data.length == 0 ? false :true
            }
        case 'set_backup':
            return {
                ...state,
                mainButtonBackup:action.data
            }
        case 'set_current_btn':
            console.log(action);
            return {
                ...state,
                currentButton:{
                    // ...state.currentButton,
                    images:[],
                    articles:[],
                    video_status:false,
                    article_status:false,
                    image_status:false,
                    document_status:false,
                    ...action.data
                }
            }
        case 'set_current_btn_history':
            return {
                ...state,
                mainbuttonHistory:{
                    ...state.mainbuttonHistory,
                    ...action.data
                }
            }
        case 'push_history':
            return{
                ...state,
                history:{
                    ...state.history,
                    // links:[...state.history.links,action.data.link],
                    data:[...state.history.data,action.data],
                    // linknames:[...state.history.linknames , action.data.name]
                },

            }
        case 'empty_history':
            return{
                ...state,
                history:{
                    ...state.history,
                    // links:[...state.history.links,action.data.link],
                    data:[],
                    mainButtons:[],
                    mainbuttonHistory:[]
                    // linknames:[...state.history.linknames , action.data.name]
                },

            }
        case 'push_mainbutton_history':
            return {
                ...state,
                history: {
                    ...state.history,
                    mainbuttons: [
                        ...state.history.mainbuttons, action.mainbutton
                    ]
                },

            }
        case 'push_mainbuttonhistory_history':
            return{
                ...state,
                history:{
                    ...state.history,
                    mainbuttonHistory:[
                        ...state.history.mainbuttonHistory , action.mainbutton
                    ]
                },

            }
    }
    return state;
}

export default leftpanelReducer;