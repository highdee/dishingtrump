import axios from 'axios'; 



const toDataURL = url => fetch(url)
    .then(response => response.blob())
    .then(blob => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.onerror = reject
    reader.readAsDataURL(blob)
}))
 

const getButton=()=>{
    return (dispatch,getstate)=>{ 
        axios.get(getstate().GlobalReducer.endpoint+'leftButtons').then(data=> {

            let buttons = processButton(data.data, getstate().GlobalReducer);
            if(getstate().leftpanelReducer.mainButtons.length == 0){
                dispatch({type: 'set_btns', data: buttons});
            }
            dispatch({type:'set_backup' , data:buttons});

            }).catch(error=>{
                console.log(error); 
            });
    } 
}


const setButton=(id,name)=>{ 
    return (dispatch,getstate)=>{ 
           
        //CHECKING IF THE BUTTON HAS CHILDREN
            let buttons=getstate().leftpanelReducer.mainButtons;
            let button={};
            buttons.forEach(item=>{ 
                if(item.id == id){
                    button=item;
                    return false;
                }
            });

            dispatch({type:'toggleBottomPanel',data:false});
            // console.log(button);

            // return false;
            if(!button.hasChildren){
                dispatch({type:'set_btns' ,data:getstate().leftpanelReducer.mainButtons});
                dispatch({type:'set_current_btn' , data:button});
                return false;
            }
           
        // PUSHING BUTTONS TP HISTORY
            dispatch({type:'push_history' , data:getstate().leftpanelReducer.mainButtons,mainbutton:button});

            if(getstate().leftpanelReducer.history.mainbuttonHistory.length > 0) {
                let lastClickedBtn = getstate().leftpanelReducer.history.mainbuttonHistory[getstate().leftpanelReducer.history.mainbuttonHistory.length - 1];
                dispatch({type: 'push_mainbutton_history', mainbutton: lastClickedBtn});
            }

            dispatch({type: 'push_mainbuttonhistory_history', mainbutton: button});


        //  EMPTY THE LEFT PANEL TO RECEIVE NEW BUTTONS
            dispatch({type:'set_btns' ,data:[]});




            let b4_no_of_history=getstate().leftpanelReducer.history.data.length;
            button=processContent(button,getstate);

            dispatch({type:'set_current_btn' , data:button});

        //  FETCH NEW BUTTONS
            axios.get(getstate().GlobalReducer.endpoint+'leftButtons/get-button/'+id)
            .then(data=>{ 
                
            let af_no_of_history=getstate().leftpanelReducer.history.data.length;
            
            //  CHECKING IF REQUEST FOR SUB BUTTONS HAS BEEN CANCELED 
            if(af_no_of_history == b4_no_of_history){

                let buttons=processButton(data.data,getstate().GlobalReducer);
                // console.log(buttons);
                dispatch({type:'set_btns' , data:buttons});
            }
 

            }).catch();

    }
}

const popHistory =()=>{
    return (dispatch,getstate)=>{
        let data=getstate().leftpanelReducer.history; 
        let buttons=data.data.pop();
        let button=data.mainbuttons.pop();
        button=button ? button:{};
        let buttonhistory=data.mainbuttonHistory.pop();


        dispatch({type:"set_btns",data:buttons});
        dispatch({type:'set_current_btn' , data:button});
        dispatch({type:'set_current_btn_history' , data:buttonhistory});

        // console.log(button);
        // console.log(data.mainbuttons);
    }
}

const processButton = (button , state)=>{
    // console.log(button);

        const r_data=button.map((element,index) => {

            return {
                name:element.title,
                key:index,
                id:element._id,
                icon:state.endpoint+'/'+element.icon,
                data:element.data,
                hasChildren:element['data']? element['data'].length == 0 ? false : true :false,
                videos:element.videos,
                images:element.images,
                articles:element.articles,
                documents:element.documents,
                video_status:element.videos.length !=0 ? true:false,
                image_status:element.images.length !=0 ? true:false,
                article_status:element.articles.length !=0 ? true:false,
                document_status:element.documents.length !=0 ? true:false,

            }
        });

    // console.log(r_data);
        return r_data;
}

const loadButtonContent=(content)=>{
    return (dispatch,getstate)=>{
        console.log(content);
        dispatch({'type':'toggleBottomPanel',data:true});
        dispatch({'type':'set_content',content:content});
    }
}

const viewVideo=(video)=>{
    return (dispatch,getstate)=>{
        // console.log(video);
        dispatch({type:'set_video',video});
    }
}

const getContent=(id,contentId)=>{
    return (dispatch,getstate)=>{

        // let video=getstate().
        axios.get(getstate().GlobalReducer.endpoint+'leftButtons/get-sub-button/'+id)
            .then(data=>{
                console.log(data);
                data.data=processContent(data.data,getstate);
                let currentvideo=data.data.videos.find(vid=>{
                    return vid.id == contentId;
                });
                if(currentvideo){
                    //will only work on videopage
                    currentvideo.rates=currentvideo.rates ? currentvideo.rates:[];
                }


                let currentimage=data.data.images.find(vid=>{
                    return vid.id == contentId;
                });

                let currentarticles=data.data.articles.find(art=>{
                    return art.id == contentId;
                });

                let currentdoc=data.data.documents.find(doc=>{
                    return doc.id == contentId;
                });


                dispatch({type:'set_video',video:currentvideo});
                dispatch({type:'set_image',images:currentimage});
                dispatch({type:'set_article',article:currentarticles});
                dispatch({type:'set_document',document:currentdoc});

                dispatch({type:'set_current_btn',data:processButton([data.data],getstate)[0]});
            }).catch(error=>{
                console.log(error);
        });

        // dispatch({type:'set_video',video});
    }
}

const processContent=(data , getstate)=>{
    data.images=data.images.map((dt)=>{
        if(dt.filename !=''){
            dt.filename=getstate().GlobalReducer.endpoint+'button_images/'+dt.filename;
        }

        return dt;
    });

    data.articles=data.articles.map((dt)=>{
        if(dt.filename !=''){
            dt.filename=getstate().GlobalReducer.endpoint+'button_articleimages/'+dt.filename;
        }

        return dt;
    });


    return data
}

const commentVideo=(button,video,payload)=>{
    return (dispatch,getstate)=>{

        // let video=getstate().
        axios.post(getstate().GlobalReducer.endpoint+'leftButtons/video-comment/'+button+'/'+video , payload)
            .then(data=>{
                // console.log(data);

            }).catch(error=>{
            console.log(error.request);
        });

        // dispatch({type:'set_video',video});
    }
}

const rateVideo=(button,video,payload)=>{
    return (dispatch,getstate)=>{

        // let video=getstate().
        axios.post(getstate().GlobalReducer.endpoint+'leftButtons/rate-video/'+button+'/'+video , payload)
            .then(data=>{
                // console.log(data);
            }).catch(error=>{
            console.log(error.request);
        });

        // dispatch({type:'set_video',video});
    }
}


const rateArticle=(button,article,payload)=>{
    return (dispatch,getstate)=>{

        // let video=getstate().
        axios.post(getstate().GlobalReducer.endpoint+'leftButtons/rate-article/'+button+'/'+article , payload)
            .then(data=>{
                console.log(data);
            }).catch(error=>{
            console.log(error.request);
        });

        // dispatch({type:'set_video',video});
    }
}


const viewAVideo=(button,video)=>{
    return (dispatch,getstate)=>{
            console.log(video);
        axios.get(getstate().GlobalReducer.endpoint+'leftButtons/view-video/'+button+'/'+video)
            .then(data=>{
                console.log(data);
            }).catch(error=>{
                console.log(error.request);
        });
    }
}



const rateImage=(button,image,payload)=>{
    return (dispatch,getstate)=>{

        // let video=getstate().
        axios.post(getstate().GlobalReducer.endpoint+'leftButtons/rate-image/'+button+'/'+image , payload)
            .then(data=>{
                console.log(data);
            }).catch(error=>{
            console.log(error.request);
        });

        // dispatch({type:'set_video',video});
    }
}

const setArticle=(id)=>{
    return (dispatch,getstate)=>{
        let currentarticles=getstate().leftpanelReducer.currentButton.articles.find((art)=>art.id == id);
        console.log(currentarticles);
        dispatch({type:'set_article',article:currentarticles});
    }
}

const getChatbuttons=()=>{
    return (dispatch, getstate)=>{
        axios.get(getstate().GlobalReducer.endpoint+'leftButtons/get-frontend-chat-buttons')
            .then(data=>{
                console.log(data.data);
                dispatch({type:'set_rowbuttons',buttons:data.data});
            }).catch(error=>{
               console.log(error.request);
        });
    }
}

const getchatSubbuttons=(btn)=>{
    // console.log(btn);
    return (dispatch , getstate)=>{
        let url=getstate().GlobalReducer.endpoint;
        let data=processButton(btn.data,getstate().GlobalReducer);
        // console.log(data);
        let videocontent=btn.code != '' ? btn.code : btn.video != '' ? url+btn.video :'';
        let imagecontent=btn.image != '' ? url+btn.image : btn.imageurl ;

        let videomobilecontent=btn.mobile.code != '' && btn.mobile.code != null ? btn.mobile.code : btn.mobile.video != '' && btn.mobile.video != null ? url+btn.mobile.video :'';
        let imagemobilecontent=btn.mobile.image != '' && btn.mobile.image != null ? url+btn.mobile.image : url+btn.mobile.imageurl ;

        // alert('not workin');

        //checking if user is using mobile
       if(!getstate().GlobalReducer.isTourActive){

           if ((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1) ){
               if(videomobilecontent != ''  || imagemobilecontent !=''){ //mobile has content if true

                   if(videomobilecontent != ''){
                       dispatch({type:'set_videoLayer',status:true});
                       dispatch({type:"set_layer_video" , data:btn.mobile});
                   }
                   else{
                       // console.log(imagemobilecontent);
                       dispatch({type:'set_imageLayer',status:true});
                       dispatch({type:"set_layer_image" , data:btn.mobile});

                   }
               }else{

                   if(videocontent){
                       dispatch({type:"set_layer_video" , data:btn});
                       dispatch({type:'set_videoLayer',status:true});
                   }else{
                       dispatch({type:'set_imageLayer',status:true});
                       dispatch({type:"set_layer_image" , data:btn});

                   }

               }
           }else{
               if(videocontent){
                   dispatch({type:"set_layer_video" , data:btn});
                   dispatch({type:'set_videoLayer',status:true});
               }
               else{
                   dispatch({type:'set_imageLayer',status:true});
                   dispatch({type:"set_layer_image" , data:btn});

               }
           }
       }

        dispatch({type:'toggleleftPanel',status:true});
        dispatch({type:'empty_history'}); // leftpanel history
        dispatch({type:'push_history',data:getstate().leftpanelReducer.mainButtons}); //making the first 9 buttons a history
        dispatch({type:'set_btns',data,loaded:true});
    }
}

let leftpanel={};
export default leftpanel={
    getButton,setButton,popHistory,loadButtonContent,viewVideo,
    getContent,commentVideo,rateVideo,viewAVideo,rateImage,rateArticle,
    setArticle , getChatbuttons , getchatSubbuttons
}