import axios from 'axios';
import leftPanel from "./leftpanelActions";
import { BrowserRouter } from 'react-router-dom';

const initialiseBot=()=>{
    return (dispatch,getstate)=>{
        let store=getstate().robotReducer;

        let chat=store.chats[0];
        let chat_active_action=chat.actions[0];

        dispatch({type:'set_chat',chat_index:0,active_action:0,chat:chat,chat_active_action});

    }
}

const chatTextFinished=()=>{

    return (dispatch,getstate)=>{ 
        let chat=getstate().robotReducer.chatAttributes;

        let active_action=chat.active_action;
        let active=chat.active;
        let actions=chat.chat.actions;
        let isLastAction=active_action >= (chat.chat.actions.length - 1) ? true:false;



        //IF THIS IS NOT THE LAST ACTION IN THIS CHAT THEN MOVE TO THE NEXT ACTION ELSE MOVE TO THE NEXT CHAT
        if( !isLastAction ){
            active_action+=1;
            let chat_active_action=chat.chat.actions[active_action];
            
            dispatch({type:'next_action',active_action:active_action,chat_active_action});
        }else{

            // /DECIDE THE NEXT CHAT
            let current_action=chat.chat.actions[chat.active_action];
            if(current_action.type == 'ua' && current_action.ua == 3){
                let ua=getstate().robotReducer.userinputs.booly;
                chat.chat.next=current_action.decisions[ua];
                // console.log(ua);
            }

            active=chat.chat.next; //THIS MOVES THE CHAT

            active_action=0;
            chat=getstate().robotReducer.chats.find(ct=> ct.id == active);

            if(chat.next=='end'){ //END OF TOUR
                dispatch({type:'endTour'});
                dispatch({type:'set_btns',data:getstate().leftpanelReducer.mainButtonBackup,loaded:true});
            }

            let chat_active_action=chat.actions[active_action];

            dispatch({type:'set_chat',chat_index:active,active_action:active_action,chat:chat,chat_active_action});

        }

        //PERFORMING AUTOMATIC ACTIONS IF AVAILABLE OR NEEDED e.g Bot clicking a button
        let chatatt=getstate().robotReducer.chatAttributes;
        let action=chatatt.action;

        console.log(getstate().robotReducer.chatAttributes);
        //THIS WILL CLICK ON VIDEO BUTTON ON THE LEFT PANEL
        if(action.type == 'si' && (action.si == 'content_buttom' || (action.si == 'click_button' && action.button=='content_video') )){
            setTimeout(()=>{
                dispatch(leftPanel.loadButtonContent(getstate().leftpanelReducer.currentButton.videos));
                dispatch({type:"set_content_type",field:'videos',value:true});
            } ,1000);
        }
        //THIS WILL PREPARE CONTENT BEFORE ROUTING TO THE VIDEO PAGE
        if(action.type == 'route' && (action.route == 'video')){
            let id=action.id;
            let videos=getstate().leftpanelReducer.currentButton.videos;
            let video=videos.find(btn => btn.id == id);
            dispatch({type:'set_video',video});
        }
    }
}

const setUserTextInput=(data)=>{
    return (dispatch,getstate)=>{ 
        console.log(data);
        dispatch({type:'setUserTextInput',data}); 

        dispatch(chatTextFinished());
    }
}

const setUserBoolInput=(data)=>{
    return (dispatch,getstate)=>{ 
        
        console.log(data); 
        dispatch({type:'setUserBoolInput',data}); 

        dispatch(chatTextFinished());
    }
}
let robotActions={};

export default robotActions={
    chatTextFinished,initialiseBot,setUserTextInput,setUserBoolInput
}