import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2';
import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";

import chat_ballon from '../../assets/images/Chat_Balloon.png';
import leftpanelActions from "../../actions/leftpanelActions";

class ViewDocument extends Component{

    render(){

        let button = this.props.button;
        let document=this.props.document;

        return(
            <div id="feeds" className="container-dt">
                <Navbar2 />
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 mb-5 inner">
                    <h5 className='p-3'>{document.title}</h5>
                    <div dangerouslySetInnerHTML={{__html: document.url}} className='col-12 document-display'>

                    </div>
                </div>
                <Foot />
            </div>
        )
    }

    componentDidMount() {
            // console.log(this.props.button);
            this.props.getContent(this.props.match.params.button,this.props.match.params.id);
    }
}

const mapStateToProps=(state)=>{
    return{
        button:state.leftpanelReducer.currentButton,
        document:state.BottomPanelReducer.document,
        mainButtons:state.leftpanelReducer.mainButtons,
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
        getContent:(id,document)=>{ dispatch(leftpanelActions.getContent(id,document))},
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ViewDocument);