import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2';
// import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";

import chat_ballon from '../../assets/images/Chat_Balloon.png';

class Membership extends Component{

    render(){   
        return(
            <div id="membership"> 
                <Navbar2 />
                    <div className="col-12 col-sm-10 col-md-8 col-lg-6 mb-5 inner"> 
                       <div className="col-12 p-0 m-0">
                            <div className="col-10 col-sm-6 col-md-6 box">
                            <h5>Membership</h5>
                                <div className="inna">  
                                    <div className="ang">
                                    <span>BASIC ACCOUNT</span>
                                    </div>
                                    <div className="bdy">
                                        <img alt="" src={chat_ballon} />
                                        <h4>FREE</h4>
                                    </div>
                                    <div className="footer">
                                        <h4>Includes</h4>
                                        <p>
                                            Access to PRIME-only content, an ad-free mobile mobile site and warm , fuzzy feelings
                                            from supporting independent journalism
                                        </p>
                                        
                                    </div>
                                </div>
                                <button className="btn primary chg">Change</button>
                            </div>
                            <div className="col-12 col-sm-6 form_mod box">
                                 <h5>Membership</h5>
                                 <form className="">
                                     <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">First name</label>
                                        <input type="text" className="form-control col-12" placeholder=""/>
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">Last name</label>
                                        <input type="text" className="form-control col-12" placeholder=""/>
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">email</label>
                                        <input type="text" className="form-control col-12" placeholder=""/>
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">Confirm email</label>
                                        <input type="text" className="form-control col-12" placeholder=""/>
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">Username</label>
                                        <input type="text" className="form-control col-12" placeholder=""/>
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">Password</label>
                                        <input type="password" className="form-control col-12" placeholder=""/>
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">Confirm password</label>
                                        <input type="password" className="form-control col-12" placeholder=""/>
                                    </div>

                                    <div className="form-group col-12 btm mt-4">
                                        <button type="button" className="btn primary">Create account</button>
                                    </div>
                                 </form>
                            </div>
                       </div>
                    </div> 
                <Foot />
            </div>
        )
    }


}

const mapStateToProps=(state)=>{
    return{
        
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
         
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Membership);