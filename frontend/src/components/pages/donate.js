import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2';
import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";
class donate extends Component{

    render(){   
        return(
            <div id="donate" className="container-dt"> 
                <Navbar2 />
                    <div className="donateInner col-12 col-sm-10 col-md-8 col-lg-6">
                        <h4 className="d_hed">Donate</h4> 
                        <div className="d_body">
                            <p>
                                If you would like to support of work please consider making a donation to THinkProgress <br />
                                Use our easy electronic payment from below , or if you prefer to write check , send to: THinkProgress, Attn Ed connor 1333 <br />
                                H St. NW, Suite 200 Washington , DC 2005 <br/>

                                if you have any problem with the form below, <a href="/#">Click here</a>. You can also email us at donate@thinkprogress.org for help
                            </p>
                            
                            <h4 className="d_hed mt-4">Donate Amount</h4>
                            <div className="btns">
                                <button className=" btn primary">$15</button>
                                <button className=" btn primary">$30</button>
                                <button className=" btn primary">$100</button>
                                <button className=" btn primary">$500</button>
                                <button className=" btn primary">$1,000</button>
                                <input type='text' className="form-control " placeholder='$0.00' />
                            </div>
                            <div className="mod2">
                                <div className="form-group">
                                    <input id="mtc" type="checkbox" className="form-check-input" />
                                    <label htmlFor="mtc" className="form-check-label">Make this contribution</label>

                                    <select className="form-control">
                                        <option>monthly</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <input id="hms" type="checkbox" className="form-check-input" />
                                    <label htmlFor="hms" className="form-check-label">
                                        i'd like to make this contribution in honour of memory of someone
                                    </label>
                                </div>
                            </div>

                            <h4 className="d_hed mt-4 mb-3">Your information</h4>
                            <div className="form_mod">
                                <form className="row">
                                    <div className="form-group col-12 col-sm-6">
                                        <label className="col-12">First name</label>
                                        <input type="text" className="form-control col-12" />
                                    </div>
                                    <div className="form-group col-12 col-sm-6">
                                        <label className="col-12">Last name</label>
                                        <input type="text" className="form-control col-12" />
                                    </div>

                                    <div className="form-group col-12 col-sm-6">
                                        <label className="col-12">Street address</label>
                                        <input type="text" className="form-control col-12" />
                                    </div>
                                    <div className="form-group col-12 col-sm-6">
                                        <label className="col-12">Address line2 <span>(optional)</span></label>
                                        <input type="text" className="form-control col-12" />
                                    </div>

                                    <div className="form-group col-12 col-sm-4">
                                        <label className="col-12">Country</label>
                                        <input type="text" className="form-control col-12" placeholde="united state"/>
                                    </div>
                                    <div className="form-group col-12 col-sm-4">
                                        <label className="col-12">Postal</label>
                                        <input type="text" className="form-control col-12" />
                                    </div>
                                    <div className="form-group col-12 col-sm-4">
                                        <label className="col-12">City</label>
                                        <input type="text" className="form-control col-12" />
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">State \ Province</label>
                                        <input type="text" className="form-control col-12" placeholder="- state -"/>
                                    </div>
                                    <div className="form-group col-12 col-sm-12">
                                        <label className="col-12">Email</label>
                                        <input type="text" className="form-control col-12" placeholder="email@email.com"/>
                                    </div>

                                    <h4 className="d_hed mt-4 mb-3 col-12">Payment information</h4>

                                    <h4 className="d_hed black mt-4 mb-3 col-12">Payment method :</h4>

                                    <div className="form-group col-12 col-sm-6">
                                        <label className="col-12">Card number</label>
                                        <input type="text" className="form-control col-12" placeholder="**** **** **** ****"/>
                                    </div>

                                    <div className="form-group col-12 col-sm-3">
                                        <label className="col-12">Expiry date</label>
                                        <input type="text" className="form-control col-12" placeholder="MM/YY"/>
                                    </div>

                                    <div className="form-group col-12 col-sm-3">
                                        <label className="col-12">Secure code</label>
                                        <input type="text" className="form-control col-12" placeholder="***"/>
                                    </div>
                                    <div className="form-group chk">
                                        <input id="hms" type="checkbox" className="form-check-input" />
                                        <label htmlFor="hms" className="form-check-label">
                                        Remember me so that i can you FacyAction next time
                                        </label>
                                    </div>

                                    <div className="form-group col-12 btm">
                                        <button type="button" onClick={()=>{this.props.popup('thanks message') } }
                                            className="btn primary">Donate
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                
                <PopUpMessage/>
                <Foot />
            </div>
        )
    }


}

const mapStateToProps=(state)=>{
    return{
        
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
        popup:(message)=>{ dispatch({type:'popup','message':message}); }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(donate);