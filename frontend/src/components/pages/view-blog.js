import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2' ;
import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";

import banner1 from '../../assets/images/728x90.png';
import banner2 from '../../assets/images/300x250.jpg';
import blogImage from '../../assets/images/blogimage.png';

import LeftPanel from '../layouts/leftpanel';
import RightPanel from '../layouts/rightpanel';
import leftpanelActions from "../../actions/leftpanelActions";
import Articleskeleton from "../layouts/articleskeleton";

class ViewBlog extends Component{

    state={
        months:['JAN','FEB','MAR','APR','MAY','JUNE','JULY','AUG','SEPT','OCT','NOV','DEC'],
        user_id: 32
    }
    render(){

        let article=this.props.article;
        let button=this.props.button;

        let date=new Date(article.date);

        let userRate=article.rates.find((rate)=> rate.user == this.state.user_id );
        this.GetUserOverAllrating(userRate);

        let rates=article.rates.map((rate)=>{ return rate.rate ;});
        let overall_rating=rates.reduce((a,c)=> a + c ,0);
        overall_rating=overall_rating / rates.length ;
        let  overAllRates=this.OverAllrateVideo(overall_rating);

        // console.log(overAllRates);
        let one=article.rates.filter((rate)=> rate.rate == 1 );
        let two=article.rates.filter((rate)=> rate.rate == 2 );
        let three=article.rates.filter((rate)=> rate.rate == 3 );
        let four=article.rates.filter((rate)=> rate.rate == 4 );

      let related=this.getRelated();

        return(

            <div id="blog">
                        <Navbar2 />
                        {/*<LeftPanel />*/}
                        {/*<RightPanel />*/}
                        {/* <BottomPanel /> */}
                <div className="col-12 col-sm-10 col-md-10 col-lg-7 mb-5 inna">
                    {
                        !article.id ?
                            <Articleskeleton/>
                            :

                                <div className="row">
                                    <div className="col-12 col-12 col-md-9 lists">
                                        <img alt="" className="ad_banner d-none d-sm-block" src={banner1}/>

                                        <div className='blogDetails'>
                                            <h3>{article.title}</h3>
                                            <div className='headStats'>
                                                <b>{article.date ? date.getDay() + ' ' + this.state.months[date.getMonth()] + '. ' + date.getHours() + ':' + date.getMinutes() + ' ' + (date.getHours() > 12 ? 'PM' : 'AM') :''}</b>
                                                <b className='votes'>
                                            <span>
                                                  {article.rates.length} votes  &nbsp;
                                            </span>

                                                    <i className={overAllRates[0] ? 'fas fa-star':'far fa-star'}></i>
                                                    <i className={overAllRates[1] ? 'fas fa-star':'far fa-star'}></i>
                                                    <i className={overAllRates[2] ? 'fas fa-star':'far fa-star'}></i>
                                                    <i className={overAllRates[3] ? 'fas fa-star':'far fa-star'}></i>
                                                </b>
                                            </div>

                                            <div style={{backgroundImage: "url('" + article.filename + "')"}}
                                                 className='headImage'></div>
                                            <div className='articleParagraph'>
                                                <p>
                                                    {article.paragraph}
                                                </p>
                                            </div>
                                            <div className='bottomStats row'>
                                                <div className='col-sm-12 col-md-4 stats1'>
                                                    <span>{article.comments.length} comments</span> | <span>{article.rates.length} votes</span>
                                                </div>

                                                <div className='col-sm-12 col-md-8 stats2'>
                                            <span>
                                                {four.length} &nbsp;
                                                <i className='far fa-star'></i><i className='far fa-star'></i>
                                                <i className='far fa-star'></i><i className='far fa-star'></i>
                                            </span>
                                            <span>
                                                {three.length} &nbsp;
                                                <i className='far fa-star'></i><i className='far fa-star'></i><i className='far fa-star'></i>
                                            </span>
                                            <span>
                                                {two.length} &nbsp;
                                                <i className='far fa-star'></i><i className='far fa-star'></i>
                                            </span>
                                            <span>
                                                {one.length} &nbsp;
                                                    <i className='far fa-star'></i>
                                            </span>

                                                </div>
                                            </div>
                                            <div className='notloggedin'>
                                                <p>Sign in to post message</p>
                                            </div>

                                            <div className='comments'>
                                                <ul>
                                                    <li>
                                                        <div className="imageHolder"></div>
                                                        <div className="commentinfo">
                                                            <span className="name">John jhein</span> <span
                                                            className="time">2 secs ago</span>
                                                            <div className="textComment">
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                Voluptas quaerat temporibus
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                Voluptas quaerat temporibus
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                Voluptas quaerat temporibus
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className='bottomButton mt-3 pr-3'>
                                                <button className='btn btn-dish'>show all</button>
                                            </div>
                                            <div id='' className='mt-5'>
                                                <div className='commentBox d-block'>
                                                    <textarea id='commentText' style={{width:'100%'}} rows='2' placeholder="Type your comment........"></textarea>
                                                    <div className='tools'>
                                                         <span className='float-left mt-2 ml-2'>
                                                             Rate &nbsp;
                                                             <i onClick={()=>{this.rateArticle(1)}} className={this.props.article.currentUserRate[0] ? 'fas fa-star':'far fa-star'}></i>
                                                             <i onClick={()=>{this.rateArticle(2)}} className={this.props.article.currentUserRate[1] ? 'fas fa-star':'far fa-star'}></i>
                                                             <i onClick={()=>{this.rateArticle(3)}} className={this.props.article.currentUserRate[2] ? 'fas fa-star':'far fa-star'}></i>
                                                             <i onClick={()=>{this.rateArticle(4)}} className={this.props.article.currentUserRate[3] ? 'fas fa-star':'far fa-star'}></i>
                                                             {/*<i onClick={()=>{this.rateArticle(5)}} className={this.props.article.currentUserRate[4] ? 'fas fa-star':'far fa-star'}></i>*/}
                                                         </span> &nbsp;
                                                        <button onClick={this.commentVideo}  className='btn primary '> &nbsp; <i className='fas fa-paper-plane'></i> &nbsp;Send &nbsp; </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="blogs">
                                                <div className="blg row col-12 p-0 m-0">

                                                    <div className="bl_img col-4 col-sm-2 col-md-2 col-lg-2 p-0">
                                                        <img alt="" className="blog_banner" src={blogImage}/>
                                                    </div>
                                                    <div className="blg_det col-8 col-md-10 col-lg-10 pr-0">
                                                        <h4 className="d-sm-block d-none">Trump say he plans to end
                                                            birthright citizenship to babies born to non-citizen:Report</h4>
                                                        <h4 className="d-sm-none d-block">Trump say he plans to </h4>
                                                        <p className="d-sm-block d-none">
                                                            President DOnald Trump said in an interview that he plans to use
                                                            an executive order to end citizenship rights for babies born in
                                                            the U.S to parents who aren't citizens.
                                                        </p>
                                                        <p className="d-sm-none d-block">
                                                            President DOnald Trump said in an interview that he plans to use
                                                            an executive order to end .
                                                        </p>
                                                        <div className="stats">
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                            <span>454 votes</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-12 col-md-3 list2">
                                        <img alt="" className="ad_banner  d-none d-sm-block" src={banner2}/>

                                        <div className="blg-right">
                                            {
                                               related.map((art,index)=>{
                                                  return (
                                                      <div className="blg2" key={index}>
                                                          <img alt="" className="ad_banner" src={blogImage}/>
                                                          <a href={"/view-article/"+button.id+'/'+art.id+'/'+art.title.replace(/ /g ,'-')}>
                                                              {art.title}
                                                          </a>
                                                      </div>
                                                  )
                                               })
                                            }
                                        </div>
                                    </div>
                                </div>

                    }
                </div>
                        <PopUpMessage/>
                        <Foot />
        </div>

        )
    }

    componentDidMount() {

        if(this.props.mainButtons.length != 0){
            let id=this.props.match.params.id;
            this.props.setArticle(id);
            // console.log(id);
            // console.log(this.props.article);
            console.log(this.props.button);

        }else{
            this.props.getContent(this.props.match.params.button , this.props.match.params.id);
        }
        // console.log(this.props.video);
    }

    GetUserOverAllrating=(rating)=>{
        if(!rating){
            return [false,false,false,false,false];
        }
        let star=rating.rate;
        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 4-star; i++) {
            empty.push(false);
        }

        let overAllRates=[false,false,false,false,false];
        overAllRates=[...marked,...empty];
        // console.log(overAllRates);
        this.props.article.currentUserRate=overAllRates;

        return overAllRates;
    }

    OverAllrateVideo=(star)=>{
        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 4-star; i++) {
            empty.push(false);
        }

        let overAllRates=[false,false,false,false,false];
        overAllRates=[...marked,...empty];

        return overAllRates;
    }

    rateArticle=(star)=>{
        let rates=this.props.article.rates;
        let userRate={};
        userRate=rates.find((rat)=> {
            return rat.user == this.state.user_id;
        } );
        if(userRate){
            userRate.rate=star;
            this.GetUserOverAllrating(userRate);
        }
        else{
            userRate={};
            userRate.rate=star;
            userRate.user=this.state.user_id;
            this.GetUserOverAllrating(userRate);
        }

        this.props.set_article_user_rate(this.state.user_id,star);


        let button=this.props.button;
        let article=this.props.article;

        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 4-star; i++) {
            empty.push(false);
        }

        this.props.rateArticle(button.id,article.id,{rate:star,user:this.state.user_id});
    }

    getRelated=()=>{
        let article=this.props.article;
        let buttton=this.props.button;

        let related=[];
        let currentArticleIndex=0;

        //GET THE INDEX OF CURRENT ARTICLE
        related=this.props.button.articles.filter((art,index)=>{
            if(art.id == article.id){
                currentArticleIndex=index;
            }
            return art.id != article.id;
        })
        //SELECT THE NEXT 4 ARTICLES AFTER THE CURRENT ARTICLE FROM THE ARTICLE ARRAYS
        related=this.props.button.articles.filter((art,index)=>{
            if(index > currentArticleIndex){
                return true;
            }
            return false;
        })

        //IF THE REMAINING ARTICLES IS NOT UP TO 4 THEN GO AND SELECT FROM THE BEGINING OF THE ARRAY
        if(related.length <4){
            let temp=this.props.button.articles.filter((art,index)=>{
                if(related.length >= 4){
                    return false;
                }
                if(index < currentArticleIndex){
                    return true;
                }
                return false;
            })
            related=[...related , ...temp];
        }

        return related;
    }
}

const mapStateToProps=(state)=>{
    return{
        button:state.leftpanelReducer.currentButton,
        article:state.BottomPanelReducer.article,
        mainButtons:state.leftpanelReducer.mainButtons,
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
        getContent:(id,video)=>{ dispatch(leftpanelActions.getContent(id,video))},
        rateArticle:(button,article,payload)=>{dispatch(leftpanelActions.rateArticle(button,article,payload))},
        set_article_user_rate:(user,data)=>{dispatch({type:'set_article_user_rate',user,data})},
        setArticle:(id)=>{dispatch(leftpanelActions.setArticle(id))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ViewBlog);