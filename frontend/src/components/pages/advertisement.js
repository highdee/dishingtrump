import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2';
import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";

import chat_ballon from '../../assets/images/Chat_Balloon.png';

class Advertisement extends Component{

    render(){   
        return(
            <div id="advertise" className="container-dt"> 
                <Navbar2 />
                    <div className="col-11 col-sm-10 col-md-8 col-lg-6 mb-5 inner"> 
                        <h5>ADVERTISE</h5>
                        <p>
                            advertise with ThinkProgress to reach an active , engaged audience that calues socially responsible companies and organizations, We offer a menu of platforms to gelp you get your cause in front of a very expansive shared-valu audience of reader
                        </p>

                        <h5>Advertise on our site</h5>
                        <p>
                              Our digital is broad and wide , with more than 8 million unique visitor  and more than 10 million each month. THinkProgress content reaches nearly 1.8million followers than 840,000 follower on Twitter,Weoffre ad banner in the following sizes: 300 x 250, 728 x 90 and 970 x 90
                        </p>
                        <h5>Advertise in our daily newsletter</h5>
                        <p>
                            Our list of more than 85,000 email list recipient shows incredible engagement with our daily newsletter send,The newsletter , which includes our tops stories and recent videos, consistently has an open rate more than 20%, We offer two ad positions in the followng sizes: 300 x 25- and 728 x 90
                        </p>
                        <h5>Send a dedicated email</h5>
                        <p>
                            Send out your email message to a list of more than 85,000 recipient. Our email subscribers value socially responsible messages and services . They are engaged and respond to advocacy messages.This is a great place to finsd that shared-value niche audience that understand and embraces the progressice agenda
                        </p>
                        <h5>Get in touch</h5>
                        <p>
                            Please contact Ed connors, ThinkProgress' Business and Operations Manage , to discuss your campaign goals
                        </p>
                    </div> 
                <Foot />
            </div>
        )
    }


}

const mapStateToProps=(state)=>{
    return{
        
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
         
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Advertisement);