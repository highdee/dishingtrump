import React, { Component } from 'react'
import Navbar2 from '../layouts/navbar2' ;
import { connect } from "react-redux";
import Foot from '../layouts/footer';

import blogImage from '../../assets/images/blogimage.png';
import leftpanelActions from "../../actions/leftpanelActions";
class MemoPage extends Component{

    state={
        videoContents:{
            details:false,
            transcripts:false,
            comments:true
        },
        viewImage:{
            show:false
        },
        rates:[false,false,false,false,false],
        user:3423
    }

    changeContent(which){
        this.setState({
                details:false,
                transcripts:false,
                comments:false,
                [which]:true
        }); 
    }

    render(){

        let button=this.props.button;
        let activeImage=this.props.image;
        console.log(activeImage);
        let actionImageUrl=activeImage.url != '' ? activeImage.url:activeImage.filename;

        let overAllRates=[false,false,false,false,false];
        let userHasRated=false;
        if(activeImage.rates) {
            let rates = activeImage.rates.map((rate) => {
                if(rate.user == this.state.user){
                    userHasRated=true;
                }
                return rate.rate;
            });
            let overall_rating = rates.reduce((a, c) => a + c, 0);
            overall_rating = overall_rating / rates.length;
             overAllRates = this.OverAllrateImage(overall_rating);
        }

        return(
            <div onClick={this.hideViewer} id="memo" className="container-dt">
                 <Navbar2 />
                 <div className="memoInner col-11 col-sm-11 col-md-11 col-lg-10">
                        <div className="row p-0 m-0">
                            <h4 className="hed">Memo Page</h4>
                            <div className="containerImage col-12">
                                {
                                    button.images.map((img)=>{
                                        let image=img.url != '' ? img.url:img.filename;
                                        let imgoverAllRates=[false,false,false,false,false];
                                        if(img.rates) {
                                            let imgrates = img.rates.map((rate) => {
                                                return rate.rate;
                                            });
                                            let imgoverall_rating = imgrates.reduce((a, c) => a + c, 0);
                                            imgoverall_rating = imgoverall_rating / imgrates.length;
                                            imgoverAllRates = this.OverAllrateImage(imgoverall_rating);
                                        }
                                        return (
                                            <div onClick={()=>{this.selectImage(img)}} className="imageContainer" style={{backgroundImage:"url('"+ image +"')"}}>

                                                <div className="imagedetails">
                                                    <span className="name">{img.title}</span>
                                                        <span className="stars">
                                                           <i className={imgoverAllRates[0] ? 'fas fa-star':'far fa-star'}></i>
                                                            <i className={imgoverAllRates[1] ? 'fas fa-star':'far fa-star'}></i>
                                                            <i className={imgoverAllRates[2] ? 'fas fa-star':'far fa-star'}></i>
                                                            <i className={imgoverAllRates[3] ? 'fas fa-star':'far fa-star'}></i>
                                                            <i className={imgoverAllRates[4] ? 'fas fa-star':'far fa-star'}></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        )
                                    })
                                }

                            </div>
                        </div> 

                        <div onClick={(e)=>{e.stopPropagation()}} style={{backgroundImage:"url('"+ actionImageUrl +"')"}} className={this.state.viewImage.show ? "imageviewer ":"imageviewer hide"}>
                            <div className="imagedet">
                                <span>
                                    {activeImage.rates.length} votes
                                   <i className={overAllRates[0] ? 'fas fa-star':'far fa-star'}></i>
                                    <i className={overAllRates[1] ? 'fas fa-star':'far fa-star'}></i>
                                    <i className={overAllRates[2] ? 'fas fa-star':'far fa-star'}></i>
                                    <i className={overAllRates[3] ? 'fas fa-star':'far fa-star'}></i>
                                    <i className={overAllRates[4] ? 'fas fa-star':'far fa-star'}></i>
                                </span>
                            </div>
                            <div onClick={(e)=>{e.stopPropagation()}} className={userHasRated ? 'rateImage hide':'rateImage'}>
                                <div className='contain'>
                                    <h3>RATE</h3>
                                    <i onClick={()=>{this.rateImage(1)}} onMouseOut={()=>{this.highlightImageRate(0)}} onMouseMove={()=>{this.highlightImageRate(1)}} className={this.state.rates[0] ? 'fas fa-star':'far fa-star'}></i>
                                    <i onClick={()=>{this.rateImage(2)}} onMouseOut={()=>{this.highlightImageRate(0)}} onMouseMove={()=>{this.highlightImageRate(2)}} className={this.state.rates[1] ? 'fas fa-star':'far fa-star'}></i>
                                    <i onClick={()=>{this.rateImage(3)}} onMouseOut={()=>{this.highlightImageRate(0)}} onMouseMove={()=>{this.highlightImageRate(3)}} className={this.state.rates[2] ? 'fas fa-star':'far fa-star'}></i>
                                    <i onClick={()=>{this.rateImage(4)}} onMouseOut={()=>{this.highlightImageRate(0)}} onMouseMove={()=>{this.highlightImageRate(4)}} className={this.state.rates[3] ? 'fas fa-star':'far fa-star'}></i>
                                    <i onClick={()=>{this.rateImage(5)}} onMouseOut={()=>{this.highlightImageRate(0)}} onMouseMove={()=>{this.highlightImageRate(5)}} className={this.state.rates[4] ? 'fas fa-star':'far fa-star'}></i>

                                </div>
                            </div>
                            <div className="imageoptions ">
                                <span className="name">{activeImage.title}</span>
                                <div className="dropdown dropup">
                                    <a className="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-h"></i>
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a className="dropdown-item" target='_blank' href={actionImageUrl}>Open in new tab</a>
                                        <a className="dropdown-item" href="#">Share</a>
                                        <a className="dropdown-item"  href='http://images2.fanpop.com/images/photos/4600000/Nature-random-4603337-1280-800.jpg' download>Download</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                 </div>
                 <Foot />
            </div>
        );
    }

    componentDidMount() {
        if(this.props.mainButtons.length != 0){
            let img=this.props.button.images.find((ig)=> ig.id == this.props.match.params.id);

            this.selectImage(img);
        }else{
            this.props.getContent(this.props.match.params.button , this.props.match.params.id);
        }

    }

    highlightImageRate=(star)=>{
        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 5-star; i++) {
            empty.push(false);
        }

        let overAllRates=[false,false,false,false,false];
        overAllRates=[...marked,...empty];

        this.setState(()=>{
            return{
                ...this.state,
                rates:overAllRates
            }
        });
    }
    rateImage=(star)=>{
        this.props.rateImage(this.props.button.id , this.props.image.id ,{user:this.state.user , rate:star});
    }
    OverAllrateImage=(star)=>{
        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 5-star; i++) {
            empty.push(false);
        }

        let overAllRates=[false,false,false,false,false];
        overAllRates=[...marked,...empty];

        return overAllRates;
    }

    selectImage=(img)=>{
        this.props.selectImage(img);
        this.setState(()=>{
            return {
                ...this.state,
                viewImage: {
                    show: true
                }
            }
        });

    }

    hideViewer=()=>{
        if(this.state.viewImage.show) {
            this.setState(() => {
                return {
                    ...this.state,
                    viewImage: {
                        show: false
                    }
                }
            });
        }
    }
}

const mapState=(state)=>{
    return {
        button:state.leftpanelReducer.currentButton,
        image:state.BottomPanelReducer.images,
        mainButtons:state.leftpanelReducer.mainButtons,
    }
}

const mapProp=(dispatch)=>{
    return {
        getContent:(id,video)=>{ dispatch(leftpanelActions.getContent(id,video))},
        selectImage:(img)=>{dispatch({type:'set_image',images:img})},
        rateImage:(id,image,paylod)=>{ dispatch(leftpanelActions.rateImage(id,image,paylod)); dispatch({type:'set_image_rate',rate:paylod})},
    }
}
export default connect(mapState,mapProp)(MemoPage);