import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2';
// import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";

// import chat_ballon from '../../assets/images/Chat_Balloon.png';

class EmailListing extends Component{

    render(){   
        return(
            <div id="emaillist" className="container-dt"> 
                <Navbar2 />
                    <div className="col-11 col-sm-10 col-md-8 col-lg-6 mb-5 inner"> 
                        <h5>SIGN UP FOR OUT EMAIL LIST</h5>
                        <p>
                            Want to keep up with ThinkProgress' coverage , Sign up for our daily newletter! Enter your email address
                             below and you will get our top stories in your inbox everyday, If you have already signed up you can unsubscribe
                             from ThinkProgress' email by <a href="">clicking here</a>
                        </p>

                        <div className="form-group col-12 col-sm-6 col-md-7">
                            <label className="col-12">Email</label>
                            <input type="text" placeholder="email@email.com" className="form-control" />
                        </div>

                        <div className="form-group">
                            <button className="btn primary">Subscribe</button>
                        </div>
                    </div> 
                <Foot />
            </div>
        )
    }


}

const mapStateToProps=(state)=>{
    return{
        
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
         
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(EmailListing);