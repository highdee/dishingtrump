import React , { Component } from 'react';
import Navbar from '../layouts/navbar'
import Foot from '../layouts/footer';
import LeftPanel from '../layouts/leftpanel';

import user_icon from "../../assets/images/usericon2.jpg";
import chat_ballon from '../../assets/images/Chat_Balloon.png';
class Settings extends Component{

    render(){

        return(
        <div id="settings">
             <Navbar />
            
             <div id="mainbody">
                <LeftPanel />
                    <div id="box" className="col-12 col-sm-8 col-md-8 offset-sm-2 offset-md-2 row ">
                        <div className="col-md-3 setting_side1 p-0">
                            <h5 className="modal-title" id="exampleModalLabel">Personal Settings</h5>

                            <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a className="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-home" aria-selected="true">Profile</a> 
                                <a className="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-sub" role="tab" aria-controls="v-pills-messages" aria-selected="false">View subscription</a>
                                <a className="nav-link" id="v-pills-settings-tab" data-toggle="pill"  role="tab" aria-controls="v-pills-settings" aria-selected="false">Exit</a>
                            </div>
                        </div>
                        <div className="col-md-9 setting_side2">
                            <div className="tab-content" id="v-pills-tabContent">
                                <div className="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <div className='row hd'>
                                            <span className='bg'>Justin Mark</span>
                                            <span className="location"><i className="fas fa-map-marker-alt"></i> Rica, Latvia</span>
                                            <small className='col-12'>Edit your name, avatar, email, etc</small>
                                    </div>
                                    <div className='row'> 
                                        <div className='row m-0 col-12 mainsettingsbody'>
                                            <div className="col-12 col-md-4 passport p-0">
                                                <div className="p-0 inner">
                                                    <img alt="" src={user_icon} />
                                                    <button className="btn btn-upld btn-block">Upload <i className="fas fa-camera"></i></button>
                                                </div>
                                                <small className="regdate">
                                                    <span>Registeration date</span>
                                                    <span>5/03/2021</span>
                                                </small>
                                            </div>
                                            <div className="col-12 col-md-8 inner-body">
                                                <h5>Personal Information</h5>

                                                <div className="form-group">
                                                    <label>Name Surename</label>
                                                    <input type="text" className="form-control" />
                                                </div>

                                                <div className="form-group">
                                                    <label>Birth date</label>
                                                    <input type="date" className="form-control" />
                                                </div>
                                                
                                                <h5>Contact Information</h5>

                                                <div className="form-group">
                                                    <label>e-mail</label>
                                                    <input type="email" className="form-control" />
                                                </div>

                                                <div className="form-group">
                                                    <label>Number</label>
                                                    <input type="text" className="form-control" />
                                                </div>

                                                <h5>Password Management</h5>

                                                <div className="form-group">
                                                    <label>Old Password</label>
                                                    <input type="password" className="form-control" />
                                                </div>

                                                <div className="form-group">
                                                    <label>New Password</label>
                                                    <input type="password" className="form-control" />
                                                </div>

                                                <button className='btn btn-dt-lg dt-text primary '>Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="v-pills-sub" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <div className='row hd hd2'>
                                            <span className='bg'>Subscription Page</span> <br /> 
                                            <small className='col-12'>Here you can add slogan</small>
                                    </div>
                                    <div id="sub_box" className="row col-12 m-0 p-0">
                                        <div className="col-12 col-md-3 side-left">
                                            <img alt="" src={chat_ballon} />

                                            <h1>FREE</h1>
                                        </div>
                                        <div className="col-12 col-md-9 side-right pr-0">
                                            <div className="col-12 hed p-0">
                                                <span>Includes</span>
                                                <span>BASIC ACCOUNT</span>
                                            </div>
                                            <div className="col-12 d-block p-0 mt-4 bdy">
                                                <p>
                                                    Access to PRIME-only content, an add free mobile site & warm fuzzy feelings from supporting independent journalism
                                                </p>

                                                <button className="btn primary">Update</button>
                                            </div>
                                        </div>

                                    </div>

                                    <h3 className="wjp">Why Join PRIME?</h3>
                                    <p className="wpp"> 
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio accusamus eveniet iure temporibus voluptatum fugiat perferendis, dolorem cumque itaque tempora dolores, provident modi debitis, dignissimos sit rem animi. Repudiandae, quas
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio accusamus eveniet iure temporibus voluptatum fugiat perferendis, dolorem cumque itaque tempora dolores, provident modi debitis, dignissimos sit rem animi. Repudiandae, quas
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio accusamus eveniet iure temporibus voluptatum fugiat perferendis, dolorem cumque itaque tempora dolores, provident modi debitis, dignissimos sit rem animi. Repudiandae, quas
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div> 

            <Foot />
        </div>
        )
    }
}

export default Settings;