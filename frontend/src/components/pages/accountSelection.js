import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2';
import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";

import chat_ballon from '../../assets/images/Chat_Balloon.png';

class Subscription extends Component{

    render(){   
        return(
            <div id="account"> 
                <Navbar2 />
                    <div className="col-12 col-sm-10 col-md-10 col-lg-7 mb-5 inner">
                        <div className="boxes row">
                            <div className="col-10 col-sm-6 col-md-4 box">
                                <div className="inna">     
                                    <div className="ang">
                                        <span>BASIC VALUE</span>
                                    </div> 
                                    <div className="bdy">
                                        <h2>ANNUAL <br /> PRIME</h2>
                                        <h4>50$/YR.</h4>
                                    </div>
                                    <div className="footer">
                                        <h4>Includes</h4>
                                        <p>
                                            Access to PRIME-only content, an ad-free mobile mobile site and warm , fuzzy feelings
                                            from supporting independent journalism
                                        </p>
                                        <button className="btn primary">JOIN PRIME</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 col-sm-6 col-md-4 box">
                                <div className="inna primary"> 
                                     <div className="bdy">
                                        <h2>MONTHLY <br /> PRIME</h2>
                                        <h4>4.99$/MO.</h4>
                                    </div>
                                    <div className="footer">
                                        <h4>Includes</h4>
                                        <p>
                                            Access to PRIME-only content, an ad-free mobile mobile site and warm , fuzzy feelings
                                            from supporting independent journalism
                                        </p>
                                        <button className="btn primary">JOIN PRIME</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 col-sm-6 col-md-4 box">
                                <div className="inna">  
                                    <div className="ang">
                                       <span>BASIC ACCOUNT</span>
                                    </div>
                                    <div className="bdy">
                                        <img src={chat_ballon} />
                                        <h4>FREE</h4>
                                    </div>
                                    <div className="footer">
                                        <h4>Includes</h4>
                                        <p>
                                            Access to PRIME-only content, an ad-free mobile mobile site and warm , fuzzy feelings
                                            from supporting independent journalism
                                        </p>
                                        <button className="btn primary">JOIN PRIME</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="fut">
                            <h3>Why Join PRIME?</h3>
                            <p>
                                Prime is complete, TPM experience, It brings you inside the news room with exclusive insight
                                update and analysis behind the headlines and bylines, in addition to exclusive content,members
                                get access to out members-only forum, Full RSS  feeds and TPM with 75% fewer ads.
                                you also get site features exclusive for members , like the ability to bookmark stories and guide to what
                                has been added since your last visit,
                                PRIME membership makes TPM independent journalism possible , Join us today
                            </p>
                        </div>
                    </div>
                <PopUpMessage/>
                <Foot />
            </div>
        )
    }


}

const mapStateToProps=(state)=>{
    return{
        
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
         
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Subscription);