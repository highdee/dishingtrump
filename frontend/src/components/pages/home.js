import React , { Component } from 'react';
import { connect } from "react-redux";
import Navbar from '../layouts/navbar'
import Foot from '../layouts/footer';
import RouteHelper from "../layouts/routerHelper";

// import AlertOver from '../layouts/alert';
import LoginModal from '../layouts/loginModal';
import RegModal from '../layouts/regModal';
import RobotText from '../layouts/robotText';
import UserInput from '../layouts/userInput'; 
import AudioWave from '../layouts/audioWave'; 
import Settings from '../layouts/settings';

import IntroVideo from "./introvideo";

import LeftPanel from '../layouts/leftpanel';
import RightPanel from '../layouts/rightpanel';
import BottomPanel from '../layouts/bottompanel';

import RowButton from '../layouts/rowButton';
import RowButton2 from '../layouts/rowButton2';

import Fullwidthvideo from "../layouts/fullwidthvideo";
import Fullwidthimage from "../layouts/fullwidthimage";

import logo from "../../assets/images/logo.png";

class Home extends Component{

    render(){
        return(
            <div style={{height:'100%'}}>
                <IntroVideo />
                <Navbar />
                
                    <div onClick={()=>{this.props.hideBottomBar()}} id="mainbody">
                        <LeftPanel />
                        <RightPanel />
                       
                        <div className="p-0 col-12 col-sm-8 col-md-10 col-lg-6 offset-sm-2 offset-md-1 offset-lg-3 ina">
                            <div id="logo">
                                <img alt="" src={logo} />
                            </div>
                            <RobotText />
                            <UserInput />
                            <AudioWave />
                            <RowButton />
                            <RowButton2 />
                        </div>
                        <Foot />
                    </div>
                    

                    {/* <AlertOver /> */}
                    <Settings />
                    <RegModal />
                    <LoginModal />

                <BottomPanel />

                <Fullwidthvideo />
                <Fullwidthimage />
                <RouteHelper {...this.props}/>
            </div>
          )
    }
    
    componentDidMount(){
        // console.log(this.props);\
        
    }
}

const mapStateToProps=(state)=>{
    return{ 
        chats:state.robotReducer.chats,
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer,
        content:state.BottomPanelReducer,
        currentButtons:state.leftpanelReducer.currentButton,
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
        hideBottomBar:()=>{dispatch({type:'toggleBottomPanel' , data:false})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Home);