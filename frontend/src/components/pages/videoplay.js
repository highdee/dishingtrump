import React, { Component } from 'react'
import Navbar2 from '../layouts/navbar2' ;
import { connect } from "react-redux";
import Foot from '../layouts/footer';
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import blogImage from '../../assets/images/blogimage.png';
import leftpanelActions from "../../actions/leftpanelActions";
import Videoskeleton from "../layouts/videopageskeleton";
import {NavLink} from "react-router-dom";
import banner2 from "../../assets/images/300x250.jpg";
import banner1 from "../../assets/images/728x90.png";
import AudioWave from '../layouts/audioWave';
import robotActions from "../../actions/robotActions";
import RouteHelper from "../layouts/routerHelper";

class VideoPlay extends Component{

    state={
        videoContents:{
            details:true,
            transcripts:false,
            comments:false
        },
        rates:[false,false,false,false,false],
        user_id:523
    }

    changeContent(which){
        this.setState(()=>{
            return{
                ...this.state,
                videoContents: {
                    details:false,
                    transcripts:false,
                    comments:false,
                    [which]:true
                },

            }
        });

    }

    render(){

        let video=this.props.video;
        let button=this.props.button;

        let userRate=video.rates.find((rate)=> rate.user == this.state.user_id );
        let userRates=this.GetUserOverAllrating(userRate);
        // this.props.video.currentUserRate=userRates;
        let stateRating=this.state.rates;

        // console.log(this.props.chat_attr);

        let rates=video.rates.map((rate)=>{ return rate.rate ;});
        let overall_rating=rates.reduce((a,c)=> a + c ,0);
        overall_rating=overall_rating / rates.length ;
        let  overAllRates=this.OverAllrateVideo(overall_rating);

        let related=[];
        related=this.getRelated();
        // if(this.props.button.videos && this.props.button.videos.length <= 4){
        //     related=this.props.button.videos.filter(vid=>{
        //         return vid.id != video.id;
        //     })
        // }

        let highlightVideo=this.props.video.code == '' ? this.props.video.url=="" ? this.props.video.filename == '' ? '': this.props.video.filename  : this.props.video.url : this.props.video.code;


        return(
            <div id="playvideo" className="container-dt"> 
                 <Navbar2 />
                 <div className="playvideoInner col-12 col-sm-11 col-md-11 col-lg-8">
                     {
                         !this.props.video.id ?
                             <Videoskeleton />
                            :
                         <div className="row p-0 m-0">

                                 <div className="col-sm-12 col-md-9 videosection">
                                     <img alt="" className="ad_banner ad_banner2 d-none d-sm-block" src={banner1}/>
                                     <h2 className="videotitle">{video.title}</h2>
                                     <span className="mt-2 commentstats d-block d-sm-block d-md-block d-lg-none">
                                        <span>
                                            {rates.length} votes  &nbsp;
                                            <i className={overAllRates[0] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[1] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[2] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[3] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[4] ? 'fas fa-star':'far fa-star'}></i>
                                         </span>

                                         <span>
                                              {video.views+1 ? video.views+1:1} views &nbsp;
                                             <i className="fas fa-eye"></i>
                                         </span> 
                                    </span>
                                     <div className="videoHolder">
                                         {
                                             this.props.video.code != '' ?
                                                 (<span dangerouslySetInnerHTML={{__html: highlightVideo}}></span>)
                                                 :
                                                 this.props.video.url ?
                                                     (<video id='display_video' controls
                                                             style={{width: '100%', height: '100%'}}
                                                             src={highlightVideo}></video>)
                                                     :
                                                     (<video id='display_video' controls
                                                             style={{width: '100%', height: '100%'}}
                                                             src={highlightVideo}></video>)
                                         }
                                     </div>
                                     <div className="tabButtons">
                                         <button onClick={() => {this.changeContent('details');}} className={this.state.videoContents.details ? 'btn primary ' : 'btn'}>Details
                                         </button>
                                         <button onClick={() => {this.changeContent('transcripts');}} className={this.state.videoContents.transcripts ? 'btn primary ' : 'btn'}>Transcripts
                                         </button>
                                         <button onClick={() => {this.changeContent('comments');}} className={this.state.videoContents.comments ? 'btn primary ' : 'btn'}>Comments (4)
                                         </button>

                                         <span style={{float: 'right'}}
                                               className="mt-4 commentstats d-none d-sm-none d-md-none d-lg-block">
                                         <span>
                                            {rates.length} votes  &nbsp;
                                             <i className={overAllRates[0] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[1] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[2] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[3] ? 'fas fa-star':'far fa-star'}></i>
                                            <i className={overAllRates[4] ? 'fas fa-star':'far fa-star'}></i>
                                         </span>

                                         <span>
                                            {video.views+1 ? video.views+1:1} views &nbsp;
                                             <i className="fas fa-eye"></i>
                                         </span> 
                                    </span>
                                     </div>
                                     <div className="videoContents">
                                         <div
                                             className={this.state.videoContents.details ? 'details ' : 'details hide'}>
                                             <p>
                                                 {video.description}
                                             </p>
                                         </div>
                                         <div
                                             className={this.state.videoContents.transcripts ? 'transcript ' : 'transcript hide'}>
                                             <p>
                                                 {video.transcript}
                                             </p>
                                         </div>
                                         <div
                                             className={this.state.videoContents.comments ? 'comments ' : 'comments hide'}>
                                             <ul>
                                                 <li>
                                                     <div className="imageHolder"></div>
                                                     <div className="commentinfo">
                                                         <span className="name">John jhein</span> <span
                                                         className="time">2 secs ago</span>
                                                         <div className="textComment">
                                                             Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                             Voluptas quaerat temporibus
                                                             Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                             Voluptas quaerat temporibus
                                                             Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                             Voluptas quaerat temporibus
                                                         </div>
                                                     </div>
                                                 </li>
                                             </ul>

                                            <div id='' className='mt-5'>
                                                 <div className='commentBox d-block'>
                                                     <textarea id='commentText' style={{width:'100%'}} rows='2' placeholder="Type your comment........"></textarea>
                                                     <div className='tools'>
                                                         <span className='float-left mt-2 ml-2'>
                                                             Rate &nbsp;
                                                             <i onClick={()=>{this.rateVideo(1)}} className={this.props.video.currentUserRate[0] ? 'fas fa-star':'far fa-star'}></i>
                                                             <i onClick={()=>{this.rateVideo(2)}} className={this.props.video.currentUserRate[1] ? 'fas fa-star':'far fa-star'}></i>
                                                             <i onClick={()=>{this.rateVideo(3)}} className={this.props.video.currentUserRate[2] ? 'fas fa-star':'far fa-star'}></i>
                                                             <i onClick={()=>{this.rateVideo(4)}} className={this.props.video.currentUserRate[3] ? 'fas fa-star':'far fa-star'}></i>
                                                             <i onClick={()=>{this.rateVideo(5)}} className={this.props.video.currentUserRate[4] ? 'fas fa-star':'far fa-star'}></i>
                                                         </span> &nbsp;
                                                         <button onClick={this.commentVideo}  className='btn primary '> &nbsp; <i className='fas fa-paper-plane'></i> &nbsp;Send &nbsp; </button>
                                                     </div>
                                                 </div>
                                            </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div className="col-sm-12 col-md-3 relatedMedia">
                                     <img alt="" className="ad_banner  d-none d-sm-block" src={banner2}/>
                                     <h4>Related Media</h4>
                                     <div className="blg-right">
                                         {
                                             related.map(relVid=>{
                                                 return (
                                                     <div className="blg2">
                                                         {
                                                              // relcontent=relVid.code == '' ? relVid.url=="" ? relVid.filename == '' ? '': relVid.filename  : relVid.url : relVid.code

                                                             relVid.code != '' ?
                                                             (<span dangerouslySetInnerHTML={{__html: relVid.code}}></span>)
                                                             :
                                                             relVid.url ?
                                                             (<video id='display_video'
                                                             style={{width: '100%', height: '100px'}}
                                                             src={relVid.url}></video>)
                                                             :
                                                             (<video id='display_video'
                                                             style={{width: '100%', height: '100px'}}
                                                             src={relVid.filename}></video>)

                                                         }

                                                         <a href={"/video/"+button.id+'/'+relVid.id+"/"+relVid.title.replace(/ /g,'-')}>
                                                             { relVid.title.length >18 ? relVid.title.substr(0,18)+'..':relVid.title}
                                                         </a>
                                                     </div>
                                                 )
                                             })
                                         }
                                     </div>
                                 </div>
                             </div>
                     }
                 </div>
                 <Foot />
                 <AudioWave class={"hide d-none"} style={{display:'none'}} />
                 <RouteHelper {...this.props} />
            </div>
        );
    }

    componentDidMount() {
        console.log(this.props.chat_attr);
        let chat=this.props.chat_attr;
        let action=chat.action;

        if(this.props.global.isTourActive && action.type=="route" && action.content=='video'){
            this.props.chatTextFinished();
        }
        if(this.props.video.id){
            let id=this.props.match.params.id;

            return false;
        }

        this.props.getContent(this.props.match.params.button , this.props.match.params.id);
        this.props.viewAVideo(this.props.match.params.button , this.props.match.params.id);
        // console.log(this.props.video);
    }


    getRelated=()=>{
        let video=this.props.video;
        let buttton=this.props.button;

        let related=[];
        let currentVideoIndex=0;

        //GET THE INDEX OF CURRENT ARTICLE
        related=this.props.button.videos.filter((art,index)=>{
            if(art.id == video.id){
                currentVideoIndex=index;
            }
            return art.id != video.id;
        })
        //SELECT THE NEXT 4 ARTICLES AFTER THE CURRENT ARTICLE FROM THE ARTICLE ARRAYS
        related=this.props.button.videos.filter((art,index)=>{
            if(index > currentVideoIndex){
                return true;
            }
            return false;
        })

        //IF THE REMAINING ARTICLES IS NOT UP TO 4 THEN GO AND SELECT FROM THE BEGINING OF THE ARRAY
        if(related.length <4){
            let temp=this.props.button.videos.filter((art,index)=>{
                if(related.length >= 4){
                    return false;
                }
                if(index < currentVideoIndex){
                    return true;
                }
                return false;
            })
            related=[...related , ...temp];
        }

        return related;
    }

    commentVideo=()=>{
        let input=document.getElementById('commentText').value;
        let button=this.props.button;
        let video=this.props.video;

        if(input.trim() == ''){
            alert('Comment cannot be empty');
        }
        this.props.commentVideo(button.id,video.id,{text:input,user:21});

    }

    OverAllrateVideo=(star)=>{
        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 5-star; i++) {
            empty.push(false);
        }

       let overAllRates=[false,false,false,false,false];
        overAllRates=[...marked,...empty];

        return overAllRates;
    }

    GetUserOverAllrating=(rating)=>{
        if(!rating){
            return [false,false,false,false,false];
        }
        let star=rating.rate;
        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 5-star; i++) {
            empty.push(false);
        }

        let overAllRates=[false,false,false,false,false];
        overAllRates=[...marked,...empty];
        // console.log(overAllRates);
        this.props.video.currentUserRate=overAllRates;

        return overAllRates;
    }

    rateVideo=(star)=>{


        let rates=this.props.video.rates;
        let userRate={};
        userRate=rates.find((rat)=> {
            return rat.user == this.state.user_id;
        } );
        if(userRate){
            userRate.rate=star;
            this.GetUserOverAllrating(userRate);
        }
        else{
            userRate={};
            userRate.rate=star;
            userRate.user=this.state.user_id;
            this.GetUserOverAllrating(userRate);
        }

        this.props.set_video_user_rate(this.state.user_id,star);


        let button=this.props.button;
        let video=this.props.video;

        let marked=[];
        for (let i = 0; i < star; i++) {
            marked.push(true);
        }
        let empty=[];
        for (let i = 0; i < 5-star; i++) {
            empty.push(false);
        }

        // this.setState(()=>{
        //     return {
        //         ...this.state,
        //         rates:[...marked,...empty]
        //     }
        // });
        this.props.rateVideo(button.id,video.id,{rate:star,user:this.state.user_id});
    }
}

const mapState=(state)=>{
    return {
        button:state.leftpanelReducer.currentButton,
        video:state.BottomPanelReducer.video,
        chats:state.robotReducer.chats,
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer,
    }
}

const mapProps=(dispatch)=>{
    return {
        getContent:(id,video)=>{ dispatch(leftpanelActions.getContent(id,video))},
        commentVideo:(button,video,payload)=>{dispatch(leftpanelActions.commentVideo(button,video,payload))},
        rateVideo:(button,video,payload)=>{dispatch(leftpanelActions.rateVideo(button,video,payload))},
        viewAVideo:(button,video)=>{dispatch(leftpanelActions.viewAVideo(button,video))},
        set_video_user_rate:(user,data)=>{dispatch({type:'set_video_user_rate',user,data})},
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
    }
}
export default connect(mapState,mapProps)(VideoPlay);