import React ,{ Component } from "react";
import Foot from '../layouts/footer';
import Navbar2 from '../layouts/navbar2';
import PopUpMessage from "../layouts/popupmessage";
import { connect } from "react-redux";

import chat_ballon from '../../assets/images/Chat_Balloon.png';

class Feeds extends Component{

    render(){   
        return(
            <div id="feeds" className="container-dt"> 
                <Navbar2 />
                    <div className="col-11 col-sm-10 col-md-10 col-lg-9 mb-5 inner"> 
                        <h5>The Full Feed</h5>
                        <div className="row p-0 m-0">
                            <div className="cols col-6 col-sm-3 col-md-3 col-lg-2">
                                <ul>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div className="cols col-6 col-sm-3 col-md-3 col-lg-2">
                                <ul>
                                    <li className='active'>
                                        <a className="" href="">Asian Voices</a>
                                    </li>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div className="cols col-6 col-sm-3 col-md-3 col-lg-2">
                                <ul>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div className="cols col-6 col-sm-3 col-md-3 col-lg-2">
                                <ul>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div className="cols col-6 col-sm-3 col-md-3 col-lg-2">
                                <ul>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div className="cols col-6 col-sm-3 col-md-3 col-lg-2">
                                <ul>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    <li>
                                        <a href="">Asian Voices</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div> 
                <Foot />
            </div>
        )
    }


}

const mapStateToProps=(state)=>{
    return{
        
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
         
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Feeds);