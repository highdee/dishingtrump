import React , { Component } from 'react';
import { connect } from "react-redux";
import robotActions from '../../actions/robotActions';

class UserInput extends Component{
    
    render(){
        let chat=this.props.chat_attr;
        let chat_active_action=chat.action;
        let flag=this.makeTextBoxActive();
 
        if(flag){
            setTimeout(()=>{
                document.getElementById('textbox').focus();
            },200);
        }
        // console.log(this.props.userinput);

        return(
            <div id="userInput">
                <div className="inner">
                    <input
                            id="textbox"
                            onKeyUp={(e)=> { this.saveDetails(e) } }
                            className={flag ? 'active': 'disabled'}
                            disabled={!flag}
                            type="text" placeholder="Type some text here"  
                         />
                    <i className="fas fa-bug"></i>
                    <i onClick={this.recognise} className="fas fa-microphone"></i> 
                </div>
            </div>
        )
    }
    makeTextBoxActive(){
        let chat=this.props.chat_attr;
        let chat_active_action=chat.action;

        return (chat_active_action.type == 'ua' && (chat_active_action.ua == 1 || chat_active_action.ua == 4)) ;
    }
    makeVoiceActive(){
        let chat=this.props.chat_attr;
        let chat_active_action=chat.action;

        return (chat_active_action.type == 'ua' && (chat_active_action.ua == 4)) ;
    }

    recognise=()=>{
        if(this.makeVoiceActive()){
            var recognition = window.webkitSpeechRecognition || window.SpeechRecognition;
            recognition=new recognition();
           
            recognition.lang = 'en-US';
            recognition.interimResults = false;
            recognition.maxAlternatives = 1;
    
            recognition.addEventListener('result',e=>{
                this.props.updateUserTextInput(e.results[0][0].transcript);
            });  
    
            recognition.start();   
        }
    }

    saveDetails(e){
        let key=e.which || e.button;
        let value=e.target.value || e.srcElement.value;

        if(key == 13){
            this.props.updateUserTextInput(value); 
            e.target.value='';
        }
    }
}

const mapStateToProps=(state)=>{
    return{
        chat_attr:state.robotReducer.chatAttributes,
        userinput:state.robotReducer.userinputs
    }
}

const mapDispatchToProps=(dispatch,state)=>{
    return {
        updateUserTextInput:(data)=>{ dispatch(robotActions.setUserTextInput(data)); }
    }
}
export default  connect(mapStateToProps,mapDispatchToProps)(UserInput);
