import React , { Component } from 'react';
import { Link } from 'react-router-dom';
// import user_icon from "../../assets/images/usericon.png";
import { connect } from 'react-redux'; 

class Navbar2 extends Component{

    render(){
        return (
            <nav id="navbar" className="navbar navbar-expand-lg navbar-light bg-light pt-4">
                {/* <a className="navbar-brand" href="#">Navbar</a> */}

                 
                    <ul className="nav mr-auto">
                        <li className="nav-item ml-4">
                            <Link onClick={(e)=>{ if(this.props.global.isTourActive){ e.preventDefault(); return false;} }} className="nav-link " to="/">
                                <i className="fas fa-caret-left"></i>
                            </Link>
                        </li>  
                    </ul>
                    
                    <ul className="nav">
                        {/* <li className="nav-item">
                            <a data-toggle="modal" className="nav-link" href="#loginModal">Login </a>
                        </li>
                        <li className="nav-item">
                            <a data-toggle="modal" className="nav-link" href="#regModal">Registration</a>
                        </li>   */}
                        {/* <li className="nav-item">
                            <Link className="nav-link" to="/settings">settings</Link>
                        </li>  */}
                        {/* <li className="nav-item">
                            <span  className="nav-link dropdown-toggle user-icon" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <img className="" src={user_icon} /> Justin Mark
                                    
                                 <i className="fas fa-ellipsis-h"></i>

                                <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <ul>
                                        <li className="dropdown-item"><Link className="nav-link" to="/settings">Edit profile</Link></li>
                                        <li className="dropdown-item"><Link className="nav-link" to="/settings">View subscription</Link></li>
                                        <li className="dropdown-item"><Link className="nav-link" to="#">Edit</Link></li>
                                    </ul>
                                </div>
                            </span>
                        </li>  */}
                        {/* <li className="nav-item pt-1 d-none d-sm-block" >
                            <Link className="nav-link primary btn-dt align-middle " to="/donate">Donate</Link>
                        </li> 
                       
                        <li className="nav-item ml-4" onClick={this.props.toggleRightPanel}>
                            <a className="nav-link " href="#">
                                <i className={ this.props.rightPanel.show ? 'active fas fa-chevron-right':'fas fa-th'}></i>
                            </a>
                        </li>  */}
                    </ul> 
            </nav>
        )
    }
}


const mapStateToProps=(state)=>{
    return{
        leftPanel:state.System_ui.leftPanel,
        rightPanel:state.System_ui.rightPanel,
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer,
    }
}

const  mapDispatchToProp=(dispatch)=>{
    return{
            toggleLeftPanel:()=>{ dispatch({type:"toggleleftPanel"}); },
            toggleRightPanel:()=>{ dispatch({type:'toggleRightPanel'})}
    }
}
export default connect(mapStateToProps,mapDispatchToProp)(Navbar2)  ;