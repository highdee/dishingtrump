import React , { Component } from "react";
import { connect } from 'react-redux'; 
 import robotActions  from "../../actions/robotActions";

class AudioWave extends Component{
    state={
        tts:{}, 
        currentVoice:0

    } 
    render(){
        
        if(this.props.global.isTourActive){
            setTimeout(()=>{
                if(this.shouldISpeack()){
                    this.speak();
                } 
            },500);
        } 

        
        return (
            <div id="waves"> 
                <button className="mt-5" onClick={this.speak}>SPEAK</button> 
            </div>
        )
    }

    shouldISpeack(){
        let chat=this.props.chat_attr;
        let chat_active_action=chat.action;  
        if(this.state.currentVoice == chat.chat.id+'/'+chat.active_action){
            return false;
        }
        
        return chat_active_action.type == 'text_voice';
    }

    speak=()=>{ 
        if(!this.props.global.isTourActive){
            return false;
        }
        let chat =this.props.chat_attr;
        let current_action=chat.chat.actions[chat.active_action];
        // console.log(current_action);
        if(current_action.type == 'text_voice'){
            // console.log(current_action);
            this.state.tts.cancel();
            
            //IF VARIABLE EXIST IN THE TEXT , IT WOULD HAVE BEEN CHANGED BY THE ROBOTTEXT.JS FILE
            let uttr=new window.SpeechSynthesisUtterance(current_action.text) ;
            uttr.addEventListener('end',()=>{
                console.log('done speaking..');
                this.props.chatTextFinished();
            });

            this.setState({
                ...this.state,
                currentVoice:chat.chat.id+'/'+chat.active_action
            });
             
            this.state.tts.speak(uttr);
            

            // console.log('i spoke');
        }
          
    }
   
    componentDidMount() {   
        this.setState({
            ...this.state,
            tts:window.speechSynthesis,
        },()=>{ 
            //THIS LINE STARTS THE BOT
            console.log(this.props.chat_attr);
            if(!this.props.chat_attr.chat.actions){
                this.props.initialiseBot();
            }
        }); 

    }
   
} 

const mapStateToProps=(state)=>{
    return { 
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer
    }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        initialiseBot:()=>{ dispatch(robotActions.initialiseBot()); }
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(AudioWave);