import React ,{ Component } from 'react';

class AlertOver extends Component{
    
    render(){
        return (
            <div id="alert">
                <div className="body">
                    <p>
                        We use cookies on this site to enhance your experience <br ></br>
                        By clicking on any link on this page you are giving us the conset for us to set cookies
                    </p>
                    
                    <button className='btn btn-dt text-primary primary-border'>No, i want to find out more</button>
                    <button className='btn btn-dt primary mr-2'>Yes ,i agree</button>
                </div>
            </div>
        )
    }
}

export default AlertOver;