import React , { Component } from "react";


class MembersOptions extends Component{

    render(){
        return(
            <div> 
                <div className="matrixMenu">
                    <div className="inne">
                        <button className="btn">Videos</button>
                        <button className="btn">Articles</button>
                        <button className="btn">Documents</button>
                        <button className="btn active">Photos</button>
                    </div>
                </div> 
            </div>
        )
    }
}

export default MembersOptions;