import React , {Component} from 'react';

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
class RowBottom2Skeleton extends Component{


    render(){
        let skel=[];
        for (let c=0;c<this.props.rem;c++){
            skel.push(<li key={c}>
                            <Skeleton width={'100%'} height={50}/>
                        </li>);
        }
        return(
            <ul className='RowBottom2Skeleton'>
                {skel}
            </ul>
        )
    }


    componentDidMount(){
        console.log(this.props);
    }
}

export default RowBottom2Skeleton;