import React , { Component } from "react";
import {connect} from 'react-redux';
import robotActions from '../../actions/robotActions';

class RowButton extends Component{

    
    render(){

        let flag=this.makeButtonActive();

        return(
            <div id="btnRow">
                <button className={this.props.global.isTourActive ? 'btn active':'btn'}>Tour</button>
                <button className={!this.props.global.isTourActive ? 'btn active':'btn'}>Dish</button>
                <button className='btn'>Muller's team</button>
                <button onClick={()=>{this.answer('YES')}} disabled={!flag} className={flag ? 'ready btn':'btn'}>YES</button>
                <button onClick={()=>{this.answer('NO')}} disabled={!flag} className={flag ? 'ready btn':'btn'}>NO</button>
                <button className='btn'>Muller's team</button>
            </div>
        )
    }
    answer=(answer)=>{
        // console.log(answer);
        this.props.setUserBoolInput(answer);
    }
    makeButtonActive(){
        let chat=this.props.chat_attr;
        let chat_active_action=chat.action;

        return chat_active_action.type == 'ua' && chat_active_action.ua == 3 ;
    }
}

const mapStateToProps=(state)=>{
    return { 
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer
    }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        initialiseBot:()=>{ dispatch(robotActions.initialiseBot()); },
        setUserBoolInput:(data)=>{ dispatch(robotActions.setUserBoolInput(data))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(RowButton);