import React , { Component } from 'react';
import  fb_icon  from "../../assets/images/fb-icon.png";
import  ga_icon  from "../../assets/images/ga-icon.png";

class RegModal extends Component{


    render(){
        return( 
            <div className="modal fade" id="regModal" tabIndex="-2" role="dialog" aria-labelledby="regexampleModalLabel" aria-hidden="true">
            <div className="modal-dialog model-xs m-0" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Registration</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body pt-0">
                    <form className="">
                        <div className="form-group">
                            <label className=''>Name</label>
                            <input type="text" className="form-control" placeholder="jhon"/>
                        </div>

                        <div className="form-group">
                            <label className=''>Surename</label>
                            <input type="text" className="form-control" placeholder="johnsons"/>
                        </div>

                        <div className="form-group">
                            <label className=''>Email</label>
                            <input type="email" className="form-control" placeholder="Email address"/>
                        </div>

                        <div className="form-group">
                            <label className=''>Password</label>
                            <input type="password" className="form-control" placeholder="********"/>
                        </div>

                        <div className="form-group">
                            <label className=''>Confirm Password</label>
                            <input type="password" className="form-control" placeholder="********"/>
                        </div> 
                        <div className=" p-0">
                            <button className='btn btn-dt dt-text primary mr-2'>Registeration</button> 
                            
                            <span className="dt-text dt-bold-600">or login with &nbsp;
                                <img alt="" src={ga_icon} className="reg_btm"/>
                                <img alt="" src={fb_icon} className="reg_btm"/>
                            </span>
                            <a href="" className="ihac"> i have an account</a>
                        </div>

                     </form>
                </div> 
                </div>
            </div>
            </div>
        )
    }
}

export default RegModal;