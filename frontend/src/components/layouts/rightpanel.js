import React , { Component } from "react";
import { connect } from "react-redux";
import RightLayerOne from './right_layer_one';
import MembersOptions from './members_options';
import robotActions from "../../actions/robotActions";
import leftPanel from "../../actions/leftpanelActions";

class RightPanel extends Component{


    render(){
        this.checkAction();

        return (
            <div id="rightbar" className={this.props.rightPanel.show ? 'show':'hidde'}>
                <div className="inner">
                    <h5>Today's News</h5>
                    <RightLayerOne />
                    <br />
                    <h5>Member options</h5>
                    <MembersOptions />
                </div>
            </div>
        )
    }

    checkAction=()=>{
        let chat=this.props.chat_attr;
        let action=chat.action;

        if(this.props.global.isTourActive && (action.type == 'si' && action.si == 'rp')){
            if(this.props.rightPanel.show){
                if(action.status == 'close'){
                    this.props.toggleRightPanel();
                }
                this.props.chatTextFinished();
                return false;
            }

            this.props.toggleRightPanel();
            this.props.chatTextFinished();
        }
    }
}

const mapStateToProps=(state)=>{
    return{
        rightPanel:state.System_ui.rightPanel,
        chat_attr:state.robotReducer.chatAttributes,
        buttons:state.bottomButtomReducer.rowbotton2,
        global:state.GlobalReducer
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        toggleRightPanel:()=>{ dispatch({type:'toggleRightPanel'})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(RightPanel);