import React , { Component } from 'react';
import { connect } from "react-redux";
import  { NavLink } from "react-router-dom";
import leftPanel from "../../actions/leftpanelActions";
import robotActions from "../../actions/robotActions";


class BottomPanel extends Component{
    
    state={
        bottomPanel:{
            Width:document.getElementsByClassName('.inner').offsetWidth,
            height:'0px'
        }
    }
   
    render(){

        this.checkAction();

        // console.log(this.props.currentButton);
        let ct=this.props.contents;
        let current_button=this.props.currentButton;
        // console.log(current_button);
        const style={width:this.state.bottomPanel.Width};

        return (
            <div  id="bottombar" className={this.props.bottomPanel.show ? 'show ':'hidde'}>
                <div className='closeBottomBar d-none d-sm-none d-md-block d-lg-block'>
                    <button onClick={this.props.hideBottomBar} className='btn btn-natural'>
                        <i className='fas fa-chevron-down'></i>
                    </button>
                </div>
                <div className={this.props.bottomPanel.show ? 'show inner':'inner hide'} style={style}>
                    <ul>
                        {
                            ct.map((cnt,index)=>{
                                if(this.props.contentType.videos){
                                    return(
                                        <li key={index} >
                                           <NavLink onClick={(e)=>{if(this.props.global.isTourActive){e.preventDefault(); return false;} this.props.viewVideo(cnt)}} to={"/video/"+current_button.id+'/'+cnt.id+"/"+cnt.title.replace(/ /g,'-')}>
                                                <i className="fas fa-camera icon"></i>
                                                <span className="txt">{cnt.title.length > 35 ? cnt.title.substr(0,35)+'....':cnt.title}</span>

                                                <button className="btn">open</button>
                                                <span className="stars">
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                            </span>
                                            </NavLink>
                                        </li>
                                    )
                                }
                                else if(this.props.contentType.articles){
                                    return(
                                        <li key={index} >
                                            <NavLink to={"/view-article/"+current_button.id+'/'+cnt.id+'/'+cnt.title.replace(/ /g ,'-')}>
                                                <i className="fas fa-align-left icon"></i>
                                                <span className="txt">{cnt.title.length > 35 ? cnt.title.substr(0,35)+'....':cnt.title}</span>

                                                <button className="btn">open</button>
                                                <span className="stars">
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                            </span>
                                            </NavLink>
                                        </li>
                                    )
                                }
                                else if(this.props.contentType.documents){
                                    return(
                                        <li key={index} >
                                            <a href={"/view-document/"+current_button.id+'/'+cnt.id+'/'+cnt.title.replace(/ /g ,'-')}>
                                                <i className="fas fa-file icon"></i>
                                                <span className="txt">{cnt.title.length > 35 ? cnt.title.substr(0,35)+'....':cnt.title}</span>

                                                <button className="btn">open</button>
                                                <span className="stars">
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                            </span>
                                            </a>
                                        </li>
                                    )
                                }
                                else if(this.props.contentType.images){
                                    return(
                                        <li key={index} >
                                            <NavLink to={"/memo-page/"+current_button.id+'/'+cnt.id+'/'+cnt.title.replace(/ /g ,'-')}>
                                                <i className="fas fa-images icon"></i>
                                                <span className="txt">{cnt.title.length > 35 ? cnt.title.substr(0,35)+'....':cnt.title}</span>

                                                <button className="btn">open</button>
                                                <span className="stars">
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                            </span>
                                            </NavLink>
                                        </li>
                                    )
                                }

                            })
                        }
                    </ul>
                </div>
                <div className='closeBottomBar d-block d-sm-block d-md-none d-lg-none'>
                    <button onClick={this.props.hideBottomBar} className='btn btn-natural'>
                        <i className='fas fa-chevron-down'></i>
                    </button>
                </div>
            </div>
        )
    }

    // onscreenresize=()=>{
    //     window.addEventListener("resize",()=>{
    //         if(window.innerWidth <= "600px")
    //             {
    //                 this.setState({
    //                     ...this.state,
    //                     bottomPanel:{
    //                         ...this.state.bottomPanel,
    //                         Width:'100%'                    
    //                     }
    //                 });
    //             }
    //         var leftPanel=document.getElementById("leftbar").offsetWidth;
    //         var rightPanel=document.getElementById("rightbar").offsetWidth;
            
    //        this.setState({
    //            ...this.state,
    //            bottomPanel:{
    //                ...this.state.bottomPanel,
    //                Width:window.innerWidth - (leftPanel+rightPanel) - 40 +'px'                    
    //            }
    //        });
    //     });
    // }

    checkAction=()=>{
        let chat=this.props.chat_attr;
        let action=chat.action;

        if(this.props.global.isTourActive && (action.type == 'si' && action.si == 'bp')){
            if(this.props.bottomPanel.show){
                this.props.hideBottomBar();
                this.props.chatTextFinished();
                return false;
            }


            this.props.chatTextFinished();
        }
    }

    componentDidMount(){
        // this.onscreenresize();
    }

}

const mapState=(state)=>{
    return{
        bottomPanel:state.System_ui.bottomPanel,
        contents:state.BottomPanelReducer.contents,
        contentType:state.BottomPanelReducer.contentType,
        currentButton:state.leftpanelReducer.currentButton,
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer
    }
}

const mapProps=(dispatch)=>{
    return {
        viewVideo:(video)=>{ dispatch(leftPanel.viewVideo(video))},
        loadButtonContent:(content)=>{dispatch(leftPanel.loadButtonContent(content));},
        hideBottomBar:()=>{dispatch({type:'toggleBottomPanel' , data:false})},
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },

    }
}

export default connect(mapState,mapProps)(BottomPanel);