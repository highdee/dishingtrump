import React , { Component } from 'react';


class LoginModal extends Component{


    render(){
        return( 
            <div className="modal fade" id="loginModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog model-xs" role="document">
                <div className="modal-content"> 
                <div className="modal-header">

                    <h5 className="modal-title" id="exampleModalLabel">Login</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body pt-0">
                    <form className="mb-3">
                        <div className="form-group">
                            <label className='col-12'>Email</label>
                            <input type="email" className="form-control" placeholder="email address"/>
                        </div>

                        <div className="form-group">
                            <label className='col-12'>Password</label>
                            <input type="password" className="form-control" placeholder="********"/>
                        </div>

                        <div className="form-group form-check mb-4">
                            <input type="checkbox" className="form-check-input form-control-lg" id="exampleCheck1" />
                            <label className="form-check-label" htmlFor="exampleCheck1">Remember me</label>
                        </div>

                        <button className='btn btn-dt dt-text primary mr-2'>Login</button>
                        <button className='btn btn-dt dt-text text-primary primary-border'>Register</button>

                     </form>
                </div> 
                </div>
            </div>
            </div>
        )
    }
}

export default LoginModal;