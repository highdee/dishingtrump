import React , { Component } from 'react';

class Foot extends Component{
    
    render(){
        return (
            <footer id="footer" className="">
                <nav className=" navbar navbar-light bg-light">
                    {/* <a className="navbar-brand" href="#">Navbar</a> */}

                    
                        <ul className="nav mr-auto">
                            <li className="nav-item">
                                <a className="nav-link " href="/#">
                                    Facebook
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link " href="/#">
                                    Instagram
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link " href="/#">
                                    Twitter
                                </a>
                            </li>  
                        </ul>
                        
                        <ul className="nav">
                            <li className="nav-item">
                                <a className="nav-link" href="/#">Copyright &copy; 2018 </a>
                            </li> 
                        </ul>
                     
                </nav>
            </footer>
        )
    }
}

export default Foot  ;