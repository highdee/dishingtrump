import React , { Component } from "react";
import {connect} from 'react-redux';
import robotActions from '../../actions/robotActions';

class Fullwidthimage extends Component{

    render(){
        // console.log(this.props.imagelayer);

        let flag=this.makeThisActive();
        let chat = this.props.chat_attr;
        let chat_active_action=chat.action;

        //this doesn't work for tour/bot
        let imagelayer=this.props.imagelayer.data;
        let url='';

        if(imagelayer.image){
            url=imagelayer.image != '' && imagelayer.image != null ? imagelayer.image : imagelayer.imageurl;
        }


        if(chat_active_action.type == 'image'){
            setTimeout(()=>{
                this.props.chatTextFinished();
                console.log('image dispayed and finished');
            },chat.action.time);
        }

        if(imagelayer.image){
            setTimeout(()=>{
                this.props.unsetImagelayer();
                // console.log('done');
            },1000);
        }


        return (
            <div id="video" className={flag ? "overlay show":'overlay'}>

                <div className="innerContent">
                    {
                        imagelayer.image ?

                            <img style={{maxWidth:'100%'}} src={this.props.global.endpoint+'chatbuttoncontent/'+url} />
                            :
                            <img src={this.props.global.endpoint+chat_active_action.image} />
                    }
                </div>
            </div>
        );


    }

    makeThisActive(){
        let chat = this.props.chat_attr;
        let chat_active_action=chat.action;

        return (chat_active_action.type == 'image' || this.props.imagelayer.show);
    }


}

const mapStateToProps=(state)=>{
    return { 
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer,
        imagelayer:state.OverlayerReducer.imageLayer
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        unsetImagelayer:()=>{dispatch({type:'unset_imageLayer'})}
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Fullwidthimage);