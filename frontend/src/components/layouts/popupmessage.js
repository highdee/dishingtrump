import React , { Component } from 'react';
import { connect } from 'react-redux';

class PopUpMessage extends Component{
    
    render(){ 
        return(
            <div id="popup" className={this.props.popup.show? 'show':'hide'}>
               <div className="inner">
                    <p>thanks message</p>

                    <button onClick={()=>{this.props.popup_hide('')}} className="btn primary">Close</button>
               </div>
            </div>
        )
    }
}

const mapState=(state)=>{
    return {
        popup:state.System_ui.popup
    }
}

const mapDispatch=(dispatch)=>{
    return{
        popup_hide:(message)=>{ dispatch({type:"popup",message:''}) ;}
    }
}
export default connect(mapState,mapDispatch)(PopUpMessage);