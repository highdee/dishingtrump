import React , {Component} from 'react';

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
class Videoskeleton extends Component{


    render(){
        return(
            <div className="row p-0 m-0">

                <div className="col-sm-12 col-md-9 videosection">
                    <h2 className="videotitle"> <Skeleton /></h2>
                    <span className="mt-2 commentstats d-block d-sm-block d-md-block d-lg-none">
                         <span>
                         <Skeleton width={100} height={20}/>
                         </span>

                         <span>
                            <Skeleton width={100} height={20}/>
                         </span>
                         </span>
                    <div className="videoHolder p-0">
                        <Skeleton height={'100%'} className='m-0'/>
                    </div>
                    <div className="tabButtons">
                        <br />
                        <Skeleton width={100} height={35}/> &nbsp;
                        <Skeleton width={100} height={35}/>&nbsp;
                        <Skeleton width={100} height={35}/>

                        <span style={{float: 'right'}}
                              className="mt-1 commentstats d-none d-sm-none d-md-none d-lg-block">
                             <span>
                                 <Skeleton width={100} height={20}/>
                             </span>

                             <span>
                                  <Skeleton width={100} height={20}/>
                             </span>
                         </span>
                    </div>
                    <div className="videoContents">
                             <span className='mb-2'>
                                 <Skeleton height={100} />
                             </span>
                        <span className='mb-2'>
                                 <Skeleton height={100} />
                             </span>
                        <span className='mb-2'>
                                 <Skeleton height={100} />
                             </span>
                    </div>a
                </div>

                <div className="col-sm-12 col-md-3 relatedMedia">
                    {/*<h4>Related Media</h4>*/}
                    <div className="blg-right">
                        <div className="blg2">
                                 <span className='mb-2'>
                                     <Skeleton height={100} />
                                 </span>
                            <p>
                                     <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                                <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                            </p>
                        </div>
                        <div className="blg2">
                                 <span className='mb-2'>
                                     <Skeleton height={100} />
                                 </span>
                            <p>
                                     <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                                <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                            </p>
                        </div>
                        <div className="blg2">
                                 <span className='mb-2'>
                                     <Skeleton height={100} />
                                 </span>
                            <p>
                                     <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                                <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                            </p>
                        </div>
                        <div className="blg2">
                                 <span className='mb-2'>
                                     <Skeleton height={100} />
                                 </span>
                            <p>
                                     <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                                <span className='mb-1'>
                                         <Skeleton height={7} />
                                     </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Videoskeleton;