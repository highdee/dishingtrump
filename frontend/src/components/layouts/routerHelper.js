import React , { Component } from 'react';
import {connect} from 'react-redux';
import robotActions from "../../actions/robotActions";
class RouteHelper extends  Component{


    render(){
        console.log(this.props.chat_attr);
        return ('');
    }

    componentDidMount() {
        // this.routeToVideo();
        // console.log(this.props);
    }

    routeToVideo(){
        let chat=this.props.chat_attr;
        let action=chat.action;
        // console.log(action);
        if(this.props.global.isTourActive && action.type == 'route' && action.content=='video'){
            setTimeout(()=>{
                this.props.history.push("/video/"+this.props.currentButtons.id+"/"+action.id+'/'+this.props.content.video.title.replace(/ /g,'-'));
            },500);
        }
    }

    routePage(){
        let chat=this.props.chat_attr;
        let action=chat.action;
        // console.log(this.props);
        if(this.props.global.isTourActive && action.type == 'route' && action.content=='page'){
            setTimeout(()=>{
                this.props.chatTextFinished();
                this.props.history.push(action.route);
            },500);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(prevProps);
        this.routePage();
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        this.routeToVideo();
        return true;
    }
}

const mapState=(state)=>{
    return {
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer,
        content:state.BottomPanelReducer,
        currentButtons:state.leftpanelReducer.currentButton,
    }
}

const mapProps=(dispatch)=>{
    return{
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
    }
}
export  default connect(mapState,mapProps)(RouteHelper);