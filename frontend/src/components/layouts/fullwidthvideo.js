import React , { Component } from "react";
import {connect} from 'react-redux';
import robotActions from '../../actions/robotActions';

class Fullwidthvideo extends Component{

    render(){
        // console.log(this.props.videolayer);
        let flag=this.makeThisActive();
        let data=this.props.videolayer.data;
        // console.log(data);
        let video= flag ? 
        (

            data.video != undefined  ?
                    data.code != '' && data.code != null ?
                        <span id='videotag' dangerouslySetInnerHTML={{__html:data.code}} ></span>
                        :
                    data.video != '' && data.video != null ?
                        <video style={{width:'100%',height:'100%'}} id="videotag" autoPlay><source  src={this.props.global.endpoint +'chatbuttoncontent/'+ data.video} type="video/mp4"/>Sorry, your browser doesn't support embedded videos.</video>
                        :
                    data.image != '' && data.image != null ?
                        <img src={data.image} alt=""/>
                        :
                        <img src={data.imageurl} alt=""/>
                    :
                    <video id="videotag" autoPlay><source  src={this.props.chat_attr.action.video ? this.props.global.endpoint+this.props.chat_attr.action.video :''} type="video/mp4"/> Sorry, your browser doesn't support embedded videos.</video>


        ) : (<video id="videotag"></video>);

        if(flag){

            setTimeout(()=>{
            //
            document.getElementById('videotag').onended=()=>{
                console.log('it has finished playing');
                let chat = this.props.chat_attr;
                if(this.makeThisActive() ){
                    this.props.chatTextFinished(chat);
                }
                this.props.unsetvideolayer();
            }
            },1000);
        }
        return (
            <div id="video" className={flag ? "overlay show":'overlay'}>
                <div className="innerContent">
                   {video}
                </div>
            </div>
        );
    }

    makeThisActive(){
        let chat = this.props.chat_attr;
        let chat_active_action=chat.action;

        return (chat_active_action.type == 'video' || this.props.videolayer.show);
    }

    // componentDidUpdate(){
        
    // }
}

const mapStateToProps=(state)=>{
    return { 
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer,
        videolayer:state.OverlayerReducer.videoLayer
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        unsetvideolayer:()=>{ dispatch({type:'unset_videoLayer'}) }
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Fullwidthvideo);