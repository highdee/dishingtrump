import React , { Component } from "react";
import {connect} from 'react-redux';
import robotActions from '../../actions/robotActions';
import leftPanel from "../../actions/leftpanelActions";
import  RowBottom2Skeleton from "./rowBottom2Skeleton";

class RowButton2 extends Component{


    render(){
        let flag=this.checkAction(); 
        let chat=this.props.chat_attr;
        if(flag){
             chat=this.props.chat_attr; 
        }


        let buttons=this.props.buttons.map((btn,index)=>{  
            return (
                <button key={index} onClick={()=>{ this.handleClick(btn); }} className={(flag && chat.action.button == btn._id)? 'ready btn':'btn'} disabled={this.props.global.isTourActive && !(flag && chat.action.button == btn._id)} data={btn._id} >{btn.title}</button>
            );
        });


        if(buttons.length == 0){
            buttons.push(<RowBottom2Skeleton rem={6} />);
        }

        if(this.props.loaded){
            let remainder=6-buttons.length;
            for(let c=0;c<remainder;c++){
                buttons.push(<button className='mute btn' disabled></button>);
            }
        }

        return(
            <div id="btnRow2">
                {buttons}
            </div>
        )
    }
    handleClick(btn){
        
        let flag=this.checkAction(); 
        let chat=this.props.chat_attr;

        if(this.props.global.isTourActive && (flag && chat.action.button == btn._id)){
            this.props.chatTextFinished();
            this.props.getchatSubbuttons(btn);
            return false;
        }
        if(btn.data.length == 0){
            return false;
        }
        this.props.getchatSubbuttons(btn);
    }
    checkAction(){
        let chat=this.props.chat_attr;
        let action=chat.action;
        // console.log(action);
        return  (this.props.global.isTourActive && action.type == 'ua' && action.ua == 5 && action.ui=="base");
    }
    componentDidMount(){
        
    }
}

const mapStateToProps=(state)=>{
    return { 
        chat_attr:state.robotReducer.chatAttributes,
        buttons:state.bottomButtomReducer.rowbotton2,
        loaded:state.bottomButtomReducer.loaded,
        global:state.GlobalReducer
    }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        initialiseBot:()=>{ dispatch(robotActions.initialiseBot()); },
        getchatSubbuttons:(btn)=>{dispatch(leftPanel.getchatSubbuttons(btn)); }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(RowButton2);