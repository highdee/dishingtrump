import React, { Component } from 'react';
import { connect } from "react-redux";

class RobotText extends Component{
    
    state={
        chat:this.props.chat_attr
    }
    render(){
       let textmessage="";

        //checking if tour/bot is active
        if(this.props.global.isTourActive){
            
            let chat=this.props.chat_attr;
            let text=chat.action.text;
        
                if(chat.action.text){
                    // replacing the user's name into the string

                    let variables=text.match(/\[\|\w+\|\]/g);
                    if(variables){
                        let varname=variables[0];
                        varname=varname.replace('[|',"");
                        varname=varname.replace('|]',"");
                        varname=chat.user[varname];
                        chat.action.text=chat.action.text.replace(/\[\|\w+\|\]/g,varname);
                    }
                }

                textmessage= chat.chat.actions ? chat.chat.actions[chat.active_action].text :'';
               
        } 
 

        return(
            <div id="robt" className="primary">
                <p>{ textmessage }</p>
            </div>
        )
    }

    componentDidMount=()=>{
        // this.setState({
        //     ...this.state,
        //     chat:this.props.chat_attr
        // });
       
    }
}

const mapStateToProps=(state)=>{
    return { 
        chat_attr:state.robotReducer.chatAttributes,
        global:state.GlobalReducer
    }
}
const mapDispatchToProps=()=>{}

export default connect(mapStateToProps,null)(RobotText);