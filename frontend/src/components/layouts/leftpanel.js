import React , { Component } from "react";
import { connect } from "react-redux";

import leftPanel from "../../actions/leftpanelActions";
import LeftLayerOne from './left_layer_one';

import robotActions from '../../actions/robotActions';

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

class LeftPanel extends Component{


    render(){
        this.checkAction();

        return (
            <div id="leftbar" className={this.props.leftPanel.show ? 'show':'hidde'}>
                <div className="inner">
                    <h5>
                        Trump Matrix 
                    </h5>

                    <LeftLayerOne buttons={this.props.leftButtons}/>
                    
                </div>
            </div>
        )
    }
    componentDidMount=()=>{
        // console.log(this.props.leftButtons);
        if(this.props.leftButtons.length <= 0){
            this.props.getBtn1();
            this.props.getChatbuttons();
        }

    } 

    checkAction=()=>{
        let chat=this.props.chat_attr;
        let action=chat.action; 

        if(this.props.global.isTourActive && (action.type == 'si' && action.si == 'lp')){
            if(this.props.leftPanel.show){
                if(action.status == 'close'){
                    this.props.toggleLeftPanel();
                }
                this.props.chatTextFinished();
                return false;    
            }

            this.props.toggleLeftPanel();
            this.props.chatTextFinished();
        }
    }

}

const mapStateToProps=(state)=>{
    return {
        leftPanel:state.System_ui.leftPanel,
        leftButtons:state.leftpanelReducer.mainButtons,
        history:state.leftpanelReducer.history,
        chat_attr:state.robotReducer.chatAttributes,
        buttons:state.bottomButtomReducer.rowbotton2,
        global:state.GlobalReducer
    }
}
const mapDispatchToProps=(dispatch)=>{ 
    return {
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        toggleLeftPanel:()=>{ dispatch({type:"toggleleftPanel"}); },
        getBtn1:()=>{dispatch( leftPanel.getButton());},
        getChatbuttons:()=>{dispatch( leftPanel.getChatbuttons());}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(LeftPanel);