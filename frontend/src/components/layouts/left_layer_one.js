import React , { Component } from "react";
import { connect } from 'react-redux';
import leftPanel from "../../actions/leftpanelActions";
import Leftbtnskeleton from './leftBtnSkeleton';
import globalActions from '../../actions/globalActions';

import robotActions from '../../actions/robotActions';

class LeftLayerOne extends Component{
 
    
    render(){  
        // console.log(this.props.history);
        // console.log(this.props.currentButton);
        let currentButton=this.props.currentButton;
        let flag=this.checkAction(); 
        flag=(!flag) || !this.props.global.isTourActive;

        let chat=this.props.chat_attr;

        if(!flag || this.checkAction3()){
            setTimeout(()=>{
                this.props.chatTextFinished();
            },500);
        }

        this.checkScroll();

        const btns=this.props.leftButtons.map(item=>{

                return (
                    <li key={item.key}>
                        <a className={this.props.global.isTourActive && chat.action.button == item.id ? 'active': currentButton.id == item.id ? 'selected':''} onClick={(e)=> { if(this.props.global.isTourActive && chat.action.button != item.id) { return false ; } this.setMainButton(e , item.id,item.name); } } href="#">
                            {/* <i className="fas fa-church"></i> */}
                            <div style={ {backgroundImage : "url('"+item.icon+"')"}}  className="imageHolder"></div>
                            {/* <img src={item.icon}/>  */}
                            <span className="title">{item.name}</span> 
                        </a> 
                    </li>
                ) 
           
        });

       if(this.props.loaded){
           let remainder=btns.length < 9 ? 9 - btns.length : (btns.length % 3) == 0 ? 0 : 3 - (btns.length % 3) ;

           for(let c=0;c<remainder;c++){
               btns.push( <li className='emptyMatrixBox'> <a className='' href="#">  <div className={'imageHolder'}></div> <span></span> </a>  </li>);
           }
       }

        return(
            <div>
                <div className="matrixBox">
                    <ul id="btnHolder">
                        {btns.length == 0 ?  <Leftbtnskeleton /> : btns}
                    </ul>
                </div>
                <div className="matrixController">
                    <div className="inne">
                        <button className="btn" onClick={this.props.popHistory} disabled={(this.props.global.isTourActive || this.props.history.data.length == 0 )? true:false}><i className="fas fa-chevron-left"></i></button>
                        <button onClick={this.top} disabled={flag && this.props.global.isTourActive} className={!flag ? "btn ready":"btn "}><i className="fas fa-chevron-up"></i></button>
                        <button onClick={this.down} disabled={flag && this.props.global.isTourActive} className={!flag ? "btn ready":"btn "}><i className="fas fa-chevron-down"></i></button>
                    </div>
                </div>

                <div className="matrixSearch">
                    <div className="inne form-group">
                        <label><i className="fas fa-search"></i></label>
                        <input type="text" className="form-control" placeholder="search" />
                    </div>
                </div>

                <div className="matrixMenu">
                    <div className="inne">
                        <button disabled={this.checkContentAction()  || !currentButton.video_status} onClick={(e)=>{e.stopPropagation() ; e.cancelBubble=true; this.props.loadButtonContent(currentButton.videos); this.props.setContentType('videos') }} className={ this.makeButtonActive(currentButton.video_status , 'content_video' ,currentButton.videos) } id='content_video'>Videos</button>
                        <button disabled={this.checkContentAction() || !currentButton.article_status} onClick={(e)=>{e.stopPropagation() ; e.cancelBubble=true; this.props.loadButtonContent(currentButton.articles); this.props.setContentType('articles') }} className={ this.makeButtonActive(currentButton.article_status , 'content_article' ,currentButton.articles) }>Articles</button>
                        <button disabled={this.checkContentAction() || !currentButton.document_status} onClick={(e)=>{e.stopPropagation() ; e.cancelBubble=true; this.props.loadButtonContent(currentButton.documents); this.props.setContentType('documents')}} className={ this.makeButtonActive(currentButton.document_status , 'content_document' ,currentButton.documents) }>Documents</button>
                        <button disabled={this.checkContentAction() || !currentButton.image_status} onClick={(e)=>{e.stopPropagation() ; e.cancelBubble=true; this.props.loadButtonContent(currentButton.images); this.props.setContentType('images') }} className={ this.makeButtonActive(currentButton.image_status , 'content_image' ,currentButton.images) }>Photos</button>
                    </div>
                </div> 
            </div>
        )


    }

    componentDidMount(){
        // console.log(this.props);
    }
    autoClick(){
        let chat=this.props.chat_attr;
        let action=chat.action;
        if (this.props.global.isTourActive && action.type == 'si' && action.si =="click_button"){

        }
    }
    makeButtonActive(status , id_content , content){
        if(status && this.clickButton(id_content)){
            // this.props.loadButtonContent(content);
            return 'btn active clicked';
        }else if( status ) {
            return 'active btn'
        }else{
            return 'btn';
        }
    }
    setMainButton(e,id,name){
        let chat=this.props.chat_attr;
        if(this.props.global.isTourActive && chat.action.button == id){
            this.props.chatTextFinished();
            console.log('done');
        }
        this.props.setMainButton(id,name);
    }
    checkAction2(){
        let chat=this.props.chat_attr;
        let action=chat.action;

        return  (action.type == 'ua' && action.ua == 5 && action.ui=="lp");
    }
    checkAction3(){
        let chat=this.props.chat_attr;
        let action=chat.action;

        return  (this.props.global.isTourActive && action.type == 'si' && (action.si == 'content_buttom' || (action.si == 'click_button' && action.button=='content_video') ));
    }
    checkContentAction(){
            let chat=this.props.chat_attr;
            let action=chat.action;

            return  (this.props.global.isTourActive && (action.type != 'si' && action.si !="content_buttom" ));
    }
    clickButton(id_content){
        let chat=this.props.chat_attr;
        let action=chat.action;
        return  (this.props.global.isTourActive && (action.type == 'si' && action.si =="click_button" && action.button==id_content ));
    }
    down(){
        let btnHolder=document.getElementById('btnHolder');
        btnHolder.scrollTop+=50;
        // console.log(btnHolder.scrollTop);
    }
    top(){
        let btnHolder=document.getElementById('btnHolder');
        if(btnHolder.scrollTop <= 0){
            return false;
        }
        btnHolder.scrollTop-=50;
        // console.log(btnHolder.scrollTop);
    }
    checkAction(){
        let chat=this.props.chat_attr;
        let action=chat.action;

        return  (this.props.global.isTourActive && action.type == 'ua' && action.ua == 6);
    }
    checkButtonActions(){
        let chat=this.props.chat_attr;
        let action=chat.action;

        return  (this.props.global.isTourActive && action.type == 'ua' && action.ua == 5 && action.ui=="lp");
    }
    checkScroll(){
        let chat=this.props.chat_attr;
        let action=chat.action;

        //CHECK IF AUTOSCROLL IS REQUIRED
        //THEN IF IT REQUIRED SCROLL DOWN
        if(this.props.global.isTourActive && action.auto && action.auto == 'lp_scroll'){
            let btnHolder=document.getElementById('btnHolder');

            btnHolder.scrollTop=9999;
            let checker=btnHolder.scrollTop;
            btnHolder.scrollTop=0;

          let timer= setInterval(()=>{
                btnHolder.scrollTop+=5;
                if(checker == btnHolder.scrollTop){
                    clearInterval(timer);
                }
                // console.log(btnHolder.scrollTop);
            },10);
        }
    }
    
}

const mapPropsToState=(state)=>{
    return{
        leftButtons:state.leftpanelReducer.mainButtons,
        loaded:state.leftpanelReducer.loaded,
        currentButton:state.leftpanelReducer.currentButton,
        history:state.leftpanelReducer.history,
        chat_attr:state.robotReducer.chatAttributes, 
        global:state.GlobalReducer
    }
}


const mapDispatchToState=(dispatch)=>{
    return{
        chatTextFinished:(chat)=>{ dispatch(robotActions.chatTextFinished(chat)) },
        toggleBottomPanel:()=>{dispatch({'type':'toggleBottomPanel'}) },
        setMainButton:(data,name)=>{dispatch(leftPanel.setButton(data,name)); },
        popHistory:()=>{ dispatch(leftPanel.popHistory());},
        loadButtonContent:(content)=>{dispatch(leftPanel.loadButtonContent(content));},
        setContentType:(field)=>{dispatch({type:"set_content_type",field:field,value:true});}
    }
}
export default connect(mapPropsToState,mapDispatchToState)(LeftLayerOne);