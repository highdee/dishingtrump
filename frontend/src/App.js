import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import { BrowserRouter , Route , Switch} from "react-router-dom";
import Home from './components/pages/home';
import Settings from './components/pages/settings';
import donate from './components/pages/donate';
import Subscription from "./components/pages/accountSelection";
import Membership from "./components/pages/membership";
import VideoPlay from "./components/pages/videoplay";
import ViewBlog from "./components/pages/view-blog";
import MemoPage from "./components/pages/memopage";
import ViewDocument from "./components/pages/view-document";

import './assets/css/home.css';
import './assets/css/media.css';
import './assets/css/mdmedia.css';
import './assets/css/smmedia.css';
import './assets/css/xsmedia.css';


import EmailListing from './components/pages/email-listing';
import advertisement from './components/pages/advertisement';
import feeds from './components/pages/feeds';
import blog from './components/pages/blog';

class App extends Component {
  render() {
    return (
      <div id="Appp" className="App">
      
         <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path="/settings" component={Settings} />
                <Route path="/donate" component={donate} />
                <Route path="/subscription" component={Subscription} />
                <Route path="/membership" component={Membership} />
                <Route path="/email-listing" component={EmailListing} />
                <Route path="/advertisement" component={advertisement} />
                <Route path="/feeds" component={feeds} />
                <Route path="/blog" component={blog} />
                <Route path="/view-article/:button/:id/:title" component={ViewBlog} />
                <Route path="/video/:button/:id/:title" component={VideoPlay} />
                <Route path="/memo-page/:button/:id/:title" component={MemoPage} />
                <Route path="/view-document/:button/:id/:title" component={ViewDocument} />
            </Switch>
         </BrowserRouter>
      </div>
    );
  }

  componentDidMount=()=>{
    // document.getElementById('Appp').click();
  }
}

export default App;
