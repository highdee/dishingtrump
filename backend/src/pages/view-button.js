import React , { Component } from "react";
import MainButtonActions from "../actions/mainbuttonActions";
import ViewButtonActions from "../actions/viewButtonActions";
import {connect} from "react-redux";
import Sidebar from "./layouts/sidebar";
import Navbar from "./layouts/navbar";
import ErrorBar from "./layouts/errorbar";
import SuccessBar from "./layouts/successbar";
import EditButtonImage from "./layouts/edit-button-image";
import EditButtonVideo from "./layouts/edit-button-video";
import EditButtonDocument from "./layouts/edit-button-document";
import EditButtonArticle from "./layouts/edit-button-article";
import CreateSubButton from "./layouts/create-sub-button";
import auth from '../auth';



class ViewButton extends Component{

    state={
        publish:false,
        display_icon:false,
        publish_image:false,
        publish_video:false,
        publish_doc:false,
        display_video:false,
        display_article:false,
        docSelected:false
    };


    render(){
            let button=this.props.button;
            let icon=button.icon ? button.icon : button.title ;
            // console.log(button);
        return(
            <div className=""> 
                  <SuccessBar />
                  <ErrorBar />
                <div className="wrapper">
                     <Sidebar pagelink="view-button"/>
                     <div className="main-panel">
                        <Navbar pagename="VIEW BUTTON"/> 
                        <div className="content">
                          <div className="container-fluid">
                                <div className="row">
                                    <div className="col-lg-4 col-md-4 col-sm-4 cards">
                                        <div className="card card-pricing card-raised">
                                            <div className="card-body view-button " >
                                                <h4 className="card-category">{button.title}</h4>
                                                <div  className="card-icon icon-rose">

                                                    {/* <i className="material-icons">home</i> */}
                                                     <img className="icon-button" src={icon} style={{width:'100%',height:'100%'}} alt={''}/>
                                                </div>
                                                {/*<h3 className="card-title">$29</h3>*/}
                                                <div className="row">
                                                    <b className='col-sm-12 col-md-6'>Videos: {button.videos.length}</b>
                                                    <b className='col-sm-12 col-md-6'>Articles: {button.articles.length}</b>
                                                    <b className='col-sm-12 col-md-6'>Images: {button.images.length}</b>
                                                    <b className='col-sm-12 col-md-6'>Documents: {button.documents.length}</b>
                                                    <b className='col-sm-12 text-center'>Created: <span style={{color:'gray'}}>Nov 13, 2019</span></b>
                                                </div>
                                                {
                                                    button.publish == 1 ? <a href="" className="btn btn-rose btn-round">ACTIVE</a> : <a href="" className="btn btn-round">ACTIVE</a>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-8 cards">
                                        <div className="card card-raised">
                                            <div className="card-body view-button pb-0 pt-1">
                                                <div className="card ">
                                                    <div className="card-header card-header-rose card-header-icon">
                                                        <div className="card-icon icon-holder" onClick={this.trigger} style={{cursor:'pointer'}}>
                                                            {
                                                                !this.state.display_icon ? <i className="material-icons">mail_outline</i> : <img src="" id="iconImage"/>
                                                            }
                                                        </div>
                                                        <h4 className="card-title">Update Main Button</h4>
                                                    </div>
                                                    <div className="card-body ">
                                                        <form method="#" action="#" onSubmit={this.updateButton}>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating">Title</label>
                                                                <input type="text" value={button.title} onChange={this.updateTitle} className="form-control" id="title" />
                                                            </div>
                                                            <div className="form-group" style={{display:'none'}}>
                                                                <label htmlFor="icon" className="bmd-label-floating">Icon</label>
                                                                <input type="file" className="form-control" id="icon" onChange={this.readImage}/>
                                                            </div>
                                                            <div className="form-check text-left">
                                                                <div className="togglebutton">
                                                                    <label>
                                                                        <input type="checkbox" id="publish" checked={button.publish == 1 ? true:false}  onChange={this.publish} />
                                                                        <span className="toggle"></span>
                                                                        Publish
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div className="card-footer ">
                                                                <button disabled={this.props.requestObject.loading} type="submit" className="btn btn-fill btn-rose">Update</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row"> 
                                     <div className="col-lg-6 col-md-12">
                                        <div className="card">
                                            <div className="card-header card-header-tabs card-header-rose">
                                                <div className="nav-tabs-navigation">
                                                    <div className="nav-tabs-wrapper"> 
                                                        <b>BUTTON PHOTOS</b><br />
                                                        <ul className="nav nav-tabs" data-tabs="tabs">
                                                            <li className="nav-item">
                                                                <a className="nav-link active" href="#profile" data-toggle="tab">
                                                                    <i className="material-icons">image</i> All
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#messages" data-toggle="tab">
                                                                    <i className="material-icons">check_circle</i> Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#settings" data-toggle="tab">
                                                                    <i className="material-icons">delete</i> Not Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#create" data-toggle="tab">
                                                                    <i className="material-icons">add_circle</i> ADD NEW
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body" >
                                                <div className="tab-content" style={{height:'400px',overflowY:'scroll',padding:'10px'}}>
                                                    <div className="tab-pane active" id="profile">
                                                        <table className="table">
                                                        <tbody>
                                                        {
                                                            button.images.map((img,index)=>{
                                                              return (
                                                                  <tr key={index}>
                                                                      <td className='p-0'>
                                                                          <div className="form-check">
                                                                              <label className="form-check-label">
                                                                                  <input onChange={()=>{this.setPublishImage(button._id,img.id)}} className="form-check-input" type="checkbox" value="" checked={img.publish == 1? true:false} />
                                                                                  <span className="form-check-sign">
                                                                                       <span className="check"></span>
                                                                                  </span>
                                                                              </label>
                                                                          </div>
                                                                      </td>
                                                                      <td className='text-left pl-0'>
                                                                          <img id='display_image' style={{ width:'50px',height:'50px'}} src={ img.url != '' ? img.url: img.filename} alt=''/>
                                                                      </td>
                                                                      <td>{img.title} <span style={{fontSize:'9px',color:'gray',display:'block'}}>{img.date}</span></td>
                                                                      <td className="td-actions text-right" style={{display:'block'}}>
                                                                          <button type="button" rel="tooltip" data-target="#editbuttonimage" data-toggle="modal" onClick={()=>{ this.props.highlightImageEdit(button._id , img.id) }} title="Edit Image" className="btn mr-auto btn-primary btn-link btn-sm">
                                                                              <i className="material-icons">edit</i>
                                                                          </button>
                                                                          <button onClick={()=>{ this.deleteImage(button._id,img.id) }} type="button" rel="tooltip" title="Remove" className="btn mr-auto btn-danger btn-link btn-sm">
                                                                              <i className="material-icons">close</i>
                                                                          </button>
                                                                      </td>
                                                                  </tr>
                                                              )
                                                            })
                                                        }
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    <div className="tab-pane" id="messages">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.images.map((img,index)=>{
                                                                    if(img.publish == 1) {
                                                                        return (
                                                                            <tr key={index}>
                                                                                <td>
                                                                                    <div className="form-check">
                                                                                        <label
                                                                                            className="form-check-label">
                                                                                            <input

                                                                                                className="form-check-input"
                                                                                                type="checkbox" value=""
                                                                                                checked={img.publish == 1 ? true : false}/>
                                                                                            <span
                                                                                                className="form-check-sign">
                                                                        <span className="check"></span>
                                                                    </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td className='text-left pl-0'>
                                                                                    <img id='display_image' style={{ width:'50px',height:'50px'}} src={ img.url != '' ? img.url: img.filename} alt=''/>
                                                                                </td>
                                                                                <td>{img.title} <span style={{fontSize:'9px',color:'gray',display:'block'}}>{img.date}</span></td>
                                                                                <td className="td-actions text-right" style={{display:'block'}}>
                                                                                    <button type="button" rel="tooltip" data-target="#editbuttonimage" data-toggle="modal" onClick={()=>{ this.props.highlightImageEdit(button._id , img.id) }} title="Edit Image" className="btn mr-auto btn-primary btn-link btn-sm">
                                                                                        <i className="material-icons">edit</i>
                                                                                    </button>
                                                                                    <button onClick={()=>{ this.deleteImage(button._id,img.id) }} type="button" rel="tooltip" title="Remove" className="btn mr-auto btn-danger btn-link btn-sm">
                                                                                        <i className="material-icons">close</i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    }
                                                                })
                                                            }
                                                            </tbody>
                                                         </table>
                                                    </div>
                                                    <div className="tab-pane" id="settings">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.images.map((img)=>{
                                                                    if(img.publish == 0) {
                                                                        return (
                                                                            <tr>
                                                                                <td>
                                                                                    <div className="form-check">
                                                                                        <label
                                                                                            className="form-check-label">
                                                                                            <input onChange={()=>{this.setPublishImage(button._id,img.id)}}
                                                                                                className="form-check-input"
                                                                                                type="checkbox" value=""
                                                                                                checked={img.publish == 1 ? true : false}/>
                                                                                            <span
                                                                                                className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                             </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td className='text-left pl-0'>
                                                                                    <img id='display_image' style={{ width:'50px',height:'50px'}} src={ img.url != '' ? img.url: img.filename} alt=''/>
                                                                                </td>
                                                                                <td>{img.title} <span style={{fontSize:'9px',color:'gray',display:'block'}}>{img.date}</span></td>
                                                                                <td className="td-actions text-right" style={{display:'block'}}>
                                                                                    <button type="button" rel="tooltip" data-target="#editbuttonimage" data-toggle="modal" onClick={()=>{ this.props.highlightImageEdit(button._id , img.id) }} title="Edit Image" className="btn mr-auto btn-primary btn-link btn-sm">
                                                                                        <i className="material-icons">edit</i>
                                                                                    </button>
                                                                                    <button onClick={()=>{ this.deleteImage(button._id,img.id) }} type="button" rel="tooltip" title="Remove" className="btn mr-auto btn-danger btn-link btn-sm">
                                                                                        <i className="material-icons">close</i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    }
                                                                })
                                                            }
                                                            </tbody>
                                                    </table>
                                                    </div>
                                                    <div className="tab-pane" id="create">
                                                        <form method="#" action="#" onSubmit={this.addImage}>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating left"> PHOTO TITLE </label>
                                                                <input type="text" className="form-control" id="title_image" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="title" style={{left:'40%'}} className="bmd-label-floating"> PHOTO URL </label>
                                                                <div className="input-group mb-3">
                                                                    <div className="input-group-prepend">
                                                                        <button type="button" className="input-group-text" >
                                                                            { this.state.display_icon ? <i onClick={this.removeFile} className="material-icons">remove</i>: <i onClick={this.trigger2} className="material-icons">cloud_upload</i>}
                                                                        </button>
                                                                    </div>
                                                                    <input type='file' id="image" onChange={this.readImage}/>
                                                                    <input type="text" className="form-control pl-4" id="url" />
                                                                </div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label left"> PHOTO DESCRIPTION </label>
                                                                <input type="text" className="form-control" id="desc" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label left"> DATE </label>
                                                                <input type="datetime-local" className="form-control" id="date" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating left"> CREDIT </label>
                                                                <input type="text" className="form-control" id="credit" />
                                                            </div>
                                                            <div className="form-check text-left">
                                                                <label className="form-check-label">
                                                                    <input onClick={this.publish_image} className="form-check-input" type="checkbox" value="" id="publish" /> PUBLISH
                                                                        <span className="form-check-sign">
                                                                          <span className="check"></span>
                                                                        </span>
                                                                </label>
                                                            </div>

                                                            <button disabled={this.props.imageObject.loading} type="submit" className="btn btn-rose">Add</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-12">
                                        <div className="card">
                                            <div className="card-header card-header-tabs card-header-rose">
                                                <div className="nav-tabs-navigation">
                                                    <div className="nav-tabs-wrapper">
                                                        <b>BUTTON VIDEOS</b><br />
                                                        <ul className="nav nav-tabs" data-tabs="tabs">
                                                            <li className="nav-item">
                                                                <a className="nav-link active" href="#allvideos" data-toggle="tab">
                                                                    <i className="material-icons">video_library</i> All
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#allactive" data-toggle="tab">
                                                                    <i className="material-icons">check_circle</i> Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#allinactive" data-toggle="tab">
                                                                    <i className="material-icons">delete</i> Not Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#createvideo" data-toggle="tab">
                                                                    <i className="material-icons">add_circle</i> ADD NEW
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="tab-content" style={{height:'400px',overflowY:'scroll',padding:'10px'}}>
                                                    <div className="tab-pane active" id="allvideos">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.videos.map((video,index)=>{
                                                                    return (
                                                                        <tr key={index}>
                                                                            <td className='p-0'>
                                                                                <div className="form-check">
                                                                                    <label className="form-check-label">
                                                                                        <input onClick={()=>{ this.pubish_video_single(video.id) }} className="form-check-input" type="checkbox" value="" checked={video.publish == 1 ? true:false} />
                                                                                        <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td className='text-left pl-0'>
                                                                                {
                                                                                    video.code != '' ?
                                                                                             (<span className='d-block' style={{width:'50px',height:'50px',overflow:'hidden'}} dangerouslySetInnerHTML={{__html:video.code}}></span>)
                                                                                        :
                                                                                    video.url ?
                                                                                            (<video style={{width:'50px',height:'50px'}}  id='display_video' src={ video.url } ></video>)
                                                                                        :
                                                                                            (<video  style={{width:'50px',height:'50px'}} id='display_video' src={video.filename} ></video>)
                                                                                }
                                                                            </td>
                                                                            <td>{video.title}  <span style={{fontSize:'9px',color:'gray',display:'block'}}>{video.date}</span> </td>
                                                                            <td className="td-actions text-right" style={{display:'block'}}>
                                                                                <button type="button" rel="tooltip" title="Edit Video"  data-target="#editbuttonvideo" data-toggle="modal" onClick={()=>{this.props.highlightVideoEdit(video.id , button._id)}} className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                    <i className="material-icons">edit</i>
                                                                                </button>
                                                                                <button type="button" rel="tooltip" title="Remove Video" onClick={()=>{ this.deleteVideo(video.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                    <i className="material-icons">close</i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="allactive">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.videos.map((video,index)=>{
                                                                   if(video.publish == 1){
                                                                       return (
                                                                           <tr key={index}>
                                                                               <td>
                                                                                   <div className="form-check">
                                                                                       <label className="form-check-label">
                                                                                           <input onClick={()=>{ this.pubish_video_single(video.id) }} className="form-check-input" type="checkbox" value="" checked={video.publish == 1 ? true:false} />
                                                                                           <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                       </label>
                                                                                   </div>
                                                                               </td>
                                                                               <td>
                                                                                   {
                                                                                       video.code != '' ?
                                                                                           (<span className='d-block' style={{width:'50px',height:'50px',overflow:'hidden'}} dangerouslySetInnerHTML={{__html:video.code}}></span>)
                                                                                           :
                                                                                           video.url ?
                                                                                               (<video style={{width:'50px',height:'50px'}}  id='display_video' src={ video.url } ></video>)
                                                                                               :
                                                                                               (<video  style={{width:'50px',height:'50px'}} id='display_video' src={video.filename} ></video>)
                                                                                   }
                                                                               </td>
                                                                               <td>{video.title}  <span style={{fontSize:'9px',color:'gray',display:'block'}}>{video.date}</span></td>
                                                                               <td className="td-actions text-right" style={{display:'block'}}>
                                                                                   <button type="button" rel="tooltip" title="Edit Video"  data-target="#editbuttonvideo" data-toggle="modal" onClick={()=>{this.props.highlightVideoEdit(video.id , button._id)}}  className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                       <i className="material-icons">edit</i>
                                                                                   </button>
                                                                                   <button type="button" rel="tooltip" title="Remove" onClick={()=>{ this.deleteVideo(video.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                       <i className="material-icons">close</i>
                                                                                   </button>
                                                                               </td>
                                                                           </tr>
                                                                       )
                                                                   }
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="allinactive">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.videos.map((video,index)=>{
                                                                    if(video.publish == 0){
                                                                        return (
                                                                            <tr key={index}>
                                                                                <td>
                                                                                    <div className="form-check">
                                                                                        <label className="form-check-label">
                                                                                            <input onClick={()=>{ this.pubish_video_single(video.id) }} className="form-check-input" type="checkbox" value="" checked={video.publish == 1 ? true:false} />
                                                                                            <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        video.code != '' ?
                                                                                            (<span className='d-block' style={{width:'50px',height:'50px',overflow:'hidden'}} dangerouslySetInnerHTML={{__html:video.code}}></span>)
                                                                                            :
                                                                                            video.url ?
                                                                                                (<video style={{width:'50px',height:'50px'}}  id='display_video' src={ video.url } ></video>)
                                                                                                :
                                                                                                (<video  style={{width:'50px',height:'50px'}} id='display_video' src={video.filename} ></video>)
                                                                                    }
                                                                                </td>
                                                                                <td>{video.title}  <span style={{fontSize:'9px',color:'gray',display:'block'}}>{video.date}</span></td>
                                                                                <td className="td-actions text-right" style={{display:'block'}}>
                                                                                    <button type="button" rel="tooltip" title="Edit Video"  data-target="#editbuttonvideo" data-toggle="modal" onClick={()=>{this.props.highlightVideoEdit(video.id , button._id)}}  className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">edit</i>
                                                                                    </button>
                                                                                    <button type="button" rel="tooltip" title="Remove" onClick={()=>{ this.deleteVideo(video.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">close</i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    }
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="createvideo">
                                                         <form method="#" action="#" onSubmit={this.createVideo}>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating left"> VIDEO TITLE </label>
                                                                <input type="text" className="form-control" id="video_title" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="title" style={{left:'40%'}} className=""> VIDEO FILE </label>
                                                                <div className="input-group mb-3">
                                                                    <div className="input-group-prepend">
                                                                        <button type="button" className="input-group-text" >
                                                                            { this.state.display_video ? <i onClick={this.removeFileVideo} className="material-icons">remove</i>: <i onClick={this.triggerVideo} className="material-icons">cloud_upload</i>}
                                                                        </button>
                                                                    </div>
                                                                    <input type='file' id="video" onChange={this.readVideo}/>
                                                                    <input type="text" className="form-control pl-4" id="video_file" />
                                                                </div>
                                                            </div>
                                                             <div className="form-group">
                                                                 <label className="bmd-label-floating left"> VIDEO URL </label>
                                                                 <input type="text" className="form-control" id="video_url" />
                                                             </div>
                                                             <div className="form-group mt-2">
                                                                 <label  className="bmd-label left mb-2"> VIDEO DATE </label>
                                                                 <input type="datetime-local" className="form-control" id="video_date" />
                                                             </div>
                                                             <div className="form-group">
                                                                 <label  className="bmd-label col-12 left text-left p-0"> VIDEO EMBEDED CODE </label>
                                                                 <textarea className='textarea edit-100' id="video_code" style={{width:'100%'}}></textarea>
                                                             </div>

                                                             <div className="form-group">
                                                                 <label  className="bmd-label col-12 left text-left p-0"> VIDEO TRANSCRIPT </label>
                                                                 <textarea className='textarea edit-100' id='video_transcript' style={{width:'100%'}}></textarea>
                                                             </div>
                                                             <div className="form-group">
                                                                 <label  className="bmd-label col-12 left text-left p-0"> VIDEO DESCRIPTION </label>
                                                                 <textarea className='textarea edit-100' id='video_desc' style={{width:'100%'}}></textarea>
                                                             </div>
                                                             <div className="form-check text-left">
                                                                <label className="form-check-label">
                                                                    <input onClick={this.publish_video} className="form-check-input" type="checkbox" value="" id="video_publish" /> PUBLISH
                                                                    <span className="form-check-sign">
                                                                          <span className="check"></span>
                                                                        </span>
                                                                </label>
                                                            </div>

                                                            <button disabled={this.props.videoObject.loading} type="submit" className="btn btn-rose">Add</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-12">
                                        <div className="card">
                                            <div className="card-header card-header-tabs card-header-rose">
                                                <div className="nav-tabs-navigation">
                                                    <div className="nav-tabs-wrapper">
                                                        <b>BUTTON DOCUMENTS</b><br />
                                                        <ul className="nav nav-tabs" data-tabs="tabs">
                                                            <li className="nav-item">
                                                                <a className="nav-link active" href="#allDoc" data-toggle="tab">
                                                                    <i className="material-icons">attach_file</i> All
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#allactiveDoc" data-toggle="tab">
                                                                    <i className="material-icons">check_circle</i> Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#allinactiveDoc" data-toggle="tab">
                                                                    <i className="material-icons">delete</i> Not Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#createDoc" data-toggle="tab">
                                                                    <i className="material-icons">add_circle</i> ADD NEW
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="tab-content" style={{height:'400px',overflowY:'scroll',padding:'10px'}}>
                                                    <div className="tab-pane active" id="allDoc">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.documents.map((doc,index)=>{
                                                                    return (
                                                                        <tr key={index}>
                                                                            <td>
                                                                                <div className="form-check">
                                                                                    <label className="form-check-label">
                                                                                        <input onClick={()=>{ this.pubish_document(doc.id) }} className="form-check-input" type="checkbox" value="" checked={doc.publish == 1 ? true:false} />
                                                                                        <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td className='text-left pl-0'> {doc.title} <span style={{fontSize:'9px',color:'gray',display:'block'}}>{doc.url}</span></td>
                                                                            <td className="td-actions text-right" style={{display:'block'}}>
                                                                                <button type="button" rel="tooltip" title="Edit Video"  data-target="#editbuttondocument" data-toggle="modal" onClick={()=>{this.props.highlightDocEdit(doc.id , button._id)}} className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                    <i className="material-icons">edit</i>
                                                                                </button>
                                                                                <button type="button" rel="tooltip" title="Remove Video" onClick={()=>{ this.deleteDoc(doc.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                    <i className="material-icons">close</i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="allactiveDoc">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.documents.map((doc,index)=>{
                                                                    if(doc.publish == 1){
                                                                        return  (
                                                                            <tr key={index}>
                                                                                <td>
                                                                                    <div className="form-check">
                                                                                        <label className="form-check-label">
                                                                                            <input onClick={()=>{ this.pubish_document(doc.id) }} className="form-check-input" type="checkbox" value="" checked={doc.publish == 1 ? true:false} />
                                                                                            <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td className='text-left pl-0'> {doc.title} <span style={{fontSize:'9px',color:'gray',display:'block'}}>{doc.url}</span></td>
                                                                                <td className="td-actions text-right" style={{display:'block'}}>
                                                                                    <button type="button" rel="tooltip" title="Edit Video"  data-target="#editbuttondocument" data-toggle="modal" onClick={()=>{this.props.highlightDocEdit(doc.id , button._id)}} className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">edit</i>
                                                                                    </button>
                                                                                    <button type="button" rel="tooltip" title="Remove Video" onClick={()=>{ this.deleteDoc(doc.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">close</i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    }else{
                                                                        return (<tr></tr>);
                                                                    }
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="allinactiveDoc">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.documents.map((doc,index)=>{
                                                                    if(doc.publish == 0){
                                                                        return  (
                                                                            <tr key={index}>
                                                                                <td>
                                                                                    <div className="form-check">
                                                                                        <label className="form-check-label">
                                                                                            <input onClick={()=>{ this.pubish_document(doc.id) }} className="form-check-input" type="checkbox" value="" checked={doc.publish == 1 ? true:false} />
                                                                                            <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td className='text-left pl-0'> {doc.title} <span style={{fontSize:'9px',color:'gray',display:'block'}}>{doc.url}</span></td>
                                                                                <td className="td-actions text-right" style={{display:'block'}}>
                                                                                    <button type="button" rel="tooltip" title="Edit Video"  data-target="#editbuttondocument" data-toggle="modal" onClick={()=>{this.props.highlightDocEdit(doc.id , button._id)}} className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">edit</i>
                                                                                    </button>
                                                                                    <button type="button" rel="tooltip" title="Remove Video" onClick={()=>{ this.deleteDoc(doc.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">close</i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    }else{
                                                                        return (<tr key={index}></tr>);
                                                                    }
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="createDoc">
                                                        <form method="#" action="#" onSubmit={this.createDoc}>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating left"> DOCUMENT TITLE </label>
                                                                <input type="text" className="form-control" id="doc_title" />
                                                            </div>

                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating left"> DOCUMENT URL </label>
                                                                <input type="text" className="form-control" id="doc_url" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating left"> AUTHOR </label>
                                                                <input type="text" className="form-control" id="doc_author" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label left"> Date </label>
                                                                <input type="datetime-local" className="form-control" id="doc_date" />
                                                            </div>
                                                            <div className="form-check text-left">
                                                                <label className="form-check-label">
                                                                    <input onClick={this.publish_doc} className="form-check-input" type="checkbox" value="" id="doc_publish" /> PUBLISH
                                                                    <span className="form-check-sign">
                                                                          <span className="check"></span>
                                                                        </span>
                                                                </label>
                                                            </div>

                                                            <button disabled={this.props.docObject.loading} type="submit" className="btn btn-rose">Add</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-12">
                                        <div className="card">
                                            <div className="card-header card-header-tabs card-header-rose">
                                                <div className="nav-tabs-navigation">
                                                    <div className="nav-tabs-wrapper">
                                                        <b>BUTTON ARTICLES</b><br />
                                                        <ul className="nav nav-tabs" data-tabs="tabs">
                                                            <li className="nav-item">
                                                                <a className="nav-link active" href="#allarticles" data-toggle="tab">
                                                                    <i className="material-icons">video_library</i> All
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#allactivearticles" data-toggle="tab">
                                                                    <i className="material-icons">check_circle</i> Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#allinactivearticles" data-toggle="tab">
                                                                    <i className="material-icons">delete</i> Not Active
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                            <li className="nav-item">
                                                                <a className="nav-link" href="#createarticle" data-toggle="tab">
                                                                    <i className="material-icons">add_circle</i> ADD NEW
                                                                    <div className="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="tab-content" style={{height:'400px',overflowY:'scroll',padding:'10px'}}>
                                                    <div className="tab-pane active" id="allarticles">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.articles.map((art,index)=>{
                                                                    return (
                                                                        <tr key={index}>
                                                                            <td className='p-0'>
                                                                                <div className="form-check">
                                                                                    <label className="form-check-label">
                                                                                        <input onClick={()=>{ this.pubish_article(art.id) }} className="form-check-input" type="checkbox" value="" checked={art.publish == 1 ? true:false} />
                                                                                        <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td className='text-left pl-0'>
                                                                                {
                                                                                    art.code != '' ?
                                                                                        (<span className='d-block' style={{width:'50px',height:'50px',overflow:'hidden'}} dangerouslySetInnerHTML={{__html:art.code}}></span>)
                                                                                        :
                                                                                        art.url ?
                                                                                            (<video style={{width:'50px',height:'50px'}}  id='display_video' src={ art.url } ></video>)
                                                                                            :
                                                                                            (<video  style={{width:'50px',height:'50px'}} id='display_video' src={art.filename} ></video>)
                                                                                }
                                                                            </td>
                                                                            <td>{art.title}  <span style={{fontSize:'9px',color:'gray',display:'block'}}>{art.date}</span> </td>
                                                                            <td className="td-actions text-right" style={{display:'block'}}>
                                                                                <button type="button" rel="tooltip" title="Edit Article"  data-target="#editbuttonArticle" data-toggle="modal" onClick={()=>{this.props.highlightArticleEdit(art.id)}} className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                    <i className="material-icons">edit</i>
                                                                                </button>
                                                                                <button type="button" rel="tooltip" title="Remove Article" onClick={()=>{ this.deleteArticle(art.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                    <i className="material-icons">close</i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="allactivearticles">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.articles.map((art,index)=>{
                                                                   if(art.publish == 1){
                                                                       return (
                                                                           <tr key={index}>
                                                                               <td className='p-0'>
                                                                                   <div className="form-check">
                                                                                       <label className="form-check-label">
                                                                                           <input onClick={()=>{ this.pubish_article(art.id) }} className="form-check-input" type="checkbox" value="" checked={art.publish == 1 ? true:false} />
                                                                                           <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                       </label>
                                                                                   </div>
                                                                               </td>
                                                                               <td className='text-left pl-0'>
                                                                                   {
                                                                                       art.code != '' ?
                                                                                           (<span className='d-block' style={{width:'50px',height:'50px',overflow:'hidden'}} dangerouslySetInnerHTML={{__html:art.code}}></span>)
                                                                                           :
                                                                                           art.url ?
                                                                                               (<video style={{width:'50px',height:'50px'}}  id='display_video' src={ art.url } ></video>)
                                                                                               :
                                                                                               (<video  style={{width:'50px',height:'50px'}} id='display_video' src={art.filename} ></video>)
                                                                                   }
                                                                               </td>
                                                                               <td>{art.title}  <span style={{fontSize:'9px',color:'gray',display:'block'}}>{art.date}</span> </td>
                                                                               <td className="td-actions text-right" style={{display:'block'}}>
                                                                                   <button type="button" rel="tooltip" title="Edit Article"  data-target="#editbuttonArticle" data-toggle="modal" onClick={()=>{this.props.highlightArticleEdit(art.id)}} className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                       <i className="material-icons">edit</i>
                                                                                   </button>
                                                                                   <button type="button" rel="tooltip" title="Remove Article" onClick={()=>{ this.deleteArticle(art.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                       <i className="material-icons">close</i>
                                                                                   </button>
                                                                               </td>
                                                                           </tr>
                                                                       )
                                                                    }
                                                                   else{
                                                                       return(<tr key={index}></tr>)
                                                                   }

                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="allinactivearticles">
                                                        <table className="table">
                                                            <tbody>
                                                            {
                                                                button.articles.map((art,index)=>{
                                                                    if(art.publish == 0){
                                                                        return (
                                                                            <tr key={index}>
                                                                                <td className='p-0'>
                                                                                    <div className="form-check">
                                                                                        <label className="form-check-label">
                                                                                            <input onClick={()=>{ this.pubish_article(art.id) }} className="form-check-input" type="checkbox" value="" checked={art.publish == 1 ? true:false} />
                                                                                            <span className="form-check-sign">
                                                                                            <span className="check"></span>
                                                                                        </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td className='text-left pl-0'>
                                                                                    {
                                                                                        art.code != '' ?
                                                                                            (<span className='d-block' style={{width:'50px',height:'50px',overflow:'hidden'}} dangerouslySetInnerHTML={{__html:art.code}}></span>)
                                                                                            :
                                                                                            art.url ?
                                                                                                (<video style={{width:'50px',height:'50px'}}  id='display_video' src={ art.url } ></video>)
                                                                                                :
                                                                                                (<video  style={{width:'50px',height:'50px'}} id='display_video' src={art.filename} ></video>)
                                                                                    }
                                                                                </td>
                                                                                <td>{art.title}  <span style={{fontSize:'9px',color:'gray',display:'block'}}>{art.date}</span> </td>
                                                                                <td className="td-actions text-right" style={{display:'block'}}>
                                                                                    <button type="button" rel="tooltip" title="Edit Article"  data-target="#editbuttonArticle" data-toggle="modal" onClick={()=>{this.props.highlightArticleEdit(art.id)}} className="btn btn-primary btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">edit</i>
                                                                                    </button>
                                                                                    <button type="button" rel="tooltip" title="Remove Article" onClick={()=>{ this.deleteArticle(art.id); }} className="btn btn-danger btn-link btn-sm mr-auto">
                                                                                        <i className="material-icons">close</i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    }
                                                                    else{
                                                                        return(<tr key={index}></tr>)
                                                                    }

                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="tab-pane" id="createarticle">
                                                        <form method="#" action="#" onSubmit={this.createArticle}>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating left"> ARTICLE TITLE </label>
                                                                <input type="text" className="form-control" id="article_title" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label style={{left:'40%'}} className=""> ARTICLE IMAGE </label>
                                                                <div className="input-group mb-3">
                                                                    <div className="input-group-prepend">
                                                                        <button type="button" className="input-group-text" >
                                                                            <i onClick={this.triggerArticle} className="material-icons">cloud_upload</i>
                                                                        </button>
                                                                    </div>
                                                                    <input type='file' id="article" onChange={this.readArticleImage}/>
                                                                    <input type="text" className="form-control pl-4" id="article_file" disabled />
                                                                </div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="bmd-label-floating left"> ARTICLE URL </label>
                                                                <input type="text" className="form-control" id="article_url" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="bmd-label-floating left"> ARTICLE AUTHOR </label>
                                                                <input type="text" className="form-control" id="article_author" />
                                                            </div>
                                                            <div className="form-group mt-2">
                                                                <label  className="bmd-label left mb-2"> ARTICLE DATE </label>
                                                                <input type="datetime-local" className="form-control" id="article_date" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label  className="bmd-label col-12 left text-left p-0"> PARAGRAPH </label>
                                                                <textarea className='textarea edit-200' id="article_paragraph" style={{width:'100%'}}></textarea>
                                                            </div>
                                                            <div className="form-group">
                                                                <label  className="bmd-label col-12 left text-left p-0"> DESCRIPTION </label>
                                                                <textarea className='textarea edit-200' id="article_desc" style={{width:'100%'}}></textarea>
                                                            </div>
                                                            <div className="form-check text-left">
                                                                <label className="form-check-label">
                                                                    <input onClick={this.publish_video} className="form-check-input" type="checkbox" value="" id="video_publish" /> PUBLISH
                                                                    <span className="form-check-sign">
                                                                          <span className="check"></span>
                                                                        </span>
                                                                </label>
                                                            </div>

                                                            <button disabled={this.props.articleObject.loading} type="submit" className="btn btn-rose">Add</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                              <div>
                                  <div className="col-lg-12 col-md-12">
                                      <div className="card">
                                          <div className="card-header card-header-tabs card-header-rose">
                                              <div className="nav-tabs-navigation">
                                                  <div className="nav-tabs-wrapper">
                                                      <b>SUB BUTTONS</b><br />

                                                  </div>
                                              </div>
                                          </div>
                                          <div className="card-body">
                                              <div className="tab-content" style={{height:'400px',overflowY:'scroll',padding:'10px'}}>
                                                  <ul className="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center"
                                                      role="tablist">
                                                      <li key={'index'} style={{margin:'10px 20px',width:'100px',height:'200px !important',padding:'5px 0px'}} className="nav-item d-block">
                                                          <a href="#createSubButton" data-toggle="modal"  className="nav-link active subbuttons" style={{height:'100%',background:'lightgray'}}
                                                             role="tablist">
                                                              <i className="material-icons">add</i>
                                                               create
                                                          </a>
                                                      </li>
                                                      {
                                                         button.subbutton.length == 0 ?
                                                             (<div style={{width:'100%',height:'fit-content'}}><b className="text-danger">No sub buttons yet</b></div>)
                                                          :
                                                          button.subbutton.map((btn,index)=>{
                                                          return (
                                                          <li key={index} style={{margin:'10px 20px',width:'100px',height:'200px !important',padding:'5px 0px'}} className="nav-item">

                                                              <a onClick={()=>{window.location='/view-button/'+btn._id}} className="nav-link active p-0 pt-2 pl-2 pr-2 pb-2 subbuttons" style={{height:'100%',background:'#dd2667',lineHeight:'1.2',position:'relative'}}
                                                              role="tablist">

                                                              <div className='mb-2' style={{width:'100%',height:'50px'}}>
                                                                  <img alt={""} className="icon-button" src={btn.icon} style={{width:'100%',height:'100%'}}/>
                                                              </div>

                                                              {btn.title}
                                                                  <a className='deleteSub'>
                                                                      <span  onClick={(e)=>{  e.stopPropagation() ; this.props.deleteSubButton(btn._id) ;return false;}} className="material-icons">cancel</span>
                                                                  </a>
                                                              </a>
                                                          </li>
                                                          )
                                                      })
                                                      }
                                                  </ul>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <EditButtonVideo />
                <EditButtonImage />
                <EditButtonDocument />
                <EditButtonArticle />
                <CreateSubButton />

            </div>

        )
    }
 
    componentDidMount(){
      let id=this.props.computedMatch.params.id;
      this.props.getButton(id);
        this.props.getSubButton(id);
    }
    updateTitle=()=>{
        let title=document.getElementById('title');
        this.props.changeTitle(title.value,this.state.publish);
    }
    trigger=()=>{
        document.getElementById('icon').click();
    }
    trigger2=()=>{
        document.getElementById('image').click();
    }
    triggerVideo=()=>{
        document.getElementById('video').click();
    }
    removeFileVideo=()=>{
        this.setState(()=>{
            return {
                ...this.state,
                display_video:false
            }
        });

        document.getElementById("video_file").value='';

    }
    readVideo=(e)=>{
        let file=e.target.files[0];
        // console.log(file);
        this.setState(()=>{
            return {
                ...this.state,
                display_video:true
            }
        });
        document.getElementById("video_file").value=file.name;

    }

    addImage=(e)=>{
        e.preventDefault();

        if(this.props.imageObject.loading){
            return false;
        }

        let title=document.getElementById('title_image');
        let credit=document.getElementById('credit');
        let desc=document.getElementById('desc');
        let icon=document.getElementById('image');
        let publish=document.getElementById('publish');
        let date=document.getElementById('date');
        let url=document.getElementById('url');

        let id=this.props.button._id;
        let payload;

        if(url.value.trim()=="" && !this.state.display_icon){
            alert("No url or file was selected.");
            return false;
        }


        if(icon.files.length != 0 && this.state.display_icon){
             payload=new FormData();
                console.log('files');
            payload.append('title',title.value);
            payload.append('credit',credit.value);
            payload.append('desc',desc.value);
            payload.append('date',date.value);
            payload.append('publish',this.state.publish_image);
            payload.append('id',id);
            payload.append('image',icon.files[0]);
            payload.append('imageurl',url.value);

            this.props.addImage(payload ,id,auth.getAuth().data.token);

        }else{
            payload=new FormData();

            payload.append('title',title.value);
            payload.append('credit',credit.value);
            payload.append('desc',desc.value);
            payload.append('date',date.value);
            payload.append('publish',this.state.publish_image);
            payload.append('id',id);
            payload.append('image',icon.files[0]);
            payload.append('imageurl',url.value);

            this.props.addImage(payload ,id,auth.getAuth().data.token);
        }

    }
    removeFile=()=>{
        this.setState(()=>{
            return {
                ...this.state,
                display_icon:false
            }
        });

        let icon=document.getElementById('image');
        let url=document.getElementById('url');
        url.value='';

    }
    readImage=(e)=>{
        let el=e.target;
        if(el.files.length == 0){
            return false;
        }
        let files=el.files[0];
        // console.log(files);
        if(!(files.type =='image/jpeg' || files.type =='image/jpg' || files.type =='image/png')){
            alert("file format not supported");
            return false;
        }
        let url=document.getElementById('url');
        url.value=files.name;
        this.setState(()=>{
            return {
                ...this.state,
                display_icon: true
            }
        });

        let filer=new FileReader();
        filer.onload=(result)=>{

            document.getElementById('iconImage').src=result.srcElement.result;
        }
        filer.readAsDataURL(files);
    }
    publish=()=>{
        this.setState(()=>{
            return{
                ...this.state,
                publish:!this.state.publish
            }
        });
        let title=document.getElementById('title');
        this.props.changeTitle(title.value,!this.state.publish);
    }
    publish_image=()=>{
        this.setState(()=>{
            return{
                ...this.state,
                publish_image:!this.state.publish_image
            }
        });
        let title=document.getElementById('title');
        this.props.changeTitle(title.value,!this.state.publish);
    }
    updateButton=(e)=>{
        e.preventDefault();

        if(this.state.loading){
            return false;
        }

        let formdata=new FormData();
        let icon=document.getElementById('icon');
        let title=document.getElementById('title');
        let publish=this.state.publish;

        if(title.value.trim() == ''){
            alert('Main buttons need to have a title');
            return false;
        }

        let data={id:this.props.button._id , title:title.value,publish:publish?1:0};
        let iconFlag=false;

        if(icon.files.length !=0){
            formdata.append('title',title.value);
            formdata.append('publish',publish ? 1:0);
            formdata.append('icon',icon.files[0]);
            iconFlag=true;
        }

        this.props.updateButton(data,formdata,iconFlag) ;

        if(iconFlag){
            // document.getElementById('iconImage').src='';
        }
    }
    setPublishImage=(button,image)=>{
        this.props.setPublishImage(button , image);
    }
    deleteImage=(button,image)=>{
        if(! window.confirm("Are you sure you want to remove this image")){
            return false;
        }
        this.props.deleteImage(button , image);
    }

    deleteVideo=(video)=>{
        if(! window.confirm("Are you sure you want to remove this image")){
            return false;
        }
        this.props.deleteVideo(this.props.button._id , video);
    }
    pubish_video_single=(id)=>{
        this.props.setPublishVideo(this.props.button._id,id);
    }
    publish_video=()=>{
        this.setState(()=>{
            return{
                ...this.state,
                publish_video:!this.state.publish_video
            }
        });
        let title=document.getElementById('title');
        this.props.changeTitle(title.value,!this.state.publish);
    }
    createVideo=(e)=>{
        e.preventDefault();

        let video_title=document.getElementById('video_title');
        let video_url=document.getElementById('video_url');
        let video_code=document.getElementById('video_code');
        let video_transcript=document.getElementById('video_transcript');
        let video_desc=document.getElementById('video_desc');
        let video_file=document.getElementById('video');
        let video_date=document.getElementById('video_date');

            if(video_title.value.trim() == '' || (video_code.value == '' && video_url.value ==''  && video_file.value =='')){
                alert('video title and embeded code is needed to create a video');
                return false;
            }

            let id=this.props.button._id;

            let formdata=new FormData();
            formdata.append('title',video_title.value);
            formdata.append('code',video_code.value);
            formdata.append('transcript',video_transcript.value);
             formdata.append('description',video_desc.value);
             formdata.append('url',video_url.value);
            formdata.append('publish',this.state.publish_video);
            formdata.append('date',video_date.value);

            if(this.state.display_video){
                console.log('files');
                formdata.append('video' , video_file.files[0]);
            }
            this.props.addVideo(formdata , id , auth.getAuth().data.token);

        }

    publish_doc=()=>{
        this.setState(()=>{
            return{
                ...this.state,
                publish_doc:!this.state.publish_doc
            }
        });
    }
    createDoc=(e)=>{
        e.preventDefault();

        let doc_title=document.getElementById('doc_title');
        let doc_url=document.getElementById('doc_url');
        let doc_author=document.getElementById('doc_author');
        let doc_date=document.getElementById('doc_date');

        if(doc_title.value.trim() == '' || doc_url.value == '' ){
            alert('Document title and URl code is needed to create a document');
            return false;
        }

        let id=this.props.button._id;

        let formdata={
            'title':doc_title.value,
            'url':doc_url.value,
            'publish':this.state.publish_doc,
            'author':doc_author.value,
            'date':doc_date.value
        };

        this.props.addDoc(formdata , id , auth.getAuth().data.token);

    }
    deleteDoc=(doc)=>{
        if(! window.confirm("Are you sure you want to remove this Document")){
            return false;
        }
        this.props.deleteDoc(this.props.button._id , doc);
    }
    pubish_document=(id)=>{
        this.props.setPublishDoc(this.props.button._id,id);
    }

    createArticle=(e)=>{
        e.preventDefault();

        let title=document.getElementById('article_title');
        let url=document.getElementById('article_url');
        let author=document.getElementById('article_author');
        let date=document.getElementById('article_date');
        let paragraph=document.getElementById('article_paragraph');
        let desc=document.getElementById('article_desc');
        let article=document.getElementById('article');


        if(title.value.trim() == '' || url.value.trim()==""){
            alert('Article title and url is needed to create an Article');
            return false;
        }

        let id=this.props.button._id;

        let formdata=new FormData();
        formdata.append('title',title.value);
        formdata.append('url',url.value);
        formdata.append('author',author.value);
        formdata.append('date',date.value);
        formdata.append('paragraph',paragraph.value);
        formdata.append('description',desc.value);

        formdata.append('image' , article.files[0]);

        this.props.addArticle(formdata , id , auth.getAuth().data.token);

    }
    triggerArticle=()=>{
        document.getElementById('article').click();
    }
    readArticleImage=(e)=>{
        let el=e.target;
        if(el.files.length == 0){
            return false;
        }
        let files=el.files[0];
        console.log(files);
        if(!(files.type =='image/jpeg' || files.type =='image/jpg' || files.type =='image/png')){
            alert("file format not supported");
            return false;
        }
        let url=document.getElementById('article_file');
        url.value=files.name;

    }
    deleteArticle=(art)=>{
        if(! window.confirm("Are you sure you want to remove this Article")){
            return false;
        }
        this.props.deleteArticle(this.props.button._id , art);
    }
    pubish_article=(id)=>{
        this.props.setPublishArticle(this.props.button._id,id);
    }
}

const mapState=(state)=>{
  return{
      button:state.ViewButtonReducer.button,
      admin:state.AdminReducer,
      requestObject:state.AdminReducer.mainButtonRequestObject,
      imageObject:state.ViewButtonReducer.image,
      videoObject:state.ViewButtonReducer.video,
      articleObject:state.ViewButtonReducer.article,
      docObject:state.ViewButtonReducer.doc,
      highlightImage:state.ViewButtonReducer.editting
  }
}
const mapProps=(dispatch)=>{
  return {
      getButton:(id)=>{ dispatch(ViewButtonActions.getButton(id)); },
      getSubButton:(id)=>{ dispatch(ViewButtonActions.getButtonSubButtons(id)); },
      publishButton:(id)=>{ dispatch(MainButtonActions.publishButton(id)); },
      updateButton:(data,formdata,iconFlag)=>{ dispatch(ViewButtonActions.updatemainButton(data,formdata,iconFlag)); },
      changeTitle:(title,publish)=>{ dispatch({type:'set_title', title:title,publish:publish}); },
      deleteSubButton:(id)=>{ dispatch(MainButtonActions.deleteSubButton(id)); },

      addImage:(data,id,token)=>{ dispatch(ViewButtonActions.addImage(data,id,token)); },
      addVideo:(data,id,token)=>{ dispatch(ViewButtonActions.addVideo(data,id,token)); },
      setPublishImage:(button,image)=>{ dispatch(ViewButtonActions.setPublishImage(button,image)); },
      setPublishVideo:(button,video)=>{ dispatch(ViewButtonActions.setPublishVideo(button,video)); },
      deleteImage:(button,image)=>{ dispatch(ViewButtonActions.deleteImage(button,image)); },
      deleteVideo:(button,video)=>{ dispatch(ViewButtonActions.deleteVideo(button,video)); },
      highlightImageEdit:(id,image)=>{ dispatch(ViewButtonActions.highlightImageEdit(id,image))},
      highlightVideoEdit:(id,video)=>{ dispatch(ViewButtonActions.highlightVideoEdit(id,video))},
      highlightDocEdit:(id,video)=>{ dispatch(ViewButtonActions.highlightDocEdit(id,video))},

      setPublishDoc:(button,doc)=>{ dispatch(ViewButtonActions.setPublishDoc(button,doc)); },
      deleteDoc:(button,video)=>{ dispatch(ViewButtonActions.deleteDoc(button,video)); },
      addDoc:(data,id,token)=>{ dispatch(ViewButtonActions.addDoc(data,id,token)); },

      addArticle:(data,id,token)=>{ dispatch(ViewButtonActions.addArticle(data,id,token)); },
      deleteArticle:(button,art)=>{ dispatch(ViewButtonActions.deleteArticle(button,art)); },
      setPublishArticle:(button,art)=>{ dispatch(ViewButtonActions.setPublishArticle(button,art)); },
      highlightArticleEdit:(id)=>{ dispatch(ViewButtonActions.highlightArticleEdit(id))},
  }
}
export default connect(mapState,mapProps)(ViewButton);