import  React , { Component } from 'react';
import { connect } from 'react-redux';
import Sidebar from "./layouts/sidebar";
import Navbar from "./layouts/navbar";
import ErrorBar from "./layouts/errorbar";
import SuccessBar from "./layouts/successbar";
import ChatButtonActions from "../actions/chatbutton";
import { NavLink } from 'react-router-dom';

class ChatButton extends  Component{

    render(){
        let buttons=this.props.buttons;
        console.log(buttons);
        let button_table=buttons.map((btn,index)=>{
            return (
                <tr key={index} role='row' className="odd">
                    <td>{index+1}</td>
                    {/*<td className="Buttonicon"><img src={btn.icon} alt=""/></td>*/}
                    <td className="text-center">{btn.title}</td>
                    <td className="td-actions text-right">
                        <button onClick={()=>{ this.publishChatButtons(btn) }}  type="button" rel="tooltip" className={'btn ' + (btn.publish == 1 ? 'btn-info':'btn-danger') }>
                            { btn.publish == 1 ?  (<i className="material-icons">visibility</i>) : (<i className="material-icons">visibility_off</i>)}
                        </button>&nbsp;
                        <NavLink to={"/view-chat-button/"+btn._id}>
                            <button type="button" rel="tooltip" className="btn btn-success">
                                <i className="material-icons">edit</i>
                            </button>&nbsp;
                        </NavLink>
                        <button type="button" rel="tooltip" onClick={()=>{this.deletechatButton(btn)}}  className="btn btn-danger">
                            <i className="material-icons">close</i>
                        </button>
                    </td>
                </tr>
            )
        });

        return(
            <div className="">
                <SuccessBar />
                <ErrorBar />
                <div className="wrapper">
                    <Sidebar pagelink="chatbutton"/>

                    <div className="main-panel">
                        <Navbar pagename="Chat-Buttons"/>
                        <div className="content">
                            <div className="container-fluid">
                                <div className="row">

                                    <div className="col-md-12">
                                        <div className="card ">
                                            <div className="card-header card-header-rose card-header-icon">
                                                <div className="card-icon icon-holder" onClick={this.trigger} style={{cursor:'pointer'}}>
                                                    <i className="material-icons">mail_outline</i>
                                                </div>
                                                <h4 className="card-title">Create Chat Button</h4>
                                            </div>
                                            <div className="card-body ">

                                                <form className={'row'} method="#" action="#" onSubmit={(e)=>{this.createChatButton(e)}}>
                                                    <div className={'col-12 col-md-6'}>
                                                        <div className="form-group">
                                                            <label htmlFor="title" className="bmd-label-floating">Title</label>
                                                            <input type="text" className="form-control" id="title" />
                                                        </div>
                                                        <div className={'row'}>
                                                            <div className='form-group col-4 mt-0'>
                                                                <label htmlFor="image" className={'mb-0 bmd-label-static'}>Image File</label>
                                                                <input type='file' onChange={()=>{this.readfiles(1)}} className={'form-control'} id='imagefile'/>
                                                            </div>
                                                            <div className='form-group col-8 mt-0'>
                                                                <label htmlFor="imageurl">Image URL</label>
                                                                <input type='text' className={'form-control'} id='imageurl'/>
                                                            </div>
                                                        </div>
                                                        <div className={'row'}>
                                                            <div className='form-group col-4 mt-0'>
                                                                <label htmlFor="image">Video File</label>
                                                                <input type='file' onChange={()=>{this.readfiles(2)}} className={'form-control'} id='videofile'/>
                                                            </div>
                                                            <div className='form-group col-8 mt-0'>
                                                                <label htmlFor="imageurl">Video URL</label>
                                                                <input type='text' className={'form-control'} id='videourl'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='col-12 col-md-6'>
                                                        <b className='mb-3'>Mobile</b>
                                                        <div className={'row'}>
                                                            <div className='form-group col-4 mt-0'>
                                                                <label htmlFor="image" className={'mb-0 bmd-label-static'}>Image File</label>
                                                                <input type='file' onChange={()=>{this.readfiles(1)}} className={'form-control'} id='imagefile2'/>
                                                            </div>
                                                            <div className='form-group col-8 mt-0'>
                                                                <label htmlFor="imageurl">Image URL</label>
                                                                <input type='text' className={'form-control'} id='imageurl2'/>
                                                            </div>
                                                        </div>
                                                        <div className={'row'}>
                                                            <div className='form-group col-4 mt-0'>
                                                                <label htmlFor="image">Video File</label>
                                                                <input type='file' onChange={()=>{this.readfiles(2)}} className={'form-control'} id='videofile2'/>
                                                            </div>
                                                            <div className='form-group col-8 mt-0'>
                                                                <label htmlFor="imageurl">Video URL</label>
                                                                <input type='text' className={'form-control'} id='videourl2'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-footer ">
                                                        <button disabled={this.props.loading} type="submit" className="btn btn-fill btn-rose">Submit</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>

                                    {/*  BUTTON'S TABLE */}
                                    <div className="col-md-12">
                                        <div className="card">
                                            <div className="card-header card-header-rose card-header-icon">
                                                <div className="card-icon">
                                                    <i className="material-icons">assignment</i>
                                                </div>
                                                <h4 className="card-title">CHAT BUTTONS</h4>
                                            </div>
                                            <div className="card-body">
                                                <div className="table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                        <tr>
                                                            <th className="text-center">#</th>
                                                            <th>Title</th>

                                                            <th className="text-right">Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {button_table}

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.props.getChatButtons();
    }
    deletechatButton=(btn)=>{
        if(! window.confirm("Are you sure you want to delete this button "+btn.title+" ?")){
            return false;
        }
         this.props.deleteChatButtons(btn._id);
    }
    publishChatButtons=(btn)=>{
        let buttons=this.props.buttons;

        let published=buttons.filter((btn)=> btn.publish == 1);
        if(published.length >= 6 && btn.publish ==0){
            alert('you can only publish a maximum of 6 buttons');
            return false;
        }
        this.props.publishChatButtons(btn._id);
    }
    createChatButton=(e)=>{

        e.preventDefault();


        let title=document.getElementById('title').value;
        let image=document.getElementById('imagefile').files[0];
        let video=document.getElementById('videofile').files[0];
        let imageurl=document.getElementById('imageurl').value;
        let videourl=document.getElementById('videourl').value;

        let image2=document.getElementById('imagefile2').files[0];
        let video2=document.getElementById('videofile2').files[0];
        let imageurl2=document.getElementById('imageurl2').value;
        let videourl2=document.getElementById('videourl2').value;

        if(title.trim() === ''){
            return false;
        }

        if(!this.readfiles(1)){
            return false;
        }
        if(!this.readfiles(2)){
            return false;
        }

        console.log(formdata);

        let formdata=new FormData();
        formdata.append('title',title);
        formdata.append('imageurl',imageurl);
        formdata.append('videourl',videourl);
        formdata.append('image',image);
        formdata.append('videofile',video);

        formdata.append('imageurl2',imageurl2);
        formdata.append('videourl2',videourl2);
        formdata.append('image2',image2);
        formdata.append('videofile2',video2);

        // let data={title};

        this.props.createChatButtons(formdata);
    }
    readfiles=(type)=>{
        let image=document.getElementById('imagefile').files[0];
        let video=document.getElementById('videofile').files[0];

        let images=['image/jpeg','image/png','image/jpg'];
        let videos=['video/mp4'];

        if(type === 1){
            if(image){
                if(!images.find(img => image.type == img)){
                    alert('Image file format not supported');

                    return false;
                }
            }
        }else{
            if(video){
                if(!videos.find(vid => video.type == vid)){
                    alert('video file format not supported');

                    return false;
                }
            }
        }

        return true;
    }

}

const mapState=(state)=>{
    return {
        buttons:state.ChatButtonReducer.chatButtons,
        admin:state.AdminReducer,
        loading:state.ChatButtonReducer.loading
    }
}

const mapProps=(dispatch)=>{
    return {
        getChatButtons:()=>{ dispatch(ChatButtonActions.getChatButtons())},
        deleteChatButtons:(id)=>{dispatch(ChatButtonActions.deleteChatButtons(id))},
        publishChatButtons:(id)=>{dispatch(ChatButtonActions.publishChatButtons(id))},
        createChatButtons:(data)=>{ dispatch(ChatButtonActions.createChatButtons(data)) }
    }
}
export  default  connect(mapState,mapProps)(ChatButton);
