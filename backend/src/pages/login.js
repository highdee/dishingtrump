import React , { Component } from "react";
import { connect } from 'react-redux';
import Authentication from "../actions/authentication";
import authentication from "../actions/authentication";

class Login extends Component{

    state={
        isLoading:false
    }
    render(){
        return(
            <div>
                <div className="wrapper wrapper-full-page">
                    <div className="page-header login-page header-filter" filter-color="black" style={{backgroundImage: "url('../../assets/img/login.jpg')", backgroundSize: 'cover', backgroundPosition: 'top center'}}>
                        <div className="container">
                            <div className="row">
                            <div className="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
                                <form className="form" onSubmit={this.adminlogin}>
                                <div className="card card-login card-hidden">
                                    <div className="card-header card-header-rose text-center">
                                    <h4 className="card-title">Login</h4>
                                   
                                    </div>
                                    <div className="card-body ">
                                    <p className="card-description text-center">DishingTrump Admin Login</p>
                                  
                                    <span className="bmd-form-group">
                                        <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                            <i className="material-icons">email</i>
                                            </span>
                                        </div>
                                        <input disabled={ this.props.verify.isLoading } type="text" id="username" className="form-control" placeholder="username... " />
                                        </div> 
                                    </span>
                                    <span className="bmd-form-group">
                                        <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                            <i className="material-icons">lock_outline</i>
                                            </span>
                                        </div>
                                        <input disabled={ this.props.verify.isLoading } type="password" id="password" className="form-control" placeholder="password..." />
                                        </div>
                                    </span>
                                    </div>
                                    <div className="card-footer justify-content-center">
                                        <button disabled={ this.props.verify.isLoading } type="submit" className="btn btn-rose btn-md btn-block">Login</button> 
                                    </div>
                                    <p >{this.props.verify.message}</p>
                                </div>
                            </form>
                            </div>
                            </div>
                            </div>
                             
                    </div>
                </div>
            </div>
    );
    }

    adminlogin=(e)=>{
        e.preventDefault(); 
        if(this.props.verify.isLoading){
            return false;
        } 

        this.props.set_is_loading();

        let username=document.getElementById('username');
        let password=document.getElementById('password');
        
        this.props.login({username:username.value,password:password.value});
    }   
}

const mapState=(state)=>{
    return {
        verify:state.AdminReducer.verification
    }
}

const mapProps=(dispatch)=>{
    return {
        login:(data)=>{ dispatch(authentication.login(data)); },
        set_is_loading:()=>{ dispatch({type:'is_loading'}); }
    }
}
export default connect(mapState,mapProps)(Login);