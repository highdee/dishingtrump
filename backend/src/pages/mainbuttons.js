import React , { Component } from "react";
import MainButtonActions from "../actions/mainbuttonActions";
import {connect} from "react-redux";
import Sidebar from "./layouts/sidebar";
import Navbar from "./layouts/navbar";
import ErrorBar from "./layouts/errorbar";
import SuccessBar from "./layouts/successbar";
import { NavLink } from "react-router-dom";

class MainButton extends Component{

    state={
      publish:false,
      display_icon:false, 
    }

    render(){
        let buttons=this.props.buttons;
        let button_table=buttons.map((btn,index)=>{
          return (
            <tr key={index} role='row' className="odd">
              <td>{index+1}</td>
              <td className="Buttonicon"><img src={btn.icon} alt=""/></td>
              <td className="text-left">{btn.title}</td>
              <td>{btn.data.length}</td>
              <td>{btn.articles.length}</td>
              <td>{btn.videos.length}</td>
              <td>{btn.images.length}</td>
              <td>{btn.documents.length}</td> 
              <td className="td-actions text-right">
                <button onClick={()=>{ this.props.publishButton(btn._id) }} type="button" rel="tooltip" className={'btn ' + (btn.publish == 1 ? 'btn-info':'btn-danger') }>
                  { btn.publish == 1 ?  (<i className="material-icons">visibility</i>) : (<i className="material-icons">visibility_off</i>)}
                </button>&nbsp;
                <NavLink to={"/view-button/"+btn._id}>
                  <button type="button" rel="tooltip" className="btn btn-success">
                    <i className="material-icons">edit</i>
                  </button>&nbsp;
                </NavLink>
                <button type="button" rel="tooltip" onClick={()=>{this.props.deletemainButton(btn._id)}}  className="btn btn-danger">
                  <i className="material-icons">close</i>
                </button>
              </td>
            </tr> 
          )
        });
        
        return(
            <div className=""> 
                  <SuccessBar />
                  <ErrorBar />
                <div className="wrapper">
                     <Sidebar pagelink="mainbutton"/>

                     <div className="main-panel">
                        <Navbar pagename="Main-Buttons"/> 
                        <div className="content">
                          <div className="container-fluid">
                            <div className="row">

                            <div className="col-md-6">
                              <div className="card ">
                                <div className="card-header card-header-rose card-header-icon">
                                  <div className="card-icon icon-holder" onClick={this.trigger} style={{cursor:'pointer'}}>
                                    {
                                      !this.state.display_icon ? <i className="material-icons">mail_outline</i> : <img src="" id="iconImage"/>
                                    }
                                  </div>
                                  <h4 className="card-title">Create Main Button</h4>
                                </div>
                                <div className="card-body ">
                                  <form method="#" action="#" onSubmit={this.createButton}>
                                    <div className="form-group">
                                      <label htmlFor="title" className="bmd-label-floating">Title</label>
                                      <input type="text" className="form-control" id="title" />
                                    </div>
                                    <div className="form-group" style={{display:'none'}}>
                                      <label htmlFor="icon" className="bmd-label-floating">Icon</label>
                                      <input type="file" className="form-control" id="icon" onChange={this.readImage}/>
                                    </div> 
                                    <div className="form-check text-left">
                                    <div className="togglebutton">
                                      <label>
                                        <input type="checkbox" id="publish" checked={this.state.publish} onChange={this.publish} />
                                        <span className="toggle"></span>
                                        Publish
                                      </label>
                                    </div>
                                    </div>
                                    <div className="card-footer ">
                                  <button disabled={this.props.requestObject.loading} type="submit" className="btn btn-fill btn-rose">Submit</button>
                                </div>
                                  </form>
                                </div>
                                
                              </div>
                            </div>

                            {/*  BUTTON'S TABLE */}
                            <div className="col-md-12">
                              <div className="card">
                                <div className="card-header card-header-rose card-header-icon">
                                  <div className="card-icon">
                                    <i className="material-icons">assignment</i>
                                  </div>
                                  <h4 className="card-title">MAIN LEFT PANEL BUTTONS</h4>
                                </div>
                                <div className="card-body">
                                  <div className="table-responsive">
                                    <table className="table">
                                      <thead>
                                        <tr>
                                          <th className="text-center">#</th>
                                          <th>Icon</th>
                                          <th>Title</th>
                                          <th>Children</th>
                                          <th>Articles</th>
                                          <th>Videos</th>
                                          <th>Images</th>
                                          <th>Documents</th>  
                                          <th className="text-right">Actions</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      {button_table}
                                        
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            </div>
                            
                          </div>
                        </div>
                     </div>
                </div>
            </div>
        )
    }
 
    componentDidMount(){
      this.props.getButtons();

    }
    trigger=()=>{
      document.getElementById('icon').click();
    }
    readImage=(e)=>{
      let el=e.target;
      let files=el.files[0]; 
      if(!(files.type =='image/jpeg' || files.type =='image/jpg' || files.type =='image/png')){
        alert("file format not supported");
        return false;
      }

      let filer=new FileReader();
      filer.onload=(result)=>{
        // console.log(result.srcElement); 
        this.setState(()=>{
          return{
            ...this.state,
            display_icon:true
          }
        });
        document.getElementById('iconImage').src=result.srcElement.result;
      }
      filer.readAsDataURL(files);
    } 
    publish=()=>{
      this.setState(()=>{
        return{
          ...this.state,
          publish:!this.state.publish
        }
      });
    }
    createButton=(e)=>{
      e.preventDefault();

      if(this.state.loading){
        return false;
      }
     
      let formdata=new FormData();
      let icon=document.getElementById('icon');
      let title=document.getElementById('title');
      let publish=this.state.publish; 
 
      if(title.value.trim() == ''){
        alert('Main buttons need to have a title');
        return false;
      }
      
      let data={title:title.value,publish:publish?1:0};
      let iconFlag=false;

      if(icon.files.length !=0){ 
        formdata.append('title',title.value);
        formdata.append('publish',publish ? 1:0);
        formdata.append('icon',icon.files[0]);
        iconFlag=true;
      } 

      this.props.createButton(data,formdata,iconFlag) ;

      title.value="";
      if(iconFlag){
        document.getElementById('iconImage').src='';
      }
    }

    publishButton(id){
      console.log(id);
    }
}

const mapState=(state)=>{
  return{
      buttons:state.MainButtonReducer.buttons,
      admin:state.AdminReducer,
      requestObject:state.AdminReducer.mainButtonRequestObject
  }
}
const mapProps=(dispatch)=>{
  return {
      getButtons:()=>{ dispatch(MainButtonActions.getButtons()); },
      publishButton:(id)=>{ dispatch(MainButtonActions.publishButton(id)); },
      createButton:(data,formdata,iconFlag)=>{ dispatch(MainButtonActions.createmainButton(data,formdata,iconFlag)); },
      deletemainButton:(id)=>{ dispatch(MainButtonActions.deletemainButton(id)); }
  }
}
export default connect(mapState,mapProps)(MainButton);