import  React , { Component } from 'react';
import { connect } from 'react-redux';
import Sidebar from "./layouts/sidebar";
import Navbar from "./layouts/navbar";
import ErrorBar from "./layouts/errorbar";
import SuccessBar from "./layouts/successbar";
import ViewButtonActions from "../actions/viewButtonActions";
import MainButtonActions from "../actions/mainbuttonActions";

class SubButton extends  Component{

    render(){
        let button=this.props.button;
        return (
            <div id="subbuttons">
                <SuccessBar />
                <ErrorBar />
                <div className="wrapper">
                    <Sidebar pagelink="subbutton"/>
                    <div className="main-panel">
                        <Navbar pagename="VIEW BUTTON"/>
                        <div className="content">
                            <div className="container-fluid">
                                <div className="col-lg-12 col-md-12">
                                    <div className="card">
                                        <div className="card-header card-header-tabs card-header-rose">
                                            <div className="nav-tabs-navigation">
                                                <div className="nav-tabs-wrapper">
                                                    <b>SUB BUTTONS</b><br />

                                                </div>
                                                <form onSubmit={this.searchButtons}>
                                                    <input disabled={this.props.search} type='text' id={'search'} className='searchBtn' placeholder='search for button titles'/>
                                                </form>

                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <div className="tab-content" style={{height:'400px',overflowY:'scroll',padding:'10px'}}>
                                                <ul className="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center"
                                                    role="tablist">
                                                    {
                                                        button.length == 0 ?
                                                            (<div style={{width:'100%',height:'fit-content'}}><b className="text-danger">No sub buttons yet</b></div>)
                                                            :
                                                            button.map((btn,index)=>{
                                                                return (
                                                                    <li key={index} style={{margin:'10px 20px',width:'100px',height:'200px !important',padding:'5px 0px'}} className="nav-item">

                                                                        <a onClick={()=>{window.location='/view-button/'+btn._id}} className="nav-link active p-0 pt-2 pl-2 pr-2 pb-2 subbuttons" style={{height:'100%',background:'#dd2667',lineHeight:'1.2',position:'relative'}}
                                                                           role="tablist">

                                                                            <div className='mb-2' style={{width:'100%',height:'50px'}}>
                                                                                <img alt={""} className="icon-button" src={btn.icon} style={{width:'100%',height:'100%'}}/>
                                                                            </div>

                                                                            {btn.title}
                                                                            <a className='deleteSub'>
                                                                                <span  onClick={(e)=>{  e.stopPropagation() ; this.props.deleteSubButton(btn._id) ;return false;}} className="material-icons">cancel</span>
                                                                            </a>
                                                                        </a>
                                                                    </li>
                                                                )
                                                            })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.props.getSubButton();
    }

     searchButtons=(e)=>{
        e.preventDefault();

        if(this.props.search){
            return false;
        }
        let input=document.getElementById('search').value;
        this.props.searchButtons({input});
    }
}

const mapState=(state)=>{
    return {
        button:state.ViewButtonReducer.subbuttons,
        search:state.MainButtonReducer.searching,
    }
}

const mapProps=(dispatch)=>{
    return {
        getSubButton:()=>{ dispatch(ViewButtonActions.getSubButton()); },
        deleteSubButton:(id)=>{ dispatch(MainButtonActions.deleteSubButton(id)); },
        searchButtons:(data)=>{ dispatch(MainButtonActions.searchButtons(data)); },
    }
}
export  default  connect(mapState,mapProps)(SubButton);
