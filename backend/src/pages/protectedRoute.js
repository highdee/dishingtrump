import React , { Component } from "react";
import { Redirect ,Route} from 'react-router-dom';
import auth from '../auth';

 const ProtectedRoute = ({component:Component ,computedMatch:computedMatch , ...rest})=>{

     return (
         <Route render={(props)=>{
           if(auth.checkAuth()){
                return <Component computedMatch={computedMatch} {...props}/>
            }else{
                return <Redirect to="/"/>
            } 
         }} 
          />
     );
 }
export default ProtectedRoute;