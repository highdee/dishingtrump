import React , { Component } from "react";
import Sidebar from "./layouts/sidebar";
import Navbar from "./layouts/navbar";
import { connect } from "react-redux";
import dashboardAction from '../actions/dashboardActions';

class Dashboard extends Component{

    render(){
        return (
            <div className=""> 
                <div className="wrapper">
                     <Sidebar pagelink="dashboard"/>

                     <div className="main-panel">
                        <Navbar pagename="Dashboard"/>

                        <div className="content">
                            <div className="row">
                                <div className="col-lg-3 col-md-6 col-sm-6">
                                    <div className="card card-stats">
                                        <div className="card-header card-header-warning card-header-icon">
                                            <div className="card-icon">
                                                <i className="material-icons">layers</i>
                                            </div>
                                            <p className="card-category">BUTTONS</p>
                                            <h3 className="card-title">{this.props.stats.buttons}</h3>
                                        </div>
                                        <div className="card-footer">
                                            <div className="stats">
                                                <i className="material-icons text-danger">layers</i>
                                                <a href="#pablo">Total Main/Sub buttons</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-sm-6">
                                    <div className="card card-stats">
                                        <div className="card-header card-header-rose card-header-icon">
                                            <div className="card-icon">
                                                <i className="material-icons">person</i>
                                            </div>
                                            <p className="card-category">USERS</p>
                                            <h3 className="card-title">{this.props.stats.users}</h3>
                                        </div>
                                        <div className="card-footer">
                                            <div className="stats">
                                                <i className="material-icons">person</i> All platform users
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-sm-6">
                                    <div className="card card-stats">
                                        <div className="card-header card-header-success card-header-icon">
                                            <div className="card-icon">
                                                <i className="material-icons">store</i>
                                            </div>
                                            <p className="card-category">Revenue</p>
                                            <h3 className="card-title">$34,245</h3>
                                        </div>
                                        <div className="card-footer">
                                            <div className="stats">
                                                <i className="material-icons">date_range</i> Last 24 Hours
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-sm-6">
                                    <div className="card card-stats">
                                        <div className="card-header card-header-info card-header-icon">
                                            <div className="card-icon">
                                                <i className="fa fa-twitter"></i>
                                            </div>
                                            <p className="card-category">Followers</p>
                                            <h3 className="card-title">+245</h3>
                                        </div>
                                        <div className="card-footer">
                                            <div className="stats">
                                                <i className="material-icons">update</i> Just Updated
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.props.getStats();
    }
}

const mapState=(state)=>{
    return {
        stats:state.DashboardReducer.stats
    }
}

const mapProps=(dispatch)=>{
    return {
        getStats:()=>{ dispatch(dashboardAction.getStats()) }
    }
}

export default connect(mapState,mapProps)(Dashboard);