import React , { Component } from "react";
import { connect } from "react-redux";
import SuccessBar from "./layouts/successbar";
import Sidebar from "./layouts/sidebar";
import Navbar from "./layouts/navbar";
import ErrorBar from "./layouts/errorbar";
import ViewButtonActions from "../actions/viewButtonActions";
import MainButtonActions from "../actions/mainbuttonActions";
import ChatButtonActions from "../actions/chatbutton";
import CreateSubButton from "./layouts/create-sub-button";

class ViewChatButton extends Component{

    render(){

        let button = this.props.button;
        console.log(button);

        return (
            <div className="">
                <SuccessBar />
                <ErrorBar />
                <div className="wrapper">
                    <Sidebar pagelink="chatbutton"/>
                    <div className="main-panel">
                        <Navbar pagename="VIEW CHAT BUTTON"/>
                        <div className="content">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-lg-4 col-md-4 col-sm-4 cards">
                                        <div className="card card-pricing card-raised">
                                            <div className="card-body view-button " >
                                                <h4 className="card-category">{button.title}</h4>
                                                <div  className="card-icon icon-rose" style={{width:'100%',height:'150px'}}>
                                                    {/* <i className="material-icons">home</i> */}
                                                    {
                                                        button.code != '' ?
                                                            <span className={'videoholder'} dangerouslySetInnerHTML={{__html:button.code}}></span>
                                                            :
                                                        button.video != '' ?
                                                            <video style={{width:'100%',height:'100%'}} controls src={button.video}></video>
                                                            :
                                                        button.image && button.image != '' ?
                                                            <img className="icon-button" src={button.image} style={{width:'100%',height:'100%'}} alt={''}/>
                                                            :
                                                        button.imageurl ?
                                                            <img className="icon-button" src={button.imageurl} style={{width:'100%',height:'100%'}} alt={''}/>
                                                            :
                                                            ''
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-4 col-md-4 col-sm-4 cards">
                                        <div className="card card-pricing card-raised">
                                            <div className="card-body view-button " >
                                                <h4 className="card-category">{button.title} (mobile)</h4>
                                                <div  className="card-icon icon-rose" style={{width:'100%',height:'150px'}}>
                                                    {/* <i className="material-icons">home</i> */}
                                                    {
                                                        button.mobile.code != '' ?
                                                            <span className={'videoholder'} dangerouslySetInnerHTML={{__html:button.mobile.code}}></span>
                                                            :
                                                            button.mobile.video != '' ?
                                                                <video style={{width:'100%',height:'100%'}} controls src={button.mobile.video}></video>
                                                                :
                                                                button.mobile.image && button.mobile.image != '' ?
                                                                    <img className="icon-button" src={button.mobile.image} style={{width:'100%',height:'100%'}} alt={''}/>
                                                                    :
                                                                button.mobile.imageurl ?
                                                                    <img className="icon-button" src={button.mobile.imageurl} style={{width:'100%',height:'100%'}} alt={''}/>
                                                                    :
                                                                ''
                                                    }
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="card ">
                                            <div className="card-header card-header-rose card-header-icon">
                                                <div className="card-icon icon-holder" onClick={this.trigger} style={{cursor:'pointer'}}>
                                                    <i className="material-icons">mail_outline</i>
                                                </div>
                                                <h4 className="card-title">Create Chat Button</h4>
                                            </div>
                                            <div className="card-body ">
                                                <form method="#" className='row' action="#" onSubmit={(e)=>{this.updateChatButton(e)}}>
                                                        <div className='col-12 col-md-8'>
                                                            <div className="form-group">
                                                                <label htmlFor="title" className="bmd-label-floating">Title</label>
                                                                <input type="text" onChange={this.updatedetails} className="form-control" id="title" value={button.title}/>
                                                            </div>
                                                            <div className={'row'}>
                                                                <div className='form-group col-4 mt-0'>
                                                                    <label htmlFor="image" className={'mb-0 bmd-label-static'}>Image File</label>
                                                                    <input type='file' onChange={()=>{this.readfiles(1)}} className={'form-control'} id='imagefile'/>
                                                                </div>
                                                                <div className='form-group col-8 mt-0'>
                                                                    <label htmlFor="imageurl">Image URL</label>
                                                                    <input type='text'  onChange={this.updatedetails} className={'form-control'} id='imageurl' value={button.imageurl}/>
                                                                </div>
                                                            </div>
                                                            <div className={'row'}>
                                                                <div className='form-group col-4 mt-0'>
                                                                    <label htmlFor="image">Video File</label>
                                                                    <input type='file' onChange={()=>{this.readfiles(2)}} className={'form-control'} id='videofile'/>
                                                                </div>
                                                                <div className='form-group col-8 mt-0'>
                                                                    <label htmlFor="imageurl">Video URL</label>
                                                                    <input type='text'  onChange={this.updatedetails} className={'form-control'} id='videourl' value={button.code}/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className='col-12 col-md-4'>
                                                            <b>Mobile</b>
                                                            <div className={'row'}>
                                                                <div className='form-group col-4 mt-0'>
                                                                    <label htmlFor="image" className={'mb-0 bmd-label-static'}>Image File</label>
                                                                    <input type='file' onChange={()=>{this.readfiles(1)}} className={'form-control'} id='imagefile2'/>
                                                                </div>
                                                                <div className='form-group col-8 mt-0'>
                                                                    <label htmlFor="imageurl">Image URL</label>
                                                                    <input type='text'  onChange={this.updatedetails} className={'form-control'} id='imageurl2' value={button.mobile.imageurl}/>
                                                                </div>
                                                            </div>
                                                            <div className={'row'}>
                                                                <div className='form-group col-4 mt-0'>
                                                                    <label htmlFor="image">Video File</label>
                                                                    <input type='file' onChange={()=>{this.readfiles(2)}} className={'form-control'} id='videofile2'/>
                                                                </div>
                                                                <div className='form-group col-8 mt-0'>
                                                                    <label htmlFor="imageurl">Video URL</label>
                                                                    <input type='text'  onChange={this.updatedetails} className={'form-control'} id='videourl2' value={button.mobile.code}/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div className="card-footer ">
                                                        <button disabled={this.props.loading} type="submit" className="btn btn-fill btn-rose">Submit</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-12 col-md-12">
                                    <div className="card">
                                        <div className="card-header card-header-tabs card-header-rose">
                                            <div className="nav-tabs-navigation">
                                                <div className="nav-tabs-wrapper">
                                                    <b>SUB BUTTONS</b><br />

                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <div className="tab-content" style={{height:'400px',overflowY:'scroll',padding:'10px'}}>
                                                <ul className="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center"
                                                    role="tablist">
                                                    <li key={'index'} style={{margin:'10px 20px',width:'100px',height:'200px !important',padding:'5px 0px'}} className="nav-item d-block">
                                                        <a href="#createSubButton" data-toggle="modal"  className="nav-link active subbuttons" style={{height:'100%',background:'lightgray'}}
                                                           role="tablist">
                                                            <i className="material-icons">add</i>
                                                            create
                                                        </a>
                                                    </li>
                                                    {
                                                        button.subbuttons.length == 0 ?
                                                            (<div style={{width:'100%',height:'fit-content'}}><b className="text-danger">No sub buttons yet</b></div>)
                                                            :
                                                            button.subbuttons.map((btn,index)=>{
                                                                return (
                                                                    <li key={index} style={{margin:'10px 20px',width:'100px',height:'200px !important',padding:'5px 0px'}} className="nav-item">

                                                                        <a onClick={()=>{window.location='/view-button/'+btn._id}} className="nav-link active p-0 pt-2 pl-2 pr-2 pb-2 subbuttons" style={{height:'100%',background:'#dd2667',lineHeight:'1.2',position:'relative'}}
                                                                           role="tablist">

                                                                            <div className='mb-2' style={{width:'100%',height:'50px'}}>
                                                                                <img alt={""} className="icon-button" src={btn.icon} style={{width:'100%',height:'100%'}}/>
                                                                            </div>

                                                                            {btn.title}
                                                                            <a className='deleteSub'>
                                                                                <span  onClick={(e)=>{  e.stopPropagation() ; this.props.deleteSubButton(btn._id) ;return false;}} className="material-icons">cancel</span>
                                                                            </a>
                                                                        </a>
                                                                    </li>
                                                                )
                                                            })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <CreateSubButton />
            </div>
        )
    }

    componentDidMount() {
        let id=this.props.computedMatch.params.id;
        let button=this.props.button;
        this.props.getButton(id);
        this.props.getSubButton(id);
    }
    updatedetails=()=>{
        let title=document.getElementById('title').value;
        let imageurl=document.getElementById('imageurl').value;
        let videourl=document.getElementById('videourl').value;
        let imageurl2=document.getElementById('imageurl2').value;
        let videourl2=document.getElementById('videourl2').value;

        this.props.updatedetails({title,imageurl,code:videourl,imageurl2,code2:videourl2});
    }
    updateChatButton=(e)=>{

        e.preventDefault();
        let title=document.getElementById('title').value;
        let image=document.getElementById('imagefile').files[0];
        let video=document.getElementById('videofile').files[0];
        let imageurl=document.getElementById('imageurl').value;
        let videourl=document.getElementById('videourl').value;

        let image2=document.getElementById('imagefile2').files[0];
        let video2=document.getElementById('videofile2').files[0];
        let imageurl2=document.getElementById('imageurl2').value;
        let videourl2=document.getElementById('videourl2').value;

        if(title.trim() === ''){
            return false;
        }

        if(!this.readfiles(1)){
            return false;
        }
        if(!this.readfiles(2)){
            return false;
        }


        let formdata=new FormData();
        formdata.append('title',title);
        formdata.append('imageurl',imageurl);
        formdata.append('videourl',videourl);
        formdata.append('image',image);
        formdata.append('videofile',video);

        formdata.append('imageurl2',imageurl2);
        formdata.append('videourl2',videourl2);
        formdata.append('image2',image2);
        formdata.append('videofile2',video2);

        this.props.updateChatButtons(formdata , this.props.button._id);
    }
    readfiles=(type)=>{
        let image=document.getElementById('imagefile').files[0];
        let video=document.getElementById('videofile').files[0];

        let images=['image/jpeg','image/png','image/jpg'];
        let videos=['video/mp4'];

        if(type === 1){
            if(image){
                if(!images.find(img => image.type == img)){
                    alert('Image file format not supported');

                    return false;
                }
            }
        }else{
            if(video){
                if(!videos.find(vid => video.type == vid)){
                    alert('video file format not supported');

                    return false;
                }
            }
        }

        return true;
    }
}


const mapState=(state)=>{
    return{
        button:state.ChatButtonReducer.selected_button ,
    }
}
const mapProps=(dispatch)=> {
    return {
        getButton:(id)=>{ dispatch(ChatButtonActions.getAChatButton(id)); },
        updateChatButtons:(data,id)=>{ dispatch(ChatButtonActions.updateChatButtons(data,id)); },
        updatedetails:(data)=>{dispatch({type:'update_selected_button',data})},
        getSubButton:(id)=>{ dispatch(ViewButtonActions.getButtonSubButtons(id)); },
        deleteSubButton:(id)=>{ dispatch(MainButtonActions.deleteSubButton(id)); },
    }
}
export  default connect(mapState,mapProps)(ViewChatButton);