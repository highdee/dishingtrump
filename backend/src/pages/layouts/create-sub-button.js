import React , { Component } from 'react';
import { connect } from 'react-redux';
import auth from "../../auth";
import MainButtonActions from '../../actions/mainbuttonActions';


class  CreateSubButton extends  Component{

    state={
        display_icon: false
    }
    render(){
        return (
            <div className="modal fade" id="createSubButton" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">CREATE SUB BUTTON</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="card ">
                                <div className="card-header card-header-rose card-header-icon">
                                    <div className="card-icon icon-holder" onClick={this.trigger} style={{cursor:'pointer'}}>
                                        {
                                            !this.state.display_icon ? <i className="material-icons">mail_outline</i> : <img style={{width:'100%'}} src="" id="iconSubImage"/>
                                        }
                                    </div>
                                    <h4 className="card-title">Create Main Button</h4>
                                </div>
                                <div className="card-body ">
                                    <form method="#" action="#" onSubmit={this.createSubButton}>
                                        <div className="form-group">
                                            <label htmlFor="title" className="bmd-label-floating">Title</label>
                                            <input type="text" className="form-control" id="icontitle" />
                                        </div>
                                        <div className="form-group" style={{display:'none'}}>
                                            <label htmlFor="icon" className="bmd-label-floating">Icon</label>
                                            <input type="file" className="form-control" id="iconSub" onChange={this.readSubImage}/>
                                        </div>
                                        <div className="card-footer ">
                                            <button disabled={this.props.requestObject.loading} type="submit" className="btn btn-fill btn-rose">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    readSubImage=(e)=>{
        console.log(e);
        let el=e.target;
        if(el.files.length == 0){
            return false;
        }
        let files=el.files[0];
        // console.log(files);
        if(!(files.type =='image/jpeg' || files.type =='image/jpg' || files.type =='image/png')){
            alert("file format not supported");
            return false;
        }
        
        this.setState(()=>{
            return {
                ...this.state,
                display_icon: true
            }
        });

        let filer=new FileReader();
        filer.onload=(result)=>{

            document.getElementById('iconSubImage').src=result.srcElement.result;
        }
        filer.readAsDataURL(files);
    }
    trigger=()=>{
        document.getElementById('iconSub').click();
    }
    createSubButton=(e)=>{
        e.preventDefault();

        let title=document.getElementById('icontitle');
        let icon=document.getElementById('iconSub');

        if(title.value.trim() == ''){
            alert('Button title is needed to create a button');
            return false;
        }

        let id=this.props.button._id;

        let formdata=new FormData();
        formdata.append('title',title.value);
        formdata.append('icon',icon.files[0]);

        if(this.props.button._id){
            formdata.append('id',this.props.button._id);
        }else{
            formdata.append('id',this.props.chatbutton._id);
        }


        this.props.createSubButton(formdata);
    }
}

const mapState =(state)=>{
    return {
        button:state.ViewButtonReducer.button,
        chatbutton:state.ChatButtonReducer.selected_button ,
        requestObject:state.AdminReducer.mainButtonRequestObject,
    }
}

const mapProps=(dispatch)=>{
    return {
        createSubButton:(data)=>{ dispatch(MainButtonActions.createsubButton(data))}
    }
}

export  default  connect(mapState,mapProps)(CreateSubButton);