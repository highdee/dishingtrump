import React , { Component } from "react";
import MainButtonActions from "../../actions/mainbuttonActions";
import ViewButtonActions from "../../actions/viewButtonActions";
import {connect} from "react-redux";
import auth from '../../auth';


class EditButtonArticle extends Component{

    state={
        publish:false,
        display_icon:false,
        publish_image:false
    };

    render(){
        let button=this.props.button;
        let highlightArticle=this.props.highlightArticle;
        return(
            <div className="modal fade" id="editbuttonArticle" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">EDIT ARTICLE</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <img id='display_image' style={{ width:'100%',height:'100%'}} src={ highlightArticle.article.filename} alt=''/>
                            <form method="#" action="#" onSubmit={this.updateArticle}>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> ARTICLE TITLE </label>
                                    <input type="text" className="form-control" value={highlightArticle.article.title} onChange={this.updateArticleDetails} id="updatearticletitle" />
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" style={{left:'40%'}} className="bmd-label mb-1"> CHANGE PHOTO URL </label>
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <button type="button" className="input-group-text" >
                                                {
                                                   <i onClick={this.triggerArticle} className="material-icons">cloud_upload</i>
                                                }
                                            </button>
                                        </div>
                                        <input type='file' onChange={this.readupdateArticle} id="updatearticle" />
                                        <input type="text"  value={highlightArticle.article.filename} className="form-control pl-4" id="updatearticlefile" />
                                    </div>
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> ARTICLE URL </label>
                                    <input type="text" className="form-control" onChange={this.updateArticleDetails} value={highlightArticle.article.url} id="updatearticleurl" />
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> ARTICLE AUTHOR </label>
                                    <input type="text" className="form-control" onChange={this.updateArticleDetails} value={highlightArticle.article.author} id="updatearticleauthor" />
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> DATE </label>
                                    <input type="date" className="form-control" onChange={this.updateArticleDetails} value={highlightArticle.article.date} id="updatearticledate" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label col-12 left text-left p-0"> PARAGRAPH </label>
                                    <textarea className='textarea edit-140' id='updatearticleparagraph' onChange={this.updateArticleDetails}  value={highlightArticle.article.paragraph} style={{width:'100%'}}></textarea>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label col-12 left text-left p-0"> DESCRIPTION </label>
                                    <textarea className='textarea edit-140' id='updatearticledescription' onChange={this.updateArticleDetails}  value={highlightArticle.article.description} style={{width:'100%'}}></textarea>
                                </div>
                                <button disabled={this.props.articleObject.loading} type="submit" className="btn btn-rose">UPDATES</button>
                            </form>
                        </div>
                        <div className="modal-footer">
                            {/*<button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>*/}
                            {/*<button type="button" className="btn btn-primary">Save changes</button>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount(){}
    updateArticleDetails=()=>{
        let title=document.getElementById('updatearticletitle');
        let url=document.getElementById('updatearticleurl');
        let author=document.getElementById('updatearticleauthor');
        let date=document.getElementById('updatearticledate');
        let paragraph=document.getElementById('updatearticleparagraph');
        let description=document.getElementById('updatearticledescription');

        let data={
            title:title.value,
            url:url.value,
            author:author.value,
            date:date.value,
            paragraph:paragraph.value,
            description:description.value,
        };
        this.props.changeTitle(data);
    }
    trigger=()=>{
        document.getElementById('icon').click();
    }
    triggerArticle=()=>{
        document.getElementById('updatearticle').click();
    }
    updateArticle=(e)=>{
        e.preventDefault();

        if(this.props.articleObject.loading){
            return false;
        }

        let title=document.getElementById('updatearticletitle');
        let url=document.getElementById('updatearticleurl');
        let author=document.getElementById('updatearticleauthor');
        let date=document.getElementById('updatearticledate');
        let paragraph=document.getElementById('updatearticleparagraph');
        let desc=document.getElementById('updatearticledescription');
        let article=document.getElementById('updatearticle');


        let id=this.props.button._id;
        let payload;

        if(title.value.trim() == '' || url.value.trim()==""){
            alert('Article title and url is needed to create an Article');
            return false;
        }

        let formdata=new FormData();
        formdata.append('title',title.value);
        formdata.append('url',url.value);
        formdata.append('author',author.value);
        formdata.append('date',date.value);
        formdata.append('paragraph',paragraph.value);
        formdata.append('description',desc.value);
        formdata.append('id',this.props.highlightArticle.article.id);

        formdata.append('image' , article.files[0]);

        this.props.updateArticle(formdata , this.props.button._id ,auth.getAuth().data.token);

    }
    readupdateArticle=(e)=>{
        let el=e.target;
        if(el.files.length == 0){
            return false;
        }
        let files=el.files[0];
        console.log(files);
        if(!(files.type =='image/jpeg' || files.type =='image/jpg' || files.type =='image/png')){
            alert("file format not supported");
            return false;
        }
        let url=document.getElementById('updatearticlefile');

        url.value=files.name;

    }

}

const mapState=(state)=>{
    return{
        button:state.ViewButtonReducer.button,
        admin:state.AdminReducer,
        requestObject:state.AdminReducer.mainButtonRequestObject,
        articleObject:state.ViewButtonReducer.article,
        highlightArticle:state.ViewButtonReducer.edittingArticle
    }
}
const mapProps=(dispatch)=>{
    return {
        getButton:(id)=>{ dispatch(ViewButtonActions.getButton(id)); },
        publishButton:(id)=>{ dispatch(MainButtonActions.publishButton(id)); },
        updateButton:(data,formdata,iconFlag)=>{ dispatch(ViewButtonActions.updatemainButton(data,formdata,iconFlag)); },
        changeTitle:(data)=>{ dispatch({type:'updateArticleDetails', data}); },
        updateArticle:(data,id,token)=>{ dispatch(ViewButtonActions.updateArticle(data,id,token)); },
        setPublishImage:(button,image)=>{ dispatch(ViewButtonActions.setPublishImage(button,image)); },
        deleteImage:(button,image)=>{ dispatch(ViewButtonActions.deleteImage(button,image)); }
    }
}
export default connect(mapState,mapProps)(EditButtonArticle);