import React , { Component } from "react";
import MainButtonActions from "../../actions/mainbuttonActions";
import ViewButtonActions from "../../actions/viewButtonActions";
import {connect} from "react-redux";
import auth from '../../auth';


class EditButtonImage extends Component{

    state={
        publish:false,
        display_icon:false,
        publish_image:false
    };

    render(){
            let button=this.props.button;
            let highlightImage=this.props.highlightImage;
                // console.log(highlightImage);
        return(
            <div className="modal fade" id="editbuttonimage" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">EDIT IMAGE</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <img id='display_image' style={{ width:'100%',height:'100%'}} src={ highlightImage.image.url != '' ? highlightImage.image.url: highlightImage.image.filename} alt=''/>
                            <form method="#" action="#" onSubmit={this.updateImage}>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> PHOTO TITLE </label>
                                    <input type="text" className="form-control" value={highlightImage.image.title} onChange={this.updateImageDetails} id="updatetitle" />
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" style={{left:'40%'}} className="bmd-label mb-1"> CHANGE PHOTO URL </label>
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <button type="button" className="input-group-text" >
                                                {
                                                    this.state.display_icon ?
                                                    <i onClick={this.removeFile} className="material-icons">remove</i>
                                                    :
                                                    <i onClick={this.trigger2} className="material-icons">cloud_upload</i>
                                                }
                                            </button>
                                        </div>
                                        <input type='file' onChange={this.readUpdateImage} id="updateimage" />
                                        <input type="text"  value={highlightImage.image.url !='' ? highlightImage.image.url:highlightImage.image.filename } className="form-control pl-4" id="updateurl" />
                                    </div>
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> PHOTO DESCRIPTION </label>
                                    <input type="text" className="form-control" onChange={this.updateImageDetails} value={highlightImage.image.desc} id="updatedesc" />
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> DATE </label>
                                    <input type="datetime-local" className="form-control" onChange={this.updateImageDetails} value={highlightImage.image.date} id="updatedate" />
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> CREDIT </label>
                                    <input type="text" className="form-control" onChange={this.updateImageDetails} value={highlightImage.image.credit} id="updatecredit" />
                                </div>

                                <button disabled={this.props.highlightImage.loading} type="submit" className="btn btn-rose">UPDATES</button>
                            </form>
                        </div>
                        <div className="modal-footer">
                            {/*<button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>*/}
                            {/*<button type="button" className="btn btn-primary">Save changes</button>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount(){}
    updateImageDetails=()=>{
        let title=document.getElementById('updatetitle');
        let credit=document.getElementById('updatecredit');
        let desc=document.getElementById('updatedesc');
        let date=document.getElementById('updatedate');
        let icon=document.getElementById('updateimage');
        let url=document.getElementById('updateurl');

        let data={
            title:title.value,
            credit:credit.value,
            desc:desc.value,
            date:date.value
        };
        // console.log(this.state.publish_image);
        this.props.changeTitle(data);
    }
    trigger=()=>{
        document.getElementById('icon').click();
    }
    trigger2=()=>{
        document.getElementById('updateimage').click();
    }
    updateImage=(e)=>{
        e.preventDefault();

        if(this.props.imageObject.loading){
            return false;
        }

        let title=document.getElementById('updatetitle');
        let credit=document.getElementById('updatecredit');
        let desc=document.getElementById('updatedesc');
        let icon=document.getElementById('updateimage');
        let url=document.getElementById('updateurl');
        let date=document.getElementById('updatedate');


        let id=this.props.button._id;
        let payload;

        if(url.value.trim()=="" && !this.state.display_icon){
            alert("No url or file was selected.");
            return false;
        }


        if(icon.files.length != 0 && this.state.display_icon){
            payload=new FormData();
            // console.log('files');
            payload.append('title',title.value);
            payload.append('credit',credit.value);
            payload.append('desc',desc.value);
            payload.append('date',date.value);
            payload.append('id',this.props.highlightImage.image.id);
            payload.append('image',icon.files[0]);
            payload.append('imageurl',url.value);

            this.props.updateImage(payload , this.props.button._id ,auth.getAuth().data.token);

        }else{
            payload=new FormData();

            payload.append('title',title.value);
            payload.append('credit',credit.value);
            payload.append('desc',desc.value);
            payload.append('date',date.value);
            payload.append('id',this.props.highlightImage.image.id);
            payload.append('image',icon.files[0]);
            payload.append('imageurl',url.value);

            this.props.updateImage(payload , this.props.button._id ,auth.getAuth().data.token);
        }

    }
    removeFile=()=>{
        this.setState(()=>{
            return {
                ...this.state,
                display_icon:false
            }
        });

        let image=document.getElementById('display_image');
        let url=document.getElementById('updateurl');
        url.value=this.props.highlightImage.image.url != '' ? this.props.highlightImage.image.url: this.props.highlightImage.image.filename;
        image.src=this.props.highlightImage.image.url != '' ? this.props.highlightImage.image.url: this.props.highlightImage.image.filename;

    }
    readUpdateImage=(e)=>{
        let el=e.target;
        if(el.files.length == 0){
            return false;
        }
        let files=el.files[0];
        // console.log(files);
        if(!(files.type =='image/jpeg' || files.type =='image/jpg' || files.type =='image/png')){
            alert("file format not supported");
            return false;
        }
        let url=document.getElementById('updateurl');
        url.value='';
        url.value=files.name;

        this.setState(()=>{
            return {
                ...this.state,
                display_icon: true
            }
        });

        let filer=new FileReader();
        filer.onload=(result)=>{
            document.getElementById('display_image').src=result.srcElement.result;
        }
        filer.readAsDataURL(files);
    }
    publish=()=>{
        this.setState(()=>{
            return{
                ...this.state,
                publish:!this.state.publish
            }
        });
        let title=document.getElementById('title');
        this.props.changeTitle(title.value,!this.state.publish);
    }
    update_publish_image=()=>{
        let result=this.state.publish_image === this.props.highlightImage.image.publish ? this.state.publish_image : !this.state.publish_image ;
        // console.log(result);
        // console.log(this.state.publish_image);

        this.setState(()=>{
            return{
                ...this.state,
                publish_image:result
            }
        });

        this.updateImageDetails();
    }

}

const mapState=(state)=>{
    return{
        button:state.ViewButtonReducer.button,
        admin:state.AdminReducer,
        requestObject:state.AdminReducer.mainButtonRequestObject,
        imageObject:state.ViewButtonReducer.image,
        highlightImage:state.ViewButtonReducer.editting
    }
}
const mapProps=(dispatch)=>{
    return {
        getButton:(id)=>{ dispatch(ViewButtonActions.getButton(id)); },
        publishButton:(id)=>{ dispatch(MainButtonActions.publishButton(id)); },
        updateButton:(data,formdata,iconFlag)=>{ dispatch(ViewButtonActions.updatemainButton(data,formdata,iconFlag)); },
        changeTitle:(data)=>{ dispatch({type:'updateImageDetails', data}); },
        updateImage:(data,id,token)=>{ dispatch(ViewButtonActions.updateImage(data,id,token)); },
        setPublishImage:(button,image)=>{ dispatch(ViewButtonActions.setPublishImage(button,image)); },
        deleteImage:(button,image)=>{ dispatch(ViewButtonActions.deleteImage(button,image)); }
    }
}
export default connect(mapState,mapProps)(EditButtonImage);