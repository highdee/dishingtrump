import React , { Component } from "react";

class Sidebar extends Component{

    render(){
        return (
            <div className="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">
          
            <div className="logo">
              <a href="http://www.creative-tim.com" className="simple-text logo-mini">
                CT
              </a>
              <a href="http://www.creative-tim.com" className="simple-text logo-normal">
                Creative Tim
              </a>
            </div>
            <div className="sidebar-wrapper">
              <div className="user">
                <div className="photo">
                  <img src="../assets/img/faces/avatar.jpg" />
                </div>
                <div className="user-info">
                  <a data-toggle="collapse" href="#collapseExample" className="username">
                    <span>
                      Tania Andrew
                      <b className="caret"></b>
                    </span>
                  </a>
                  <div className="collapse" id="collapseExample">
                    <ul className="nav">
                      <li className="nav-item">
                        <a className="nav-link" href="#">
                          <span className="sidebar-mini"> MP </span>
                          <span className="sidebar-normal"> My Profile </span>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="#">
                          <span className="sidebar-mini"> EP </span>
                          <span className="sidebar-normal"> Edit Profile </span>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="#">
                          <span className="sidebar-mini"> S </span>
                          <span className="sidebar-normal"> Settings </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <ul className="nav">
                <li className={this.props.pagelink == 'dashboard'? 'nav-item active':'nav-item'}>
                  <a className="nav-link" href="/dashboard">
                    <i className="material-icons">dashboard</i>
                    <p> Dashboard </p>
                  </a>
                </li>
                <li className={this.props.pagelink == 'mainbutton'||'view-button'? 'nav-item active':'nav-item'}>
                  <a className="nav-link" data-toggle="collapse" href="#pagesExamples">
                    <i className="material-icons">image</i>
                    <p> BUTTONS
                      <b className="caret"></b>
                    </p>
                  </a>
                  <div className={this.props.pagelink == 'mainbutton'||'subbutton'? 'collapse show':'collapse'} id="pagesExamples">
                    <ul className="nav">
                      <li className={this.props.pagelink == 'mainbutton'? 'nav-item active':'nav-item'}>
                        <a className="nav-link" href="/mainbutton">
                          <span className="sidebar-mini"> M </span>
                          <span className="sidebar-normal"> Main Buttons </span>
                        </a>
                      </li>
                      <li className={this.props.pagelink == 'subbutton' || 'Subbutton' ? 'nav-item active':'nav-item'} className="nav-item ">
                        <a className="nav-link" href="/sub-buttons">
                          <span className="sidebar-mini"> S </span>
                          <span className="sidebar-normal"> Sub-Buttons </span>
                        </a>
                      </li>
                      <li className={this.props.pagelink == 'chatbutton' || 'Chatbutton' ? 'nav-item active':'nav-item'} className="nav-item ">
                        <a className="nav-link" href="/chat-buttons">
                          <span className="sidebar-mini"> S </span>
                          <span className="sidebar-normal"> Chat-Buttons </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

              </ul>
            </div>
          </div>
        )
    }
}

export default Sidebar;