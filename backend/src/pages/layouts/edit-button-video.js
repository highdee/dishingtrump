import React , { Component } from "react";
import MainButtonActions from "../../actions/mainbuttonActions";
import ViewButtonActions from "../../actions/viewButtonActions";
import {connect} from "react-redux";
import auth from '../../auth';


class EditButtonVideo extends Component{

    state={
        publish:false,
        display_icon:false,
        publish_image:false
    };

    render(){
        let button=this.props.button;

        let highlightVideo=this.props.highlightVideo.video.code == '' ? this.props.highlightVideo.video.url=="" ? this.props.highlightVideo.video.filename == '' ? '': this.props.highlightVideo.video.filename  : this.props.highlightVideo.video.url : this.props.highlightVideo.video.code;
        // console.log(this.props.highlightVideo);
        return(
            <div className="modal fade" id="editbuttonvideo" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">EDIT VIDEO</h5>

                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">

                            {
                                this.props.highlightVideo.video.code != '' ?
                                    (<span  dangerouslySetInnerHTML={{__html:highlightVideo}}></span>)
                                :
                                this.props.highlightVideo.video.url ?
                                    (<video id='display_video' controls style={{ width:'100%',height:'300px'}} src={ highlightVideo } ></video>)
                                :
                                     (<video id='display_video' controls style={{ width:'100%',height:'300px'}} src={ highlightVideo } ></video>)
                            }
                            <form method="#" action="#" onSubmit={this.updateVideo}>
                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label left"> VIDEO TITLE </label>
                                    <input type="text" className="form-control" onChange={this.updateVideoDetails} value={this.props.highlightVideo.video.title} id="updatevideotitle" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title" style={{left:'40%'}} className=""> VIDEO FILE </label>
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <button type="button" onClick={this.trigger2}  className="input-group-text" >
                                                { this.state.display_video ? <i onClick={this.removeFileVideo} onChange={this.updateVideoDetails}  className="material-icons">remove</i>: <i onClick={this.triggerVideo} className="material-icons">cloud_upload</i>}
                                            </button>
                                        </div>
                                        <input type='file' id="updatevideo" onChange={this.readVideo}/>
                                        <input type="text" className="form-control pl-4" onChange={this.updateVideoDetails}  value={this.props.highlightVideo.video.filename} id="updatevideofile" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label left"> VIDEO URL </label>
                                    <input type="text" className="form-control" onChange={this.updateVideoDetails}  value={this.props.highlightVideo.video.url} id="updatevideourl" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label left"> VIDEO DATE </label>
                                    <input type="date" className="form-control" onChange={this.updateVideoDetails}  value={this.props.highlightVideo.video.date} id="updatevideodate" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label col-12 left text-left p-0"> VIDEO EMBEDED CODE </label>
                                    <textarea className='textarea edit-140' id="updatevideocode" onChange={this.updateVideoDetails}  value={this.props.highlightVideo.video.code} style={{width:'100%'}}></textarea>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label col-12 left text-left p-0"> VIDEO TRANSCRIPT </label>
                                    <textarea className='textarea edit-140' id='updatevideotranscript' onChange={this.updateVideoDetails}  value={this.props.highlightVideo.video.transcript} style={{width:'100%'}}></textarea>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="title" className="bmd-label col-12 left text-left p-0"> VIDEO DESCRIPTION </label>
                                    <textarea className='textarea edit-140' id='updatevideodesc' onChange={this.updateVideoDetails}  value={this.props.highlightVideo.video.description} style={{width:'100%'}}></textarea>
                                </div>

                                <button disabled={this.props.highlightVideo.loading} type="submit" className="btn btn-rose">UPDATE</button>
                            </form>
                        </div>
                        <div className="modal-footer">
                            {/*<button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>*/}
                            {/*<button type="button" className="btn btn-primary">Save changes</button>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount(){}
    updateVideoDetails=()=>{
        let title=document.getElementById('updatevideotitle');
        let file=document.getElementById('updatevideofile');
        let code=document.getElementById('updatevideocode');
        let video=document.getElementById('updatevideo');
        let url=document.getElementById('updatevideourl');
        let date=document.getElementById('updatevideodate');
        let transcript=document.getElementById('updatevideotranscript');
        let desc=document.getElementById('updatevideodesc');

        let data={
            title:title.value,
            file:file.value,
            code:code.value,
            url:url.value,
            transcript:transcript.value,
        };
        // console.log(this.state.ublish_image);
        this.props.changeVideo(data);
    }
    trigger=()=>{
        document.getElementById('icon').click();
    }
    trigger2=()=>{
        document.getElementById('updatevideo').click();
    }
    updateVideo=(e)=>{
        e.preventDefault();

        if(this.props.imageObject.loading){
            return false;
        }

        let title=document.getElementById('updatevideotitle');
        let file=document.getElementById('updatevideo');
        let code=document.getElementById('updatevideocode');
        let date=document.getElementById('updatevideodate');
        let transcript=document.getElementById('updatevideotranscript');
        let desc=document.getElementById('updatevideodesc');
        let url=document.getElementById('updatevideourl');


        let id=this.props.button._id;
        let payload;


        if(title.value.trim() == '' && ( (code.value == '' && url.value ==''  && file.value =='') && this.props.highlightVideo.video.filename !="") ){
            alert('video title and embeded code is needed to create a video');
            return false;
        }


        if(file.files.length != 0 && this.state.display_icon){
            payload=new FormData();
            // console.log('files');
            payload.append('title',title.value);
            payload.append('code',code.value);
            payload.append('transcript',transcript.value);
            payload.append('description',desc.value);
            payload.append('date',date.value);
            payload.append('id',this.props.highlightVideo.video.id);
            payload.append('video',file.files[0]);
            payload.append('url',url.value);

            this.props.updateVideo(payload , this.props.button._id ,auth.getAuth().data.token);

        }else{
            payload=new FormData();

            payload.append('title',title.value);
            payload.append('code',code.value);
            payload.append('transcript',transcript.value);
            payload.append('description',desc.value);
            payload.append('date',date.value);
            payload.append('id',this.props.highlightVideo.video.id);
            payload.append('video',file.files[0]);
            payload.append('url',url.value);

            this.props.updateVideo(payload , this.props.button._id ,auth.getAuth().data.token);
        }

    }
    removeFile=()=>{
        this.setState(()=>{
            return {
                ...this.state,
                display_icon:false
            }
        });

        let image=document.getElementById('display_image');
        let url=document.getElementById('updateurl');
        url.value=this.props.highlightImage.image.url != '' ? this.props.highlightImage.image.url: this.props.highlightImage.image.filename;
        image.src=this.props.highlightImage.image.url != '' ? this.props.highlightImage.image.url: this.props.highlightImage.image.filename;

    }
    readUpdateImage=(e)=>{
        let el=e.target;
        if(el.files.length == 0){
            return false;
        }
        let files=el.files[0];
        // console.log(files);
        if(!(files.type =='image/jpeg' || files.type =='image/jpg' || files.type =='image/png')){
            alert("file format not supported");
            return false;
        }
        let url=document.getElementById('updateurl');
        url.value='';
        url.value=files.name;

        this.setState(()=>{
            return {
                ...this.state,
                display_icon: true
            }
        });

        let filer=new FileReader();
        filer.onload=(result)=>{
            document.getElementById('display_image').src=result.srcElement.result;
        }
        filer.readAsDataURL(files);
    }
    publish=()=>{
        this.setState(()=>{
            return{
                ...this.state,
                publish:!this.state.publish
            }
        });
        let title=document.getElementById('title');
        this.props.changeTitle(title.value,!this.state.publish);
    }
    readVideo=(e)=>{
        let file=e.target.files[0];
        let title=document.getElementById('updatevideotitle');
        let code=document.getElementById('updatevideocode');
        let video=document.getElementById('updatevideo');
        let url=document.getElementById('updatevideourl');
        let transcript=document.getElementById('updatevideotranscript');
        let desc=document.getElementById('updatevideodesc');

        let data={
            title:title.value,
            file:file.name,
            code:code.value,
            url:url.value,
            transcript:transcript.value,
            description:desc.value
        };
        // console.log(this.state.ublish_image);
        this.props.changeVideo(data);

    }
}

const mapState=(state)=>{
    return{
        button:state.ViewButtonReducer.button,
        admin:state.AdminReducer,
        requestObject:state.AdminReducer.mainButtonRequestObject,
        imageObject:state.ViewButtonReducer.image,
        highlightVideo:state.ViewButtonReducer.edittingVideo,
        videoObject:state.ViewButtonReducer.video,
    }
}
const mapProps=(dispatch)=>{
    return {
        getButton:(id)=>{ dispatch(ViewButtonActions.getButton(id)); },
        publishButton:(id)=>{ dispatch(MainButtonActions.publishButton(id)); },
        updateButton:(data,formdata,iconFlag)=>{ dispatch(ViewButtonActions.updatemainButton(data,formdata,iconFlag)); },
        changeVideo:(data)=>{ dispatch({type:'updateVideoDetails', data}); },
        updateVideo:(data,id,token)=>{ dispatch(ViewButtonActions.updateVideo(data,id,token)); },
        setPublishImage:(button,image)=>{ dispatch(ViewButtonActions.setPublishImage(button,image)); },
        deleteImage:(button,image)=>{ dispatch(ViewButtonActions.deleteImage(button,image)); },
        getButtonVideos:()=>{dispatch(ViewButtonActions.getButtonVideos())}
    }
}
export default connect(mapState,mapProps)(EditButtonVideo);