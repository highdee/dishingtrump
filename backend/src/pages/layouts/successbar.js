import React , { Component } from 'react';
import { connect } from 'react-redux';

class SuccessBar extends Component{

    render(){

        if(this.props.success.state){
            setTimeout(()=>{
                this.props.destroy();
            },2000);
        }

        if(this.props.success.state){
            return ( 
                <div id="success">
                    {this.props.success.message==''? 'Operation was successfull':this.props.success.message }
                </div>
            )
        }
        else{
            return (
             <span></span>  
            )
        }
        
    }
}

const mapState=(state)=>{
    return {
        success:state.AdminReducer.globalSuccess
    }
}
const mapProps=(dispatch)=>{
    return{
        destroy:()=>{dispatch({type:'close_globalNotification'})}
    }
}

export default connect(mapState,mapProps)(SuccessBar);