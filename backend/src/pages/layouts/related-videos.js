import React , { Component } from 'react';
import { connect } from 'react-redux';
import viewButtonActions from "../../actions/viewButtonActions";

class RelatedVideo extends  Component{
    render() {
        return(

            <div className="modal fade" id="relatedvideos" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">RELATED VIDEOS</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">

                            {
                                this.props.videos.loading ? <b>LOADING CONTENT.....</b> : <span></span>
                            }
                            <div className='row'>

                                {


                                    this.props.videos.data.map((video)=>{
                                        return (
                                            <div className="col-md-4" >
                                                <div className="card card-product">
                                                    <div className="card-header card-header-image" data-header-animation="true">
                                                        <a href="#pablo">
                                                            <img className="img" src="../assets/img/card-3.jpg" />
                                                        </a>
                                                    </div>
                                                    <div className="card-body">
                                                        <div className="card-actions text-center">
                                                            <button type="button" className="btn btn-danger btn-link fix-broken-card">
                                                                <i className="material-icons">build</i> Fix Header!
                                                            </button>
                                                            <button onClick={()=>{ this.props.relate(video.id)}} type="button" className="btn btn-success btn-link" rel="tooltip"
                                                                    data-placement="bottom" title="Edit">
                                                                <i className="material-icons">check</i>
                                                            </button>

                                                        </div>
                                                        <h4 className="card-title">
                                                            <a href="#pablo">{video.Title}</a>
                                                        </h4>

                                                    </div>

                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapstate=(state)=>{
    return{
        videos:state.ViewButtonReducer.relatedVideos
    }
}
const mapprops=(dispatch)=>{
    return{
        relate:(id)=>{ dispatch(viewButtonActions.relate(id)) }
    }
}

export  default  connect(mapstate,mapprops)(RelatedVideo);