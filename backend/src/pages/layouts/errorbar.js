import React , { Component } from 'react';
import { connect } from 'react-redux';

class ErrorBar extends Component{

    render(){
        if(this.props.error.state){
            setTimeout(()=>{
                this.props.destroy();
            },2000);
        }
       

        if(this.props.error.state){
            return (
                <div id="error">
                    {this.props.error.message==''? ' !An error occured while loading content ':this.props.error.message }
                </div>
            )
        }
        else{
            return (
             <span></span>  
            )
        }
        
    }
}

const mapState=(state)=>{
    return {
        error:state.AdminReducer.globalerror
    }
}
const mapProps=(dispatch)=>{
    return{
        destroy:()=>{dispatch({type:'close_globalNotification'})}
    }
}

export default connect(mapState,mapProps)(ErrorBar);