import React , { Component } from "react";
import MainButtonActions from "../../actions/mainbuttonActions";
import ViewButtonActions from "../../actions/viewButtonActions";
import {connect} from "react-redux";
import auth from '../../auth';


class EditButtonDocument extends Component {

    state = {
        publish: false,
        display_icon: false,
        publish_image: false
    };

    render() {
        let button = this.props.button;
        let highlightDoc = this.props.highlightDoc;
        return (
            <div className="modal fade" id="editbuttondocument" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">EDIT DOCUMENT</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form method="#" action="#" onSubmit={this.updateDoc}>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> DOCUMENT TITLE </label>
                                    <input type="text" className="form-control" value={highlightDoc.doc.title}
                                           onChange={this.updateDocDetails} id="updatedoctitle"/>
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> DOCUMENT URL </label>
                                    <input type="text" className="form-control" onChange={this.updateDocDetails}
                                           value={highlightDoc.doc.url} id="updatedocurl"/>
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> DATE </label>
                                    <input type="date" className="form-control" onChange={this.updateDocDetails}
                                           value={highlightDoc.doc.date} id="updatedocdate"/>
                                </div>
                                <div className="form-group mt-4">
                                    <label htmlFor="title" className="bmd-label left"> DOCUMENT AUTHOR </label>
                                    <input type="text" className="form-control" onChange={this.updateDocDetails}
                                           value={highlightDoc.doc.author} id="updatedocauthor"/>
                                </div>

                                <button disabled={this.props.highlightDoc.loading} type="submit"
                                        className="btn btn-rose">UPDATES
                                </button>
                            </form>
                        </div>
                        <div className="modal-footer">
                            {/*<button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>*/}
                            {/*<button type="button" className="btn btn-primary">Save changes</button>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
    }

    updateDocDetails = () => {
        let title = document.getElementById('updatedoctitle');
        let url = document.getElementById('updatedocurl');
        let author = document.getElementById('updatedocauthor');
        let date = document.getElementById('updatedocdate');

        let data = {
            title: title.value,
            url: url.value,
            date: date.value,
            author: author.value
        };
        this.props.changeTitle(data);
    }

    updateDoc = (e) => {
        e.preventDefault();

        if (this.props.imageObject.loading) {
            return false;
        }

        let title = document.getElementById('updatedoctitle');
        let author = document.getElementById('updatedocauthor');
        let url = document.getElementById('updatedocurl');
        let date = document.getElementById('updatedocdate');

        let id = this.props.button._id;

        if (title.value.trim() == '' || url.value.trim() == "") {
            alert("Document title / url is required to continue");
            return false;
        }


        let payload = {
            'title' : title.value,
            'author': author.value,
            'date': date.value,
            'id' : this.props.highlightDoc.doc.id,
            'url': url.value
        }

        this.props.updateDoc(payload, id, auth.getAuth().data.token);
    }


}
const mapState=(state)=>{
    return{
        button:state.ViewButtonReducer.button,
        admin:state.AdminReducer,
        requestObject:state.AdminReducer.mainButtonRequestObject,
        imageObject:state.ViewButtonReducer.image,
        highlightDoc:state.ViewButtonReducer.edittingDoc
    }
}
const mapProps=(dispatch)=>{
    return {
        getButton:(id)=>{ dispatch(ViewButtonActions.getButton(id)); },
        publishButton:(id)=>{ dispatch(MainButtonActions.publishButton(id)); },
        updateButton:(data,formdata,iconFlag)=>{ dispatch(ViewButtonActions.updatemainButton(data,formdata,iconFlag)); },
        changeTitle:(data)=>{ dispatch({type:'updateDocDetails', data}); },
        updateDoc:(data,id,token)=>{ dispatch(ViewButtonActions.updateDoc(data,id,token)); },
        setPublishImage:(button,image)=>{ dispatch(ViewButtonActions.setPublishImage(button,image)); },
        deleteImage:(button,image)=>{ dispatch(ViewButtonActions.deleteImage(button,image)); }
    }
}
export default connect(mapState,mapProps)(EditButtonDocument);