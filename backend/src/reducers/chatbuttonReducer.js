let init={
    selected_button:{
        title:'',publish:0,
        subbuttons:[],
        mobile:{}
    },
    chatButtons:[],
    loading:false,

}

const ChatButtonReducer=(state=init , action)=>{
    switch(action.type){
        case 'set_subbutton':
            return {
                ...state,
                selected_button:{
                    ...state.selected_button,
                    subbuttons: action.buttons
                }
            }
        case 'update_subbutton':
            return {
                ...state,
                selected_button:{
                    ...state.selected_button,
                    subbuttons: [
                        ...state.selected_button.subbuttons,action.button
                    ]
                }
            }
        case 'update_selected_button':
            return {
                ...state,
                selected_button:{
                    ...state.selected_button,
                    title: action.data.title,
                    imageurl:action.data.imageurl,
                    code:action.data.code,
                    mobile:{
                        ...state.selected_button.mobile,
                        imageurl:action.data.imageurl2,
                        code:action.data.code2,
                    }
                }
            }
        case 'set_selected_button':
            return {
                ...state,
                selected_button: {
                    ...state.selected_button,
                    ...action.button
                }
            }
        case 'set_loading':
            return{
                ...state,
                loading: action.status
            }
        case 'update_button':
            return {
                ...state,
                chatButtons: [
                    action.button , ...state.chatButtons
                ]
             }
        case 'set_chat_buttons':
            return {
                ...state,
                chatButtons: [
                    ...action.buttons
                ]
            }
        case 'delete_chat_buttons':
            let rem=state.chatButtons.filter(btn=> btn._id != action.id);
            return {
                ...state,
                chatButtons: [
                    ...rem
                ]
            }
        case 'publish_chat_buttons':
            let button=state.chatButtons.map(btn=> {
                if(btn._id == action.id) {
                    btn.publish=btn.publish == 1 ? 0 : 1;
                }
                return btn;
            });
            return {
                ...state,
                chatButtons: [
                    ...button
                ]
            }
        default:
            return state
    }
}

export  default   ChatButtonReducer;