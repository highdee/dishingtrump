const init={
    endpoint:"http://localhost:2000/admin/",
    endpoint2:"http://localhost:2000/",
    globalerror:{state:false,'message':''},
    globalSuccess:{state:false,'message':''},
    mainButtonRequestObject:{
         loading:false, 
    },
    verification:{
        isLoading:false,
        error:false,
        message:""
    }
}



const AdminReducer=(state=init , action)=>{
    switch(action.type){
        case 'mainButtonRequestObject':
            return{
                ...state,
                mainButtonRequestObject:{
                    ...state.mainButtonRequestObject,
                    loading:action.data
                }
            }
        case 'globalerror':
            return{
                ...state,
                globalerror:{
                    state:true,
                    message:action.message
                },
                globalSuccess:{
                    state:false,
                    message:action.message
                }
            }
        case 'globalSuccess': 
            return{
                ...state,
                globalerror:{
                    state:false,
                    message:action.message
                },
                globalSuccess:{
                    state:true,
                    message:action.message
                }
            }
        case 'close_globalNotification':
            return{
                ...state,
                globalerror:{
                    state:false,
                    message:''
                },
                globalSuccess:{
                    state:false,
                    message:''
                }
            }
        case 'is_loading':
            return {
                ...state,
                verification:{
                    ...state.verification,
                    isLoading:true,
                    error:false,
                    message:''
                }
            }
        case 'login_successfull':
            return{
                ...state,
                verification:{
                    ...state.verification,
                    isLoading:false,
                    error:false,
                    message:''
                }
            }
        case 'is_error':
            return {
                ...state,
                verification:{
                    ...state.verification,
                    isLoading:false,
                    error:true,
                    message:action.message
                }
            }
        default:
            return state;
        
    }
    return state;
}


export default AdminReducer;