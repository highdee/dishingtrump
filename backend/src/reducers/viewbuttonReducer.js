const init={
    button:{
        videos:[],
        articles:[],
        documents:[],
        images:[],
        title:'',
        subbutton:[]
    },
    subbuttons:[],
    image:{
        loading:false
    },
    video:{
        loading:false
    },
    doc:{
        loading:false
    },
    article:{
        loading:false
    },
    editting:{
        loading:false,
        image:{}
    },
    edittingVideo:{
        loading:false,
        video:{}
    },
    edittingDoc:{
        loading:false,
        doc:{}
    },
    edittingArticle:{
        loading:false,
        article:{}
    },
    relatedVideos:{
        data:[],
        loading:false
    }
}

const ViewButtonReducer=(state=init , action)=>{

    switch (action.type) {
        case 'set_related_videos_loading':
            return{
                ...state,
                relatedVideos: {
                    ...state.relatedVideos,
                    loading: action.value
                }
        }
        case 'set_related_videos':
            return{
                ...state,
                relatedVideos: {
                    ...state.relatedVideos,
                    data: action.data
                }
            }
        case 'set_article_edit_status':
            return{
                ...state,
                edittingArticle:{
                    ...state.edittingArticle,
                    loading:action.status
                }
            }
        case 'updateArticleDetails':
            return{
                ...state,
                edittingArticle:{
                    article:{
                        ...state.edittingArticle.article,
                        title:action.data.title,
                        url:action.data.url,
                        date:action.data.date,
                        paragraph:action.data.paragraph,
                        author:action.data.author,
                        description:action.data.description,
                    }
                }
            }
        case 'highlightArticleEdit':
            return{
                ...state,
                button:{
                    ...state.button,
                    ...action.button
                },
                edittingArticle:{
                    ...state.edittingArticle,
                    button:action.button,
                    article:action.article
                }
            }
        case 'update_button_articles':
            return{
                ...state,
                button:{
                    ...state.button,
                    articles:action.articles
                }
            }
        case 'change_article_object':
            return{
                ...state,
                article:{
                    ...state.article,
                    loading:action.status
                }
            }
        case 'set_doc_edit_status':
            return{
                ...state,
                edittingDoc:{
                    ...state.edittingDoc,
                    loading:action.status
                }
        }
        case 'highlightDocEdit':
            return{
                ...state,
                button:{
                    ...state.button,
                    ...action.button
                },
                edittingDoc:{
                    ...state.edittingDoc,
                    button:action.button,
                    doc:action.documents
                }
            }
        case 'updateDocDetails':
            return{
                ...state,
                edittingDoc:{
                    doc:{
                        ...state.edittingDoc.doc,
                        title:action.data.title,
                        date:action.data.date,
                        author:action.data.author,
                        url:action.data.url,
                    }
                }
            }
        case 'update_button_documents':
            return{
                ...state,
                button:{
                    ...state.button,
                    doc:action.documents
                }
            }
        case 'change_doc_object':
            return{
                ...state,
                doc:{
                    ...state.doc,
                    loading:action.status
                }
            }
        case 'updateVideoDetails':
            console.log(action.data);
            return{
                ...state,
                edittingVideo:{
                    video:{
                        ...state.edittingVideo.video,
                        title:action.data.title,
                        code:action.data.code,
                        url:action.data.url,
                        transcript:action.data.transcript,
                        filename:action.data.file,
                        description:action.data.description,
                        date:action.data.date
                    }
                }
            }
        case 'set_video_edit_status':
            return{
                ...state,
                edittingVideo:{
                    ...state.edittingVideo,
                    loading:action.status
                }
        }
        case 'highlightVideoEdit':
            return{
                ...state,
                button:{
                    ...state.button,
                    ...action.button
                },
                edittingVideo:{
                    ...state.edittingVideo,
                    button:action.button,
                    video:action.video
                }
        }
        case 'update_button_videos':
            return{
                ...state,
                button:{
                    ...state.button,
                    videos:action.videos
                }
            }
        case 'change_video_object':
            return{
                ...state,
                video:{
                    ...state.video,
                    loading:action.status
                }
            }
        case 'set_edit_status':
            return{
                ...state,
                editting:{
                    ...state.editting,
                    loading:action.status
                }
            }
        case 'updateImageDetails':
            return{
                ...state,
                editting:{
                    image:{
                        ...state.editting.image,
                        title:action.data.title,
                        credit:action.data.credit,
                        desc:action.data.desc,
                        publish:action.data.publish,
                    }
                }
            }
        case 'highlightImageEdit':
            return{
                ...state,
                button:{
                    ...state.button,
                    ...action.button
                },
                editting:{
                    ...state.editting,
                    button:action.button,
                    image:action.image
                }
            }
        case 'update_button_image':
            return{
                ...state,
                button:{
                    ...state.button,
                    images:action.images
                }
        }
        case 'change_image_object':
            return{
                ...state,
                image:{
                    ...state.image,
                    loading:action.status
                }
            }
        case 'set_button':
            return{
                ...state,
                button: {
                    ...state.button,
                    ...action.button
                }
            }
        case 'set_sub_button':
            return{
                ...state,
                subbuttons: action.button
            }
        case 'set_button_subbutton':
            return{
                ...state,
                button: {
                    ...state.button,
                    subbutton: action.button
                }
            }
        case 'remove_sub_button':
            return{
                ...state,
                button: {
                    ...state.button,
                    subbutton: state.button.subbutton.filter((btn)=> btn._id != action.id)
                },
                subbuttons:state.subbuttons.filter((btn)=> btn._id != action.id)
            }
        case 'update_sub_button':
            return{
                ...state,
                button: {
                    ...state.button,
                    subbutton:[
                        ...state.button.subbutton,
                        action.button
                    ]
                }
            }
        case 'set_title':
            return{
                ...state,
                button: {
                    ...state.button,
                    title:action.title,
                    publish:action.publish
                }
            }
        default:
            return state;
    }
    return state;
}

export  default  ViewButtonReducer;