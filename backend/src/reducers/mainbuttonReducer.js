const init={
    buttons:[],
    searching:true
}

const MainButtonReducer=(state=init,action)=>{
    switch(action.type){
        case 'toggle_search':
            return {
                ...state,
                searching: action.value
            }
        case 'set_buttons':
            return{
                ...state,
                buttons:action.buttons
            }
        case 'add_button':
            return{
                ...state,
                buttons:[...action.buttons , ...state.buttons]
            }
        case 'remove_button': 
            return{
                ...state,
                buttons:state.buttons.filter((btn)=> btn._id != action.id),

            }
        case 'update_button_publish': 
            return{
                ...state,
                buttons:[
                    ...state.buttons, 
                ]
            }
        default:
            return state;
    }
}

export default MainButtonReducer;