const init={
    stats:{
        buttons:0,
        users:0
    }
}

const DashboardReducer=(state=init , action)=>{
    switch (action.type) {
        case 'update_stat':
            return{
                ...state,
                stats:{
                    ...state.stats,
                    [action.field]:action.data
                }
            }
        default:
            return state;
    }
}

export default  DashboardReducer;