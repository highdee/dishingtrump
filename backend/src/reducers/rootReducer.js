import { combineReducers } from "redux";
import AdminReducer from "./adminReducer";
import MainButtonReducer from './mainbuttonReducer';  
import ViewButtonReducer from "./viewbuttonReducer";
import DashboardReducer from "./dashboardReducer";
import ChatButtonReducer from "./chatbuttonReducer";


const rootReducer=combineReducers({
    AdminReducer:AdminReducer,
    MainButtonReducer:MainButtonReducer,
    ViewButtonReducer:ViewButtonReducer,
    DashboardReducer:DashboardReducer,
    ChatButtonReducer:ChatButtonReducer
});

export default rootReducer;