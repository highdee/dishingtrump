class auth {
    
    constructor(){
        this.admin=localStorage.getItem('admin');
        this.admin=JSON.parse(this.admin); 
    }
    checkAuth(){
         if(this.admin){
             return true;
         }
         return false;
    }
    getAuth(){
        return this.admin;
    }
}

export default new auth();