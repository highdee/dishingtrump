import axios from 'axios';
import auth from "../auth";


const checkToken=(code)=>{
    if(code == -111){
        localStorage.removeItem('admin');
        window.location="/";
        alert('session time out');
    }
}

const getButton=(id)=>{
    return (dispatch,getstate)=>{
        console.log(id);
        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/view-button/"+id).
            then((data)=>{
                console.log(data);

                let button=processButton(getstate,data.data);
                dispatch({type:"set_button",button:button});
                console.log(button);
             }).
            catch(error=>{
                console.log(error);
            })

    }
}

const getSubButton=(id)=>{
    return (dispatch,getstate)=>{

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/view-sub-button").
        then((data)=>{
            console.log(data);
            let button=processButtons(getstate,data.data);
            dispatch({type:"toggle_search",value:false});
            dispatch({type:"set_sub_button",button:button});
        }).
        catch(error=>{
            console.log(error);
            dispatch({type:'globalerror',message:"error in connections"});
        })

    }
}
const getButtonSubButtons=(id)=>{
    return (dispatch,getstate)=>{

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/view-button-subbutton/"+id).
        then((data)=>{
            let button=processButtons(getstate,data.data);


            // if(getstate().ViewButtonReducer.button._id){
                dispatch({type:"set_button_subbutton",button:button});
            // }else{
                dispatch({type:"set_subbutton",buttons:button});
            // }

        }).
        catch(error=>{
            console.log(error);
        })

    }
}
const updatemainButton=(data , icon , iconAvailable)=>{
    return (dispatch , getstate)=>{
        console.log(data);

        dispatch({type:'mainButtonRequestObject','data':true});

        let token=auth.getAuth().data.token;


        axios.post(getstate().AdminReducer.endpoint2+"leftButtons/update-main-buttons",{token:token,...data})
            .then((data)=> {
                checkToken(data.data.code);

                if (iconAvailable) {
                    axios.post(getstate().AdminReducer.endpoint2 + "leftButtons/create-main-buttons-icon", icon, {
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then((data) => {
                        console.log(data);
                        dispatch({
                            type: 'set_button',
                            button: processButton(getstate,data.data.data.button)
                        });

                        window.location.reload();
                    });
                }

                dispatch({type:'mainButtonRequestObject','data':false});

                console.log(data);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    if(!iconAvailable){
                        dispatch({type:'set_button',button:processButton(getstate,data.data.data.button)});
                    }
                }
            })
            .catch((error)=>{
                console.log(error);
                dispatch({type:'mainButtonRequestObject','data':false});
                if(error.request && error.request.status == 400){
                    let err=JSON.parse(error.request.response);
                    dispatch({type:'globalerror',message:err.message});
                }else{
                    dispatch({type:'globalerror',message:'unknown error occured while creating a button'})
                }

            });
    }
}
const addImage=(data,id,token)=>{
    return (dispatch,getstate)=>{
        console.log(data);

        dispatch({type:'change_image_object',status:true});
        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/add-buttom-image/"+token+'/'+id , data ,
            {
                headers:{'Content-Type':'application/x-www-form-urlencoded'}
            })
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
                dispatch({type:'change_image_object',status:false});
            })
            .catch((error)=>{
                // console.log(error);

                dispatch({type:'change_image_object',status:false});
                if(error.request){
                    console.log(error.request);
                    if(error.request.status == 400){
                        dispatch({type:'globalError',message:data.data.message});
                        return false;
                    }
                }
                dispatch({type:'globalError',message:'Error in connection.'});

            })
        ;
    }
}
const setPublishImage=(button,image)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let images=getstate().ViewButtonReducer.button.images.map((img)=> {
            if( img.id == image){
                img.publish=img.publish == 1 ? 0 : 1;
            }
            return img;
        });

        dispatch({type:'update_button_image',images:images});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/publish-buttom-image/"+token+'/'+button+'/'+image)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
                dispatch({type:'change_image_object',status:false});
            })
            .catch((error)=>{
                console.log(error);
            });
    }
}
const deleteImage=(button,image)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let images=getstate().ViewButtonReducer.button.images.filter((img)=> {
            return img.id != image;
        });

        dispatch({type:'update_button_image',images:images});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/delete-buttom-image/"+token+'/'+button+'/'+image)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
                dispatch({type:'change_image_object',status:false});
            })
            .catch((error)=>{
                console.log(error);
            });
    }
}
const updateImage=(data,id,token)=>{
    return (dispatch,getstate)=>{
        console.log(data);

        dispatch({type:'set_edit_status',status:true});
        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/update-buttom-image/"+token+'/'+id , data ,
            {
                headers:{'Content-Type':'application/x-www-form-urlencoded'}
            })
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'highlightImageEdit',button:processButton(getstate,data.data.data) , image:processImage(getstate,data.data.image)});
                }
                dispatch({type:'set_edit_status',status:false});
            })
            .catch((error)=>{
                console.log(error);

                dispatch({type:'set_edit_status',status:false});
                if(error.request){
                    console.log(error.request);
                    if(error.request.status == 400){
                        dispatch({type:'globalError',message:data.data.message});
                        return false;
                    }
                }
                dispatch({type:'globalError',message:'Error in connection.'});

            })
        ;
    }
}
const highlightImageEdit=(id,image)=>{
    return (dispatch,getstate)=>{
        let button=getstate().ViewButtonReducer.button;
        let highlightedImage=button.images.find((img)=>{
            return img.id == image;
        });
        // console.log(highlightedImage);
        dispatch({type:'highlightImageEdit',button:button,image:highlightedImage});
    }
}
const processButton=(getstate,button)=>{
    button.icon=getstate().AdminReducer.endpoint2+button.icon;
    button.images=button.images.map((img)=>{
        if(img.filename != ''){
            img.filename=getstate().AdminReducer.endpoint2+'button_images/'+img.filename;
        }
        img.publish=img.publish == 1 ? true:false ;
        return img;
    });
    return button;
}
const processButtons=(getstate,buttons)=>{
   let new_buttons= buttons.map(button=>{
        button.icon=getstate().AdminReducer.endpoint2+button.icon;
        button.images=button.images.map((img)=>{
            if(img.filename != ''){
                img.filename=getstate().AdminReducer.endpoint2+'button_images/'+img.filename;
            }
            img.publish=img.publish == 1 ? true:false ;
            return img;
        });
        return button;
    });

   return new_buttons;
}
const processImage=(getstate,image)=>{
    if(image.filename != ''){
        image.filename=getstate().AdminReducer.endpoint2+'button_images/'+image.filename;
    }
    return image;
}

const addVideo=(data,id,token)=>{
    return (dispatch,getstate)=>{
        console.log(data);

        dispatch({type:'change_video_object',status:true});
        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/add-button-video/"+token+'/'+id , data ,
            {
                headers:{'Content-Type':'application/x-www-form-urlencoded'}
            })
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'update_button_videos',videos:data.data.data.videos});
                }

                dispatch({type:'change_video_object',status:false});
            })
            .catch((error)=>{
                console.log(error);

                //

            })
        ;
    }
}
const deleteVideo=(button,video)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let videos=getstate().ViewButtonReducer.button.videos.filter((vid)=> {
            return vid.id != video;
        });

        dispatch({type:'update_button_videos',videos:videos});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/delete-button-video/"+token+'/'+button+'/'+video)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
                dispatch({type:'change_image_object',status:false});
            })
            .catch((error)=>{
                console.log(error);
            });
    }
}
const setPublishVideo=(button,video)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let videos=getstate().ViewButtonReducer.button.videos.map((vid)=> {
            if( vid.id == video){
                vid.publish=vid.publish == 1 ? 0 : 1;
            }
            return vid;
        });

        dispatch({type:'update_button_videos',videos:videos});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/publish-buttom-video/"+token+'/'+button+'/'+video)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    // dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
                dispatch({type:'change_image_object',status:false});
            })
            .catch((error)=>{
                console.log(error.request);
            });
    }
}
const highlightVideoEdit=(id,video)=>{
    return (dispatch,getstate)=>{
        let button=getstate().ViewButtonReducer.button;
        let highlightedVideo=button.videos.find((vid)=>{
            return vid.id == id;
        });
        dispatch({type:'highlightVideoEdit',button:button,video:processVideo(getstate,highlightedVideo)});
    }
}
const updateVideo=(data,id,token)=>{
    return (dispatch,getstate)=>{


        dispatch({type:'set_video_edit_status',status:true});
        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/update-button-video/"+token+'/'+id , data ,
            {
                headers:{'Content-Type':'application/x-www-form-urlencoded'}
            })
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'highlightVideoEdit',button:processButton(getstate,data.data.data) , video:processVideo(getstate,data.data.video)});
                }
                dispatch({type:'set_video_edit_status',status:false});
            })
            .catch((error)=>{
                console.log(error);

                dispatch({type:'set_video_edit_status',status:false});
                if(error.request){
                    console.log(error.request);
                    if(error.request.status == 400){
                        dispatch({type:'globalError',message:data.data.message});
                        return false;
                    }
                }
                dispatch({type:'globalError',message:'Error in connection.'});

            })
        ;
    }

}
const processVideo=(getstate,video)=>{
    if(video.filename != ''){
        video.filename=getstate().AdminReducer.endpoint2+'button_videos/'+video.filename;
    }
    return video;
}


const setPublishDoc=(button,doc)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let docs=getstate().ViewButtonReducer.button.documents.map((dc)=> {
            if( dc.id == doc){
                dc.publish=dc.publish == 1 ? 0 : 1;
            }
            return dc;
        });

        console.log(docs);
        dispatch({type:'update_button_documents',documents:docs});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/publish-button-documents/"+token+'/'+button+'/'+doc)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    // dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
            })
            .catch((error)=>{
                console.log(error.request);
            });
    }
}
const addDoc=(data,id,token)=>{
    return (dispatch,getstate)=>{

        dispatch({type:'change_doc_object',status:true});
        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/add-button-document/"+token+'/'+id , data)
            .then((data)=>{
                console.log(data);

                // checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'update_button_document',videos:data.data.data.videos});
                }

                dispatch({type:'change_doc_object',status:false});
            })
            .catch((error)=>{
                console.log(error);

                //

            })
        ;
    }
}
const deleteDoc=(button,doc)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let docs=getstate().ViewButtonReducer.button.documents.filter((dc)=> {
            return dc.id != doc;
        });
        dispatch({type:'update_button_documents',documents:docs});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/delete-button-documents/"+token+'/'+button+'/'+doc)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
                dispatch({type:'change_image_object',status:false});
            })
            .catch((error)=>{
                console.log(error);
            });
    }
}
const highlightDocEdit=(id)=>{
    return (dispatch,getstate)=>{
        let button=getstate().ViewButtonReducer.button;
        let highlightedDoc=button.documents.find((dc)=>{
            return dc.id == id;
        });
        dispatch({type:'highlightDocEdit',button:button,documents:highlightedDoc});
    }
}
const updateDoc=(data,id,token)=>{
    return (dispatch,getstate)=>{


        dispatch({type:'set_doc_edit_status',status:true});
        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/update-button-document/"+token+'/'+id , data)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'highlightDocEdit',button:processButton(getstate,data.data.data) , documents:data.data.doc});
                }
            dispatch({type:'set_doc_edit_status',status:false});
            })
            .catch((error)=>{
                console.log(error);

                dispatch({type:'set_doc_edit_status',status:false});
                if(error.request){
                    console.log(error.request);
                    if(error.request.status == 400){
                        dispatch({type:'globalError',message:data.data.message});
                        return false;
                    }
                }
                dispatch({type:'globalError',message:'Error in connection.'});

            })
        ;
    }

}

const addArticle=(data,id,token)=>{
    return (dispatch,getstate)=>{
        console.log(data);

        dispatch({type:'change_article_object',status:true});

        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/add-button-article/"+token+'/'+id , data ,
            {
                headers:{'Content-Type':'application/x-www-form-urlencoded'}
            })
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'update_button_articles',articles:data.data.data.articles});
                }

                dispatch({type:'change_article_object',status:false});
            })
            .catch((error)=>{
                console.log(error);

                //

            })
        ;
    }
}
const deleteArticle=(button,article)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let artic=getstate().ViewButtonReducer.button.articles.filter((art)=> {
            return art.id != article;
        });
        dispatch({type:'update_button_articles',articles:artic});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/delete-button-article/"+token+'/'+button+'/'+article)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
                dispatch({type:'change_article_object',status:false});
            })
            .catch((error)=>{
                console.log(error);
            });
    }
}
const setPublishArticle=(button,doc)=>{
    return (dispatch,getstate)=>{
        let token=auth.getAuth().data.token;
        let docs=getstate().ViewButtonReducer.button.articles.map((dc)=> {
            if( dc.id == doc){
                dc.publish=dc.publish == 1 ? 0 : 1;
            }
            return dc;
        });

        dispatch({type:'update_button_articles',articles:docs});

        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/publish-button-articles/"+token+'/'+button+'/'+doc)
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    // dispatch({type:'set_button',button:processButton(getstate,data.data.data)});
                }
            })
            .catch((error)=>{
                console.log(error.request);
            });
    }
}
const highlightArticleEdit=(id)=>{
    return (dispatch,getstate)=>{
        let button=getstate().ViewButtonReducer.button;
        let highlightedarticle=button.articles.find((dc)=>{
            return dc.id == id;
        });
        dispatch({type:'highlightArticleEdit',button:button,article:processArticle(getstate,highlightedarticle)});
    }
}
const updateArticle=(data,id,token)=>{
    return (dispatch,getstate)=>{

        dispatch({type:'change_article_object',status:true});
        axios.post(getstate().AdminReducer.endpoint2+"leftbuttons/update-button-article/"+token+'/'+id , data ,
            {
                headers:{'Content-Type':'application/x-www-form-urlencoded'}
            })
            .then((data)=>{
                console.log(data);
                checkToken(data.data.code);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    dispatch({type:'highlightArticleEdit',button:processButton(getstate,data.data.data) , article:processArticle(getstate,data.data.article)});
                }
                dispatch({type:'change_article_object',status:false});
            })
            .catch((error)=>{
                console.log(error);

                dispatch({type:'change_article_object',status:false});
                if(error.request){
                    console.log(error.request);
                    if(error.request.status == 400){
                        dispatch({type:'globalError',message:data.data.message});
                        return false;
                    }
                }
                dispatch({type:'globalError',message:'Error in connection.'});

            })
        ;
    }

}
const processArticle=(getstate,article)=>{
    if(article.filename != ''){
        article.filename=getstate().AdminReducer.endpoint2+'button_articleimages/'+article.filename;
    }
    return article;
}

const getButtonVideos=()=>{
    return (dispatch,getstate)=>{
        dispatch({type:'set_related_videos_loading' , value:true});
        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/view-sub-button-videos")
            .then((data)=>{
                console.log(data);
                dispatch({type:'set_related_videos' , data:data.data});
                dispatch({type:'set_related_videos_loading' , value:false});
            })
            .catch((error)=>{
                console.log(error);
                dispatch({type:'set_related_videos_loading' , value:false});
            });
    }
}



let ViewButtonActions={};
export default  ViewButtonActions={
    getButton,getSubButton,getButtonSubButtons,updatemainButton,addImage,setPublishImage,
    setPublishVideo,deleteImage,highlightImageEdit,updateImage,
    addVideo,deleteVideo,highlightVideoEdit,updateVideo,addDoc,
    deleteDoc,setPublishDoc,highlightDocEdit,updateDoc,
    addArticle,deleteArticle,setPublishArticle,highlightArticleEdit,
    updateArticle,getButtonVideos,
}
