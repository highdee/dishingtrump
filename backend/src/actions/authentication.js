import axios from 'axios';


const login=(data)=>{
    return (dispatch , getstate)=>{ 
        axios.post(getstate().AdminReducer.endpoint+'login',data).
        then(data=>{
            console.log(data);
            localStorage.setItem('admin',JSON.stringify(data));
            dispatch({type:'login_successfull'});
            window.location="/dashboard";
        })
        .catch(error=>{ 
            console.log(error.request);
            if(error.request.status ==  '401')
            {
                if(error.request.response){
                    let errorm=JSON.parse(error.request.response);
                    dispatch({type:'is_error', message:errorm.message});
                }
            }

        });
    }
}

let Authentication={};
export default Authentication={
    login:login
}