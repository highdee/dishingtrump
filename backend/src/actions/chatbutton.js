import axios from 'axios';


const getChatButtons=()=>{
    return (dispatch,getstate)=>{
        axios.get(getstate().AdminReducer.endpoint2+'leftbuttons/get-chat-buttons')
            .then(data=>{
                console.log(data);
                dispatch({type:'set_chat_buttons',buttons:data.data})
            })
            .catch(error=>{
                console.log(error);
                dispatch({type: 'globalerror',message:'unknown error occured',status:true});
            });
    }
}

const deleteChatButtons=(id)=>{
    return (dispatch,getstate)=>{
        axios.delete(getstate().AdminReducer.endpoint2+'leftbuttons/delete-chat-button/'+id)
            .then(data=>{
                console.log(data);
                dispatch({type:'delete_chat_buttons',id:id})
            })
            .catch(error=>{
                console.log(error);
                dispatch({type: 'globalerror',message:'Unable to delete button',status:true});
            });
    }
}

const publishChatButtons=(id)=>{
    return (dispatch,getstate)=>{
        console.log(id);
        dispatch({type:'publish_chat_buttons',id:id});

        axios.post(getstate().AdminReducer.endpoint2+'leftbuttons/publish-chat-button/'+id)
            .then(data=>{
                console.log(data);
            })
            .catch(error=>{
                console.log(error.request);
                dispatch({type: 'globalerror',message:'Unable to publish / Unpublish button',status:true});
            });
    }
}

const createChatButtons=(data)=>{
    return (dispatch,getstate)=>{
        dispatch({type:'set_loading',status:true});

        axios.post(getstate().AdminReducer.endpoint2+'leftbuttons/create-chat-button' , data , {
            headers:{'Content-Type':'application/x-www-form-urlencoded'}
        })
            .then(data=>{
                console.log(data);
                dispatch({type:'globalSuccess',message:"Chat buton was created successfully"});
                dispatch({type:'update_button' ,button:data.data});
                dispatch({type:'set_loading',status:false});
            })
            .catch(error=>{
                console.log(error);
                dispatch({type: 'globalerror',message:'Unable to create chat button',status:true});
                dispatch({type:'set_loading',status:false});
            });
    }
}


const updateChatButtons=(data , id)=>{
    return (dispatch,getstate)=>{
        dispatch({type:'set_loading',status:true});

        axios.post(getstate().AdminReducer.endpoint2+'leftbuttons/update-chat-button/'+id , data , {
            headers:{'Content-Type':'application/x-www-form-urlencoded'}
        })
            .then(data=>{
                console.log(data);
                data.data=processButton(data.data , getstate);
                dispatch({type:'set_selected_button',button:data.data})
                dispatch({type:'globalSuccess',message:"Chat buton was updated successfully"});
                dispatch({type:'set_loading',status:false});
            })
            .catch(error=>{
                console.log(error);
                dispatch({type: 'globalerror',message:'Unable to create chat button',status:true});
                dispatch({type:'set_loading',status:false});
            });
    }
}


const getAChatButton=(id)=>{
    return (dispatch,getstate)=>{
        axios.get(getstate().AdminReducer.endpoint2+'leftbuttons/get-chat-button/'+id)
            .then(data=>{
                data.data=processButton(data.data , getstate)
                dispatch({type:'set_selected_button',button:data.data})
            })
            .catch(error=>{
                console.log(error);
                dispatch({type: 'globalerror',message:'unknown error occured',status:true});
            });
    }
}
const processButton=(button , getstate)=>{
    button.video=button.video && button.video != '' ? getstate().AdminReducer.endpoint2+'chatbuttoncontent/'+button.video:'';
    button.image=button.image && button.image != '' ? getstate().AdminReducer.endpoint2+'chatbuttoncontent/'+button.image:'';

    button.mobile.video=button.mobile.video && button.mobile.video != '' ? getstate().AdminReducer.endpoint2+'chatbuttoncontent/'+button.mobile.video:'';
    button.mobile.image=button.mobile.image && button.mobile.image != '' ? getstate().AdminReducer.endpoint2+'chatbuttoncontent/'+button.mobile.image:'';

    return button;
}

let ChatButtonActions={};
export  default ChatButtonActions={
    getChatButtons , deleteChatButtons , publishChatButtons ,createChatButtons,
    getAChatButton , updateChatButtons
}