import axios from 'axios';
import auth from "../auth";

const checkToken=(code)=>{
    if(code == -111){
        localStorage.removeItem('admin');
        window.location="/";
        alert('session time out');
    }
}
const getButtons=()=>{
    return (dispatch,getstate)=>{

        axios.get(getstate().AdminReducer.endpoint2+'leftButtons/mainbutton').then(data=>{
            
            let buttons=processButton(data.data,getstate().AdminReducer);
            // console.log(buttons); 
            dispatch({type:'set_buttons' , buttons:buttons});

            }).catch(error=>{
                console.log(error); 
                dispatch({type:'globalerror',message:""});
            });
    }
}

const searchButtons=(input)=>{
    return (dispatch,getstate)=>{
        dispatch({type:"toggle_search",value:true});
        dispatch({type:"set_sub_button",button:[]});
        axios.post(getstate().AdminReducer.endpoint2+'leftButtons/searchbutton', input).
        then((data)=>{
            console.log(data);
            let button=processButton(data.data,getstate().AdminReducer);
            dispatch({type:"set_sub_button",button:button});
            dispatch({type:"toggle_search",value:false});
        }).
        catch(error=>{
            console.log(error);
            dispatch({type:"toggle_search",value:false});
            dispatch({type:'globalerror',message:"error in connections"});
        })
    }
}
 
const publishButton=(id)=>{
    return (dispatch,getstate)=>{ 
        let button=getstate().MainButtonReducer.buttons.find((btn)=>{ 
            return btn._id == id;
        });
        
        if (!button) return false;

        if(button.publish == 0){
            button.publish=1;
           dispatch({type:'update_button_publish'});
        }else{
            button.publish=0;
            dispatch({type:'update_button_publish'});
        }
 
        let token=auth.getAuth().data.token;
        axios.post(getstate().AdminReducer.endpoint2+"leftButtons/publish-main-button/"+id, {token:token})
        .then((data)=>{
            console.log(data);
            checkToken(data.data.code);
        })
        .catch((error)=>{
            if(button.publish == 0){
                button.publish=1;
               dispatch({type:'update_button_publish'});
            }else{
                button.publish=0;
                dispatch({type:'update_button_publish'});
            }
            console.log(error);
        });
    }
}

const createmainButton=(data,icon,iconAvailable)=>{ 
        return ((dispatch , getstate)=>{

            dispatch({type:'mainButtonRequestObject','data':true});

            let token=auth.getAuth().data.token;

            axios.post(getstate().AdminReducer.endpoint2+"leftButtons/create-main-buttons",{token:token,...data})
            .then((data)=>{
                checkToken(data.data.code);
                if(iconAvailable){
                    axios.post(getstate().AdminReducer.endpoint2+"leftButtons/create-main-buttons-icon",icon,{
                        headers:{'Content-Type':'application/x-www-form-urlencoded'}
                    }).then(()=>{ 
                        dispatch({type:'add_button',buttons:processButton([data.data.data.button],getstate().AdminReducer)});
                    });
                }

            dispatch({type:'mainButtonRequestObject','data':false}); 

            console.log(data);

            if(data.data.code == 1){ 
                dispatch({type:'globalSuccess',message:data.data.message});
                if(!iconAvailable){
                    dispatch({type:'add_button',buttons:processButton([data.data.data.button],getstate().AdminReducer)});
                }
            }
        })
        .catch((error)=>{
            console.log(error); 
            dispatch({type:'mainButtonRequestObject','data':false});
            if(error.request && error.request.status == 400){
                let err=JSON.parse(error.request.response);
                dispatch({type:'globalerror',message:err.message});
            }else{
                dispatch({type:'globalerror',message:'unknown error occured while creating a button'})
            }
            
        }); 
    });
}

const createsubButton=(data)=>{
    return ((dispatch , getstate)=>{

        dispatch({type:'mainButtonRequestObject','data':true});

        let token=auth.getAuth().data.token;

        axios.post(getstate().AdminReducer.endpoint2+"leftButtons/create-sub-buttons/"+token , data ,{
            headers:{'Content-Type':'application/x-www-form-urlencoded'}
        })
            .then((data)=>{
                checkToken(data.data.code);

                dispatch({type:'mainButtonRequestObject','data':false});

                console.log(data);

                if(data.data.code == 1){
                    dispatch({type:'globalSuccess',message:data.data.message});
                    if(getstate().ViewButtonReducer.button._id){
                        dispatch({type:'update_sub_button',button:processButtonSingle(getstate,data.data.data.button)});
                    }else{
                        dispatch({type:'update_subbutton',button:processButtonSingle(getstate,data.data.data.button)});
                    }

                }
            })
            .catch((error)=>{
                console.log(error);
                dispatch({type:'mainButtonRequestObject','data':false});
                if(error.request && error.request.status == 400){
                    let err=JSON.parse(error.request.response);
                    dispatch({type:'globalerror',message:err.message});
                }else{
                    dispatch({type:'globalerror',message:'unknown error occured while creating a button'})
                }

            });
    });
}

const deletemainButton=(id)=>{
    return (dispatch,getstate)=>{
        console.log(id);
        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/delete-main-button/"+auth.getAuth().data.token+'/'+id).
        then((data)=>{
            if(data.data.code == 1){
                console.log(data);
                dispatch({type:'globalSuccess',message:data.data.message});
                dispatch({type:'remove_button',id:data.data.data._id});
            }else{
                dispatch({type:'globalError',message:data.data.message});
            }
        })
        .catch((error)=>{
            console.log(error);
            dispatch({type:'globalError',message:'an unknown error occurred'});
        });
    }
}

const deleteSubButton=(id)=>{
    return (dispatch,getstate)=>{

        if(!window.confirm("Are you sure you want to delete this button")){
            return false;
        }
        dispatch({type:'remove_sub_button',id:id});
        axios.get(getstate().AdminReducer.endpoint2+"leftbuttons/delete-main-button/"+auth.getAuth().data.token+'/'+id).
        then((data)=>{
            console.log(data);
            if(data.data.code == 1){
                console.log(data);
                dispatch({type:'globalSuccess',message:data.data.message});
            }else{
                dispatch({type:'globalError',message:data.data.message});
            }
        })
            .catch((error)=>{
                console.log(error);
                dispatch({type:'globalError',message:'an unknown error occurred'});
            });
    }
}

const processButtonSingle=(getstate,button)=>{
    button.icon=getstate().AdminReducer.endpoint2+button.icon;
    button.images=button.images.map((img)=>{
        if(img.filename != ''){
            img.filename=getstate().AdminReducer.endpoint2+'button_images/'+img.filename;
        }
        img.publish=img.publish == 1 ? true:false ;
        return img;
    });
    return button;
}
const processButton=(data,admin)=>{
    return data.map((btn)=>{
        btn.icon=admin.endpoint2+btn.icon;
        return btn;
    });
}
 
let MainButtonActions={};
export default MainButtonActions={
    getButtons:getButtons,searchButtons,createsubButton,deleteSubButton,
    publishButton,createmainButton,deletemainButton
}