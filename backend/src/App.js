import React, { Component } from 'react';
import { BrowserRouter, Route , Switch } from "react-router-dom"; 
import Login from "./pages/login";
import Dashboard from "./pages/dashboard";
import Mainbutton from "./pages/mainbuttons";
import ProtectedRoute  from "./pages/protectedRoute";
import ViewButton from "./pages/view-button";
import SubButton from './pages/sub-button';
import ChatButton from './pages/chat-button';
import ViewChatButton from "./pages/view-chat-button";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">

        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Login}/>
                {/* <Route path="/dashboard" component={Dashboard}/> */}
                <ProtectedRoute path="/dashboard" component={Dashboard} />
                <ProtectedRoute path="/mainbutton" component={Mainbutton} />
                <ProtectedRoute path="/view-button/:id" component={ViewButton} />
                <ProtectedRoute path="/sub-buttons" component={SubButton} />
                <ProtectedRoute path="/chat-buttons" component={ChatButton} />
                <ProtectedRoute path="/view-chat-button/:id" component={ViewChatButton} />
            </Switch>
        </BrowserRouter>

      </div>
    );
  }
}

export default App;
