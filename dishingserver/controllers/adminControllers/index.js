var express=require('express');
var router=express.Router();
var bcrypt=require('bcryptjs'); 
const authMiddleware=require('../../middleware/adminauth');
var LocalStrategy = require('passport-local').Strategy;
const jwt=require('jsonwebtoken');

const main=require('../../models/main/mainbutton');
const main_subbutton=require('../../models/main/subbuttons');


const admin=require('../../models/admin/adminSchema');

var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
// var hash = bcrypt.hashSync("123", salt);
 
 
router.get('/dashboard',authMiddleware,function(req,res){ 
    // main.
    res.end(JSON.stringify('fdfs'));
});

router.post('/login' , function(req,res){
    let username=req.body.username;
    let password=req.body.password; 

    admin.findOne({ username: username }, function(err, adminUser) {
        if (err) { return res.status(401).json({code:-1,'message':"unknown error"}); }
        if (!adminUser) {
          return res.status(401).json({code:-1,'message':'Incorrect login credentials'});
        } 
        
        if (!bcrypt.compareSync(password,adminUser.password)) { 
          return res.status(401).json({code:-1,"message":'Incorrect login credentials'});
        } 
        
        const token=jwt.sign({
            username:adminUser.username,
            id:adminUser._id
        },"secret",{
            expiresIn:"10h"
        });

        return res.status(200).json({
            token:token,
            admin:{
                username:adminUser.username,
                id:adminUser._id
            }
        });
    });
    
});

module.exports=router;