const express=require('express');
const router=express.Router();
const mongoose=require('mongoose');
const authMiddleware=require('../middleware/adminauth');

const mainleftbuttons=require("../models/mainleftbuttonsSchema"); 
const subbuttons=require('../models/subbuttonSchema');

const main=require('../models/main/mainbutton');
const main_subbutton=require('../models/main/subbuttons');
const chatButton=require('../models/main/chatbutton');


// MULTER SETUP
const multer=require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'assets/ftpimages/')
    },
    filename: function (req, file, cb) { 
        cb(null, req.body.title + '.png') ;   
    }
});
var button_image_storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'assets/buttonimages/')
    },
    filename: function (req, file, cb) { 
        req.body.url=Date.now().toString().replace(' ','')+file.originalname;
        // console.log(file);
        cb(null, req.body.url);   
    }
  })

var button_video_storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'assets/buttonvideos/')
    },
    filename: function (req, file, cb) { 
        req.body.filename=Date.now().toString().replace(' ','')+file.originalname;
        // console.log(file);
        cb(null, req.body.filename);
    }
})

var button_article_storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'assets/buttonarticleimages/')
    },
    filename: function (req, file, cb) { 
        req.body.filename=Date.now().toString().replace(' ','')+file.originalname;
        // console.log(file);
        cb(null, req.body.filename);
    }
})

var chat_button_storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'assets/chatbuttoncontent/')
    },
    filename: function (req, file, cb) { 
        let images=['image/jpeg','image/png','image/jpg']; 
        
        if(images.find(img => file.mimetype == img)){
            if(file.fieldname == 'image'){
                req.body.imagename=Date.now().toString().replace(' ','')+file.originalname;
                cb(null, req.body.imagename);
            }else{
                req.body.imagename2=Date.now().toString().replace(' ','')+file.originalname;
                cb(null, req.body.imagename2);
            }
           
        }else{
            if(file.fieldname == 'videofile'){
                req.body.filename=Date.now().toString().replace(' ','')+file.originalname;
                cb(null, req.body.filename);
            }
            else{
                req.body.filename2=Date.now().toString().replace(' ','')+file.originalname;
                cb(null, req.body.filename2);
            }
        }
       
    }
})

const upload=multer({storage:storage});
const button_images=multer({storage:button_image_storage});
const button_videos=multer({storage:button_video_storage});
const button_articles=multer({storage:button_article_storage});
const chat_button=multer({storage:chat_button_storage});

//MULTER ENDS HERE


// MAIN LEFT BUTTON API FOR FRONTEND
router.get('/',function(req,res){
    const btns=main.find({publish:1})
    .then(btns=>{
        // res.status(200).json(btns);

        let temp=[];
        return new Promise((resolve,reject)=>{
            btns.forEach((btn,index)=>{   
                main_subbutton.find({'parent_id':btn._id,publish:1}).then(res=>{
                    // res=processButton(res);
                    btn.data=res;
                    temp.push(btn);
                    
                    if(index == btns.length-1){
                        resolve(temp);
                    }

                }).catch(error=>{
                    res.status(200).json('no content for inner');
                 }); 
            });

        }).then((new_data)=>{
            res.status(200).json(new_data); 
        }); 

    }).catch(error=>{
        console.log(error);
        res.json(error);
    });
}); 

let processButton=(data)=>{
    data=data.map((button)=>{
        button.videos=button.filter(vid=> vid.publish == 1);

        return button;
    });

    return data;
}

// MAIN LEFT BUTTON API FOR FRONTEND
router.get('/mainbutton',function(req,res){
    const btns=main.find()
    .then(btns=>{
        // res.status(200).json(btns);

        let temp=[];
        return new Promise((resolve,reject)=>{
            btns.forEach((btn,index)=>{   
                main_subbutton.find({'parent_id':btn._id}).then(res=>{
                    // btns[index].data=res; 
                    // console.log(btn._id);
                    btn.data=res;
                    temp.push(btn);
                    // new_file.push(btn); 
                   
                    if(index == btns.length-1){
                        resolve(temp);
                    }

                }).catch(error=>{
                    res.status(200).json('no content for inner');
                 }); 
            });

        }).then((new_data)=>{
            res.status(200).json(new_data); 
        }); 

    }).catch(error=>{
        console.log(error);
        res.json(error);
    });
}); 

// SUB LEFT BUTTON API FOR FRONTEND
router.get('/view-sub-button',function(req,res){
    const btns=main_subbutton.find()
    .then(btns=>{
        return res.status(200).json(btns);  
    }).catch(error=>{
        console.log(error);
        return res.status(400).json(error);
    });
});
// SUB LEFT BUTTON VIDEOS API FOR FRONTEND
router.get('/view-sub-button-videos',function(req,res){
    const btns=main_subbutton.find({},'videos')
    .then(btns=>{
        let temp=[];
        btns.forEach(element => {
            temp.push(...element.videos);
        });

        return res.status(200).json(temp);  
    }).catch(error=>{
        console.log(error);
        return res.status(400).json(error);
    });
}); 
 
// SUB LEFT BUTTON GET VIDEO API FOR FRONTEND
router.get('/get-sub-button/:id',function(req,res){
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then(btn=>{ 
            if(!btn){
                main_subbutton.findOne({_id:req.params.id})
                .then(btn=>{ 
                    resolve(btn);
                }).catch(error=>{
                    console.log(error);
                    return res.status(400).json(error);
                });
            }else{
                resolve(btn);
            }
             
        }).catch(error=>{
            console.log(error);
            return res.status(400).json(error);
        });        
    }).then((btn)=>{ 
        return res.status(200).json(btn);   
    })
});

// SUB LEFT BUTTON API FOR BACKEND
router.post('/searchbutton',function(req,res){
    // console.log(req.body);
    let inp=new RegExp(req.body.input,'i');
    const btns=main_subbutton.find({title:inp})
    .then(btns=>{
        return res.status(200).json(btns);  
    }).catch(error=>{
        // console.log(error);
        return res.status(400).json(error);
    });
}); 

// GET THE CHILDREN OF BUTTONS
router.get('/get-button/:id',function(req,res){  
     const btns=main_subbutton.find({'parent_id':req.params.id}).
     then(data=>{
        // res.status(200).json(data);
        // let payload={btns_content:data,data:{}};

        return new Promise((resolve,reject)=>{
            data.forEach((btn,index)=>{
                main_subbutton.find({'parent_id':btn._id}).then(res=>{
                    
                    // console.log(btn._id);
                    // if(btn._id == 'ho5c5b14acc70c592fac68049a'){
                    //     console.log(res);
                    // }
                    btn.data=[];
                    btn.data=res;
                    console.log(res[0]);
                    // new_file.push(btn); 
                   
                    if(index == data.length-1){
                        resolve(data);
                    }

                }).catch(error=>{
                    res.status(200).json('no content for inner');
                 }); 
            });

        }).then((new_data)=>{
            res.status(200).json(new_data); 
        }); 
        
        
     }).catch(error=>{
        res.status(200).json(error);
     });

});

// PUBLISH AND UNPUBLISH BUTTONS HERE
router.post('/publish-main-button/:id',authMiddleware,function(req,res){
    let response;
    return new Promise((resolve,reject)=>{
        main.findOne({'_id':req.params.id})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({'_id':req.params.id})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(401).json({'code':-1,'message':'An error occured'});
                }); 
            }else{
                resolve(data);
            }
          
        })
        .catch((error)=>{
            return res.status(401).json({'code':-1,'message':'An error occured'});
        });
    })
    .then((data)=>{
        if(data.publish == 1){
            data.publish=0;
        }
        else{
            data.publish=1;
        }
        data.save(); 
        return res.status(200).json({code:1,'message':'Update successfully'});
    });
    
});

// CREATE NEW MAIN BUTTONS 
router.post('/create-main-buttons',authMiddleware,function(req,res){
   
    let payload=req.body; 

    
    let check=main.findOne({'title':payload.title})
    .then((btn)=>{ 
        if(btn != null){
            // returnResponse(res,400,-1,'Button name already exist',payload);
            return res.status(400).json({code:-1,'message':'Button name already exist'});

        }

        let button=new main(); 
        button.title=req.body.title;
        button.publish=req.body.publish;  
        button.icon=req.body.title+".png";
        button.save(); 
        
        returnResponse(res,200,1, payload.title+' button was created successfully.',{button:button});

    })
    .catch((error)=>{
        returnResponse(res,400,-1,'unknown error while creating main button'); 
    });
    
 

    // return res.status(200).json(payload);
});

router.post('/create-main-buttons-icon',upload.single('icon'),function(req,res){ 
    console.log(req.file);
    let check=main.findOne({'title':req.body.title})
    .then((button)=>{

        if(button == null){ 
            return res.status(400).json({code:-1,'message':"Button doesn't exist"});
        } 

        button.icon=req.file.filename;
        button.save(); 

        returnResponse(res,200,1, req.body.title+' button was updated successfully.',{button:button});

    }) 
    .catch((error)=>{
        returnResponse(res,400,-1,'unknown error while updating main button'); 
    }); 
});

// CREATE NEW SUB BUTTONS 
router.post('/create-sub-buttons/:token',upload.single('icon'),authMiddleware,function(req,res){
   
    let payload=req.body; 
    // 3098367755
       
        let button=new main_subbutton();
        console.log(req.body);
        button.title=req.body.title;
        button.parent_id=req.body.id;
        button.publish=0;  
        button.icon=req.body.title+".png";
        // button.icon=req.file.filename; 
        button.save(); 
        
        returnResponse(res,200,1, payload.title+' button was created successfully.',{button:button});
    

    // return res.status(200).json(payload);
});

// DELETE MAIN BUTTONS 
router.get("/delete-main-button/:token/:id",authMiddleware,function(req,res){
    let id=req.params.id;
    console.log(id);
    
    main.findOneAndDelete({_id:id}).then((data)=>{
        return res.status(200).json({code:1,'message':data.title+' button was removed successfully',data});
    }).catch((error)=>{
        main_subbutton.findOneAndDelete({_id:id}).then((data)=>{
            return res.status(200).json({code:1,'message':data.title+' button was removed successfully',data});
        }).catch((error)=>{
            return res.status(200).json({code:-1,'message':'unable to remove sub button due to an unknown error',error});
        });
        // return res.status(200).json({code:-1,'message':'unable to remove button due to an unknown error',error});
    });
     
 
});

//GET BUTTONS DETAILS
router.get('/view-button/:id',function(req,res){
    let button=main.findOne({_id:req.params.id}).then((data)=>{

        if(!data){ 
            main_subbutton.findOne({_id:req.params.id})
            .then((dt)=>{
                res.status(200).json(dt);        
            }).catch(error=>{
                res.status(400).json({code:-1,'message':"button was not found"});
            });
        }else{
            res.status(200).json(data);        
        }
        
    }).catch(error=>{
        res.status(400).json({code:-1,'message':"button was not found"});
    });
    
});
router.get('/view-button-subbutton/:id',function(req,res){
    let button=main_subbutton.find({parent_id:req.params.id}).then((data)=>{
        res.status(200).json(data);    
    }).catch(error=>{
        res.status(400).json({code:-1,'message':"button was not found"});
    });
    
});

// UPDATE NEW MAIN BUTTONS 
router.post('/update-main-buttons',authMiddleware,function(req,res){
   
    let payload=req.body; 
    let response;
    
    return new Promise((resolve,reject)=>{
        main.findOne({'_id':payload.id})
        .then((button)=>{
            if(!button){
                main_subbutton.findOne({'_id':payload.id})
                .then((button)=>{
                    resolve(button);
                })
                .catch((error)=>{
                    returnResponse(res,400,-1,'unknown error while updating main button'); 
                });
            }else{
                resolve(button); 
            }
            
        })
        .catch((error)=>{
            returnResponse(res,400,-1,'unknown error while updating main button'); 
        });
    }).then(button=>{

        if(button == null){ 
            return res.status(400).json({code:-1,'message':"Button doesn't exist"});
        } 

        button.title=req.body.title;
        button.publish=req.body.publish;
        button.save(); 

        returnResponse(res,200,1, payload.title+' button was updated successfully.',{button:button});

    });
    
    
 

    // return res.status(200).json(payload);
});

// ADD IMAGE
router.post('/add-buttom-image/:token/:id',authMiddleware,button_images.single('image'),function(req,res){
    
    let response;
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then((data)=>{

            if(!data){
                main_subbutton.findOne({_id:req.params.id})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{

                }); 
            }
            else{
                resolve(data);
            }
        })
        .catch((error)=>{
            
        });
    }).then(data=>{
            if(!data){
                return res.status(400).json({code:-1,message:"button not found"});
            }

            let payload=req.body;
            console.log(payload);
            let images=data.images;
            let random="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            let a=random[Math.ceil(Math.random()*24)];
            let b=random[Math.ceil(Math.random()*24)];
            let c=random[Math.ceil(Math.random()*24)];
            let id=a+''+Math.ceil(Math.random()*999)+''+b+c;

            let image={
                id:id,    
                title:payload.title,
                credit:payload.credit,
                desc:payload.desc,
                publish: payload.publish == 'true' ? 1:0,
                filename:req.file ? payload.url : '',
                url:req.file ? '' : payload.imageurl,
                date:payload.date,
                views:[],
                rates:[]
            };

            images.push(image);
            data.title=data.title;
            data.images=images;
            data.save(); 
    
            return res.status(200).json({code:1,data,'message':"Image was added successfully"});
        })
    

    // return req.status(200).json(req.body);
});

// SET PUBLISH FOR BUTTON IMAGES
router.get('/publish-buttom-image/:token/:button/:image',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{
           if(!data){
                main_subbutton.findOne({_id:req.params.button})
                .then((data)=>{ 
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(400).json({code:-1,message:"unknown error occured"});
                });
           }
           else{
               resolve(data);
           }
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:"unknown error occured"});
        });
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }
        let button_images=[];
        button_images=data.images;
        let images=button_images.map((img)=> { 
            
            if( req.params.image == img.id ){
                img.publish=img.publish == 1 ? 0 : 1;
            }
            return img;
        });
        
        // console.log(images);
        data.images=[];
        data.images=images;
        data.save();
        return res.status(200).json({code:1,data,'message':"Image was updated successfully"});
    });
   
 
    // return req.status(200).json(req.body);
});

// DELETE BUTTON IMAGES
router.get('/delete-buttom-image/:token/:button/:image',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.button})
                    .then((data)=>{
                        resolve(data);
                    })
                    .catch((error)=>{
                        return res.status(400).json({code:-1,message:"unknown error occured"});
                    });
            
            }else{
                resolve(data);    
            }
            
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:"unknown error occured"});
        });
    }).then((data)=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }
        let button_images=[];
        button_images=data.images;
        let images=button_images.filter((img)=> { 
            return req.params.image != img.id ;
        });
        
        // console.log(images);
        data.images=[];
        data.images=images;
        data.save();
        return res.status(200).json({code:1,data,'message':"Image was removed successfully"});
    });
    
});

// UPDATE IMAGE
router.post('/update-buttom-image/:token/:id',authMiddleware,button_images.single('image'),function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then((data)=>{

           if(!data){
                    main_subbutton.findOne({_id:req.params.id})
                    .then((data)=>{
                        resolve(data);
                    })
                    .catch((error)=>{
                
                    }); 
           }
           else{
               resolve(data);
           }
        })
        .catch((error)=>{
    
        });
    }).then(data=>{
        
            if(!data){
                return res.status(400).json({code:-1,message:"button not found"});
            }
    
            let payload=req.body;
            console.log(req.file);
            let imageindex=0;
            let updateimage=data.images.find((img,index)=>{
                imageindex=index;
                return img.id == payload.id;
            });
    
            let images=data.images;
            
            
            updateimage.title=payload.title,
            updateimage.credit=payload.credit,
            updateimage.desc=payload.desc,
            updateimage.date=payload.date,
            updateimage.filename=req.file ? payload.url : '',
            updateimage.url=req.file ? '' : payload.imageurl,
    
    
            images[imageindex]=updateimage;
            data.images=[];
            data.images=images;
            data.save(); 
             
            return res.status(200).json({code:1,data,image:updateimage,'message':"Image was updated successfully"});
    });
   

    // return req.status(200).json(req.body);
}); 


//CREATE CHAT BUTTON
router.post("/create-chat-button",chat_button.fields([{name:'videofile'},{name:'image'},{name:'videofile2'},{name:'image2'}]),function(req,res){
    
    console.log(req.body);
    // return res.status(200).json({});
    let chatbutton=new chatButton();
    chatbutton.title=req.body.title;
    chatbutton.code=!req.body.filename ? req.body.videourl : '';
    chatbutton.imageurl=!req.body.imagename ? req.body.imageurl : '';

    chatbutton.video=req.body.filename ? req.body.filename: '';
    chatbutton.image=req.body.imagename !='' ? req.body.imagename : '';
    chatbutton.publish=0;

    let mobile={
        code:!req.body.filename2 ? req.body.videourl2 : '',
        imageurl:imageurl=!req.body.imagename2 ? req.body.imageurl2 : '',
        video:video=req.body.filename2 ? req.body.filename2: '',
        image:image=req.body.imagename2 !='' ? req.body.imagename2 : ''
    };


    chatbutton.mobile=mobile;

    chatbutton.save();

    return res.status(200).json(chatbutton);

});

// GET CHAT BUTTONS
router.get('/get-chat-buttons',function(req,res){
    chatButton.find()
    .then(data=>{
        return res.status(200).json(data);
    })
    .catch(error=>{
        return res.status(200).json(error);
    })
 
});

// GET CHAT BUTTON
router.get('/get-chat-button/:id',function(req,res){
    chatButton.findOne({_id:req.params.id})
    .then(data=>{
        return res.status(200).json(data);
    })
    .catch(error=>{
        return res.status(200).json(error);
    })
 
});

//DELETE CHAT BUTTON
router.delete('/delete-chat-button/:id',function(req,res){
    chatButton.findOne({_id:req.params.id}).then(data=>{
        data.remove();
        return res.status(200).json(data); 
    }).catch(error=>{
        return res.status(400).json(error); 
    });


});

//PUBLISH CHAT BUTTON
router.post('/publish-chat-button/:id',function(req,res){
    chatButton.find().then(btns=>{
        let published=btns.filter(bt=> bt.publish == 1);
        if(published.length >= 6){
            return res.status(400).json(error); 
        }

        let btn=btns.find(b=> b._id == req.params.id);
        btn.publish=btn.publish == 1 ? 0 : 1;
        btn.save();

        return res.status(200).json(btn); 
    }).catch(error=>{
        return res.status(400).json(error); 
    });


});

//CREATE CHAT BUTTON
router.post("/update-chat-button/:id",chat_button.fields([{name:'videofile'},{name:'image'},{name:'videofile2'},{name:'image2'}]),function(req,res){
    
    chatButton.findOne({_id:req.params.id}).then((chatbutton)=>{
        chatbutton.title=req.body.title;
        chatbutton.code=req.body.filename || req.body.imagename ? '' : req.body.videourl;
        chatbutton.imageurl=req.body.filename || req.body.imagename || req.body.videourl ? '' : req.body.imageurl ;

        chatbutton.video=req.body.filename ? req.body.filename : req.body.imagename  ? '' : req.body.videourl || req.body.imageurl ? '' : chatbutton.video;
        chatbutton.image=req.body.imagename ? req.body.imagename : req.body.filename  ? '' : req.body.videourl || req.body.imageurl ? '' : chatbutton.image;
        
        let mobile={
            code:req.body.filename2 || req.body.imagename2 ? '' : req.body.videourl2,
            imageurl:req.body.filename2 || req.body.imagename2  || req.body.videourl2 ? '' : req.body.imageurl2,
            video:req.body.filename2 || req.body.imagename2 ? req.body.filename2 : chatbutton.mobile.video2,
            image:req.body.imagename2 || req.body.filename2  ? req.body.imagename2 : chatbutton.mobile.image2
        };

        chatbutton.mobile=mobile; 
        chatbutton.save();
 
        return res.status(200).json(chatbutton);
    }).catch((error)=>{
        return res.status(400).json({});
    }); 

});

//GET CHAT BUTTONS
router.get("/get-frontend-chat-buttons",function(req,res){

    
        chatButton.find({publish:1}).then(chatButtons=>{
            return new Promise((resolve,reject)=>{
                let data=[];

                let length=chatButtons.length - 1;
                    // console.log(chatButtons.length+'chtbutton'); 
                chatButtons.forEach((btn,index)=>{ 
                    main_subbutton.find({parent_id:btn._id}).then(subbuttons=>{
                        btn.data=[];
                        // btn.data=subbuttons; 
                        // data.push(btn);
                        let data2=[];

                        
                        
                        subbuttons.forEach((element , index2) => {
                            
                            main_subbutton.find({parent_id:element._id}).then(subsubbuttons=>{
                                element.data=[];
                                element.data=subsubbuttons; 
                                
                                data2.push(element);
                                
                                if(index2 == subbuttons.length -1 ){
                                    btn.data=data2;
                                    data.push(btn);
                                }

                                if(data.length == chatButtons.length ){ 
                                    resolve(data); 
                                }

                                
                            });
                        });
                            
                        if(subbuttons.length == 0){
                            btn.data=subbuttons; 
                            data.push(btn);
                        }
                        
                         

                        if((index == length ) && subbuttons.length == 0 && data.length == chatButtons.length){
                            resolve(data);
                        }
     

                        
                    }).catch(error=>{
                        return res.status(400).json(error);
                    }); 
                })
            }).then(data=>{
                return res.status(200).json(data);
            });
        }).catch(error=>{
            return res.status(400).json(error);
        });
   

});
  
//   ================================= VIDEO API'S ========================= //
// CREATE VIDEO
router.post('/add-button-video/:token/:id' , authMiddleware , button_videos.single('video'),function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
            .then((data)=>{
                
                if(!data){
                    main_subbutton.findOne({_id:req.params.id})
                    .then((data)=>{
                        resolve(data)
                    })
                    .catch((error)=>{

                    });
                
                }
                else{
                    resolve(data)
                }
            })
            .catch((error)=>{

            });
        
    })
    .then(data=>{
         if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let payload=req.body;
        console.log(req.file);
        let videos=data.videos;
        let random="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let a=random[Math.ceil(Math.random()*24)];
        let b=random[Math.ceil(Math.random()*24)];
        let c=random[Math.ceil(Math.random()*24)];
        let id=a+''+Math.ceil(Math.random()*999)+''+b+c;

        let video={
            id:id,    
            title:payload.title,
            code:payload.code,
            transcript:payload.transcript,
            description:payload.description,
            url:payload.url,
            publish: payload.publish == 'true' ? 1:0, 
            filename:req.file ? payload.filename:'',
            date:payload.date,
            views:[],
            rates:[]
        };

        videos.push(video);
        data.title=data.title;
        data.videos=[]
        data.videos=videos;
        data.save(); 
 
        return res.status(200).json({code:1,data,'message':"Video was added successfully"});
    });
    
    // return req.status(200).json(req.body);
});

// DELETE BUTTON IMAGES
router.get('/delete-button-video/:token/:button/:video',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{ 
           if(!data){
                main_subbutton.findOne({_id:req.params.button})
                .then((data)=>{ 
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(400).json({code:-1,message:"unknown error occured"});
                });
           }else{
               resolve(data);
           }
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:"unknown error occured"});
        });
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }
        let button_videos=[];
        button_videos=data.videos;
        let videos=button_videos.filter((vid)=> { 
            return req.params.video != vid.id ;
        });
        
        // console.log(videos);
        data.videos=[];
        data.videos=videos;
        data.save();
        return res.status(200).json({code:1,data,'message':"Video was removed successfully"});
    });
    // return req.status(200).json(req.body);
});
const returnResponse=(res,status,code,message,data={})=>{
    return res.status(status).json({code:code,'message':message,data:data});
}

// SET PUBLISH FOR BUTTON VIDEOS
router.get('/publish-buttom-video/:token/:button/:video',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.button})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(400).json({code:-1,message:"unknown error occured"});
                });
            }
            else{
                resolve(data);
            }
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:"unknown error occured"});
        });
    })
    .then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let button_videos=[];
        button_videos=data.videos;
        
        let videos=button_videos.map((vid)=> { 
            if( req.params.video == vid.id ){
                vid.publish=vid.publish == 1 ? 0 : 1;
            }
            return vid;
        });
        
        
        data.videos=[];
        data.videos=videos;
        data.save();
        return res.status(200).json({code:1,data,'message':"Video was updated successfully"});
    });
  
    // return req.status(200).json(req.body);
});

// UPDATE VIDEO
router.post('/update-button-video/:token/:id',authMiddleware,button_videos.single('video'),function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.id})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
            
                }); 
            }
            else{
                resolve(data);
            }
        })
        .catch((error)=>{
    
        }); 
    })
    .then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let payload=req.body;
        console.log(req.file);
        let videoindex=0;
        let updatevideo=data.videos.find((vid,index)=>{
            videoindex=index;
            return vid.id == payload.id;
        });

        let videos=data.videos; 
        updatevideo.title=payload.title;
        updatevideo.filename=req.file ? payload.filename : (payload.code !='' || payload.url !='' )? '':updatevideo.filename;
        
        updatevideo.url=(req.file || payload.code !='' ) ? '':payload.url ;
        updatevideo.code=(req.file) ? '':payload.code;
        updatevideo.transcript=payload.transcript;
        updatevideo.description=payload.description;
        updatevideo.date=payload.date;
        

        videos[videoindex]=updatevideo;
        data.videos=[]; 
        data.videos=videos;
        data.save();  
        
        return res.status(200).json({code:1,data,video:updatevideo,'message':"Video was updated successfully"});
    });

    // return req.status(200).json(req.body);
});



//   ================================ DOCUMENT API'S ========================= //
// CREATE DOCUMENT
router.post('/add-button-document/:token/:id' , function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.id})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
        
                });
            }
            else{
                resolve(data);
            }
        })
        .catch((error)=>{

        });
    })
    .then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let payload=req.body;
        console.log(req.body);
        let docs=data.documents;
        let random="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let a=random[Math.ceil(Math.random()*24)];
        let b=random[Math.ceil(Math.random()*24)];
        let c=random[Math.ceil(Math.random()*24)];
        let id=a+''+Math.ceil(Math.random()*999)+''+b+c;

        let doc={
            id:id,
            title:payload.title, 
            author:payload.author,
            url:payload.url,
            publish: payload.publish == true ? 1:0,  
            date:payload.date
        };

        docs.push(doc);
        data.title=data.title;
        data.documents=[];
        data.documents=docs;
        data.save(); 

        return res.status(200).json({code:1,data,'message':"Documents was added successfully"});
    });
    // return req.status(200).json(req.body);
});

// DELETE BUTTON DOCUMENTS
router.get('/delete-button-documents/:token/:button/:image',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.button})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(400).json({code:-1,message:"unknown error occured"});
                });
            }
            else{
                resolve(data);
            }
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:"unknown error occured"});
        });
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }
        let button_documents=[];
        button_documents=data.documents;
        let documents=button_documents.filter((img)=> { 
            return req.params.image != img.id ;
        });
        
        // console.log(documents);
        data.documents=[];
        data.documents=documents;
        data.save();
        return res.status(200).json({code:1,data,'message':"Document was removed successfully"});
    });
 
    // return req.status(200).json(req.body);
});

// SET PUBLISH FOR BUTTON DOCUMENT
router.get('/publish-button-documents/:token/:button/:video',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.button})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(400).json({code:-1,message:"unknown error occured"});
                });
            }else{
                resolve(data);
            }
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:"unknown error occured"});
        });
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let button_documents=[];
        button_documents=data.documents;
        
        let documents=button_documents.map((dc)=> { 
            if( req.params.video == dc.id ){
                dc.publish=dc.publish == 1 ? 0 : 1;
            }
            return dc;
        });
        
        
        data.documents=[];
        data.documents=documents;
        data.save();
        return res.status(200).json({code:1,data,'message':"Documents was updated successfully"});
    });
  
    // return req.status(200).json(req.body);
});

// UPDATE DOCUMENTS
router.post('/update-button-document/:token/:id',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.id})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(405).json({code:-1 ,error});
                }); 
            }else{
                resolve(data);
            }
        })
        .catch((error)=>{
            return res.status(405).json({code:-1 ,error});
        }); 
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let payload=req.body; 
        // console.log(payload);
        let docindex=0;

        let updatedoc=data.documents.find((dc,index)=>{
            docindex=index;
            return dc.id == payload.id;
        });

        let documents=data.documents; 
        updatedoc.title=payload.title;  
        updatedoc.url=payload.url;
        updatedoc.author=payload.author;
        updatedoc.date=payload.date;
        
 
        documents[docindex]=updatedoc;
        data.documents=[]; 
        data.documents=documents;
        data.save();  
         
        return res.status(200).json({code:1,data,doc:updatedoc,'message':"Document was updated successfully"});
    });
    

    // return req.status(200).json(req.body);
});



//   ================================= ARTICLE API'S ========================= //
// CREATE ARTICLE
router.post('/add-button-article/:token/:id' , authMiddleware , button_articles.single('image'),function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then((data)=>{
           if(!data){
            main_subbutton.findOne({_id:req.params.id})
            .then((data)=>{ 
                resolve(data); 
            })
            .catch((error)=>{
                return res.status(404).json({code:-1,data,'message':error});
            });
           }else{
               resolve(data);
           }
        })
        .catch((error)=>{
            return res.status(404).json({code:-1,data,'message':error});
        });
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let payload=req.body;
        console.log(req.file);
        let articles=data.articles;
        let random="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let a=random[Math.ceil(Math.random()*24)];
        let b=random[Math.ceil(Math.random()*24)];
        let c=random[Math.ceil(Math.random()*24)];
        let id=a+''+Math.ceil(Math.random()*999)+''+b+c;

        let article={
            id:id,    
            title:payload.title,
            url:payload.url,
            author:payload.author,
            date:payload.date,
            paragraph:payload.paragraph,
            description:payload.description,
            publish: payload.publish == 'true' ? 1:0, 
            filename:req.file ? payload.filename:'', 
        };

        articles.push(article);
        data.title=data.title;
        data.article=[]
        data.article=articles;
        data.save(); 

        return res.status(200).json({code:1,data,'message':"Article was added successfully"});
    });
 
    // return req.status(200).json(req.body);
});

// DELETE BUTTON ARTICLES
router.get('/delete-button-article/:token/:button/:article',authMiddleware,function(req,res){
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.button})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(400).json({code:-1,message:error});
                });
            }else{
                resolve(data);
            }
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:error});
        });
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }
        let button_articles=[];
        button_articles=data.articles;
        let articles=button_articles.filter((art)=> { 
            return req.params.article != art.id ;
        });
        
        // console.log(articles);
        data.articles=[];
        data.articles=articles;
        data.save();
        return res.status(200).json({code:1,data,'message':"Article was removed successfully"});
    });
    // return req.status(200).json(req.body);
});

// UPDATE ARTICLES
router.post('/update-button-article/:token/:id',authMiddleware,button_articles.single('image'),function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.id})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.id})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(405).json({code:-1 ,error});
                });
            }
            else{
                resolve(data);
            }
        })
        .catch((error)=>{
            return res.status(405).json({code:-1 ,error});
        }); 
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let payload=req.body; 

        console.log(req.file);
        console.log(req.body); 

        let docindex=0;

        let updatearticle=data.articles.find((art,index)=>{
            docindex=index;
            return art.id == payload.id;
        });

        let articles=data.articles; 

        updatearticle.title=payload.title;  
        updatearticle.url=payload.url; 
        updatearticle.author=payload.author;
        updatearticle.date=payload.date;
        updatearticle.paragraph=payload.paragraph;
        updatearticle.description=payload.description;
        updatearticle.filename=req.file ? payload.filename:updatearticle.filename, 
        
        articles[docindex]=updatearticle;
        data.articles=[]; 
        data.articles=articles;
        data.save();  
         
        return res.status(200).json({code:1,data,article:updatearticle,'message':"Article was updated successfully"});
    });

    // return req.status(200).json(req.body);
});

// SET PUBLISH FOR BUTTON ARTICLE
router.get('/publish-button-articles/:token/:button/:article',authMiddleware,function(req,res){
    
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then((data)=>{
            if(!data){
                main_subbutton.findOne({_id:req.params.button})
                .then((data)=>{
                    resolve(data);
                })
                .catch((error)=>{
                    return res.status(400).json({code:-1,message:"unknown error occured"});
                });
            }else{
                resolve(data);
            }
        })
        .catch((error)=>{
            return res.status(400).json({code:-1,message:"unknown error occured"});
        });
    }).then(data=>{
        if(!data){
            return res.status(400).json({code:-1,message:"button not found"});
        }

        let button_articles=[];
        button_articles=data.articles;
        
        let articles=button_articles.map((art)=> {
            if( req.params.article == art.id ){
                art.publish=art.publish == 1 ? 0 : 1;
            }
            return art;
        });
        
        
        data.articles=[];
        data.articles=articles;
        data.save(); 
        return res.status(200).json({code:1,data,'message':"Article was updated successfully"});
    });
  
    // return req.status(200).json(req.body);
});

//COMMENTS ON VIDEOS
router.post("/video-comment/:button/:video",function(req,res){
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then(btn=>{ 
            if(!btn){
                main_subbutton.findOne({_id:req.params.button})
                .then(btn=>{ 
                    resolve(btn);
                }).catch(error=>{
                    console.log(error);
                    return res.status(400).json(error);
                });
            }else{
                resolve(btn);
            }
            
        }).catch(error=>{
            console.log(error);
            return res.status(400).json(error);
        });        
    }).then((btn)=>{ 
        console.log(btn.title);
        let video=btn.videos.find(vid=>vid.id == req.params.video);
        let comments=video.comments;
        if(!comments){
            console.log(comments);
            comments=[];
        }
        let comment={
            user:req.body.user,
            text:req.body.text,
            date:new Date()
        }
        comments.push(comment);
        video.comments=[];
        video.comments=comments;

        let videos=btn.videos;
        videos.map(vid=>{
            if(vid.id ==req.params.video){
                vid.comments=comments;
            } 
            return vid;
        });
        btn.videos=[];
        
        btn.videos=videos; 
        btn.save();

        return res.status(200).json(btn);   
    })
});

//RATE ON VIDEOS
router.post("/rate-video/:button/:video",function(req,res){
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then(btn=>{ 
            if(!btn){
                main_subbutton.findOne({_id:req.params.button})
                .then(btn=>{ 
                    resolve(btn);
                }).catch(error=>{ 
                    return res.status(400).json(error);
                });
            }else{
                resolve(btn);
            }
            
        }).catch(error=>{
            console.log(error);
            return res.status(400).json(error);
        });        
    }).then((btn)=>{ 
        
        let video=btn.videos.find(vid=>vid.id == req.params.video);
        let rates=video.rates;
        if(!rates){
            console.log("rates no dey");
            rates=[];
        }
        let rate_exist=rates.find((rt=> rt.user == req.body.user));
        
        let rate={
            user:req.body.user,
            rate:req.body.rate,
            date:new Date()
        }
        if(rate_exist){ 
            rate=rate_exist;
            rate.rate=req.body.rate;
            rate.date=new Date();

            rates=rates.map(rt=>{
                if(rt.user == rate.user){
                    rt=rate;
                }

                return rt;
            });
        }else{
            rates.push(rate);
        }
        
        video.rates=[];
        video.rates=rates;

        let videos=btn.videos;
        videos.map(vid=>{
            if(vid.id ==req.params.video){
                vid.rates=rates;
            } 
            return vid;
        });
        btn.videos=[];
        
        btn.videos=videos; 
        btn.save();

        return res.status(200).json(video);   
    })
});
 

//VIEW ON VIDEOS
router.get("/view-video/:button/:video",function(req,res){
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then(btn=>{ 
            if(!btn){
                main_subbutton.findOne({_id:req.params.button})
                .then(btn=>{ 
                    resolve(btn);
                }).catch(error=>{ 
                    return res.status(400).json(error);
                });
            }else{
                resolve(btn);
            }
            
        }).catch(error=>{
            console.log(error);
            return res.status(400).json(error);
        });        
    }).then((btn)=>{ 
        
        let video=btn.videos.find(vid=>vid.id == req.params.video);
        let views=video.views;
        if(!views){ 
            views=0;
        }
        

        views=parseInt(views)+1; 
        video.views=views;
        let videos=btn.videos;
        videos.map(vid=>{
            if(vid.id ==req.params.video){
                vid.views=views;
            } 
            return vid;
        });
        btn.videos=[];
        
        btn.videos=videos; 
        btn.save();

        return res.status(200).json(video);   
    })
});
 

//RATE ON IMAGES
router.post("/rate-image/:button/:image",function(req,res){
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then(btn=>{ 
            if(!btn){
                main_subbutton.findOne({_id:req.params.button})
                .then(btn=>{ 
                    resolve(btn);
                }).catch(error=>{ 
                    return res.status(400).json(error);
                });
            }else{
                resolve(btn);
            }
            
        }).catch(error=>{
            console.log(error);
            return res.status(400).json(error);
        });        
    }).then((btn)=>{ 
        
        let image=btn.images.find(img=>img.id == req.params.image);
        let rates=image.rates;
        if(!rates){ 
            rates=[];
        }
        let rate_exist=rates.find((rt=> rt.user == req.body.user));
        
        let rate={
            user:req.body.user,
            rate:req.body.rate,
            date:new Date()
        }
        if(rate_exist){ 
            rate=rate_exist;
            rate.rate=req.body.rate;
            rate.date=new Date();

            rates=rates.map(rt=>{
                if(rt.user == rate.user){
                    rt=rate;
                }

                return rt;
            });
        }else{
            rates.push(rate);
        }
        
        image.rates=[];
        image.rates=rates;

        let images=btn.images;
        images.map(img=>{
            if(img.id ==req.params.image){
                img.rates=rates;
            } 
            return img;
        });
        btn.images=[];
        
        btn.images=images; 
        btn.save();

        return res.status(200).json(image);   
    })
});


//RATE ON ARTICLE
router.post("/rate-article/:button/:article",function(req,res){
    return new Promise((resolve,reject)=>{
        main.findOne({_id:req.params.button})
        .then(btn=>{ 
            if(!btn){
                main_subbutton.findOne({_id:req.params.button})
                .then(btn=>{ 
                    resolve(btn);
                }).catch(error=>{ 
                    return res.status(400).json(error);
                });
            }else{
                resolve(btn);
            }
            
        }).catch(error=>{
            console.log(error);
            return res.status(400).json(error);
        });        
    }).then((btn)=>{ 
        
        let article=btn.articles.find(vid=>vid.id == req.params.article);
        let rates=article.rates;
        if(!rates){
            // console.log("rates no dey");
            rates=[];
        }
        let rate_exist=rates.find((rt=> rt.user == req.body.user));
        
        let rate={
            user:req.body.user,
            rate:req.body.rate,
            date:new Date()
        }
        if(rate_exist){ 
            rate=rate_exist;
            rate.rate=req.body.rate;
            rate.date=new Date();

            rates=rates.map(rt=>{
                if(rt.user == rate.user){
                    rt=rate;
                }

                return rt;
            });
        }else{
            rates.push(rate);
        }
        
        article.rates=[];
        article.rates=rates;

        let articles=btn.articles;
        articles.map(vid=>{
            if(vid.id ==req.params.article){
                vid.rates=rates;
            } 
            return vid;
        });
        btn.articles=[];
        
        btn.articles=articles; 
        btn.save();

        return res.status(200).json(article);   
    })
});
 

// router.get('/sub',function(req,res){
//     //  res.status(200).json('data');
//     const subbutton=subbuttons.find().
//     then(data=>{ 
//         return new Promise((resolve,reject)=>{
//             data.forEach((btn,index)=>{
//                 main_subbutton.find({'parent_id':btn._id}).then(res=>{
//                     // btns[index].data=res; 
//                     btn.data=res;
//                     // new_file.push(btn); 
                   
//                     if(index == data.length-1){
//                         resolve(data);
//                     }

//                 }).catch(error=>{
//                     res.status(200).json('no content for inner');
//                  }); 
//             });

//         }).then((new_data)=>{
//             res.status(200).json(data);
//         });
//     })
//     .catch((error)=>{
//        return res.status(200).json(JSON.stringify(error));
//     }); 
   
// });


//CONVERT MAIN BUTTTONS
// router.get('/convertmainbutton',function(req,res){
//     const btns=mainleftbuttons.find()
//     .then(btns=>{ 

//         let temp=[];
//         return new Promise((resolve,reject)=>{
//             btns.forEach((btn,index)=>{   
//                 main_subbutton.findOne({'folderid':btn.folderid}).then(dt=>{
//             //    CONVERTING FORMAL BUTTONS TO NEW ONE 
//                     let mainBtn=new main(); 
//                     mainBtn._id=dt._id;
//                     mainBtn.title=dt.title;
//                     mainBtn.parent_id=dt.parent_id;
//                     mainBtn.folderid=dt.folderid;
//                     mainBtn.icon=dt.icon;
//                     mainBtn.images=[];
//                     mainBtn.videos=dt.videos;
//                     mainBtn.documents=[];
//                     mainBtn.articles=dt.videos;
//                     mainBtn.publish=1;
//                     mainBtn.save();  
                   

//                     dt.delete();

//                     if(index == btns.length-1){
//                         resolve(temp);
//                     }

//                 }).catch(error=>{
//                     console.log(error);
//                     res.status(200).json(error);
//                  }); 
//             });

//         }).then((new_data)=>{
//             res.status(200).json(new_data); 
//         }); 

//     }).catch(error=>{
//         console.log(error);
//         res.json(error); 
//     });
// }); 

//CONVERT SUB BUTTONS
router.get('/convertbuttons',function(req,res){
    const btns=subbuttons.find().
    then(data=>{
       let payload=[]; 
        return new Promise((resolve,reject)=>{
            data.forEach((dt,index)=>{
                    // dt.publish=1;
                    // dt.save();

                    let random="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    let a=random[Math.ceil(Math.random()*24)];
                    let b=random[Math.ceil(Math.random()*24)];
                    let c=random[Math.ceil(Math.random()*24)];
                    let id=a+''+Math.ceil(Math.random()*999)+''+b+c; 

            //    CONVERTING FORMAL VIDOES TO NEW ONE 
                let videos=(dt['Spreadsheets'].length == 0 || dt['Spreadsheets'] == null) ? []:(dt['Spreadsheets'][1] == null || dt['Spreadsheets'][1] == '') ? []:dt['Spreadsheets'][1][1] == null || dt['Spreadsheets'][1][1] == '' ?[]:dt['Spreadsheets'][1][1];
                let new_videos=videos.map(video=>{
                    let vid={};
                    vid.id=id;
                    vid.title=video.Title ? video.Title : video.title ? video.title: '';
                    vid.url="https://www.youtube.com/watch?v="+video.URL;
                    vid.description=video.Description;
                    vid.date=video.Uploaded;
                    vid.filename='';
                    vid.code='';
                    vid.transcript='';
                    vid.publish=1;
                    return vid;
                });

            //    CONVERTING FORMAL ARTICLES TO NEW ONE 
                let articles=(dt['Spreadsheets'].length == 0 || dt['Spreadsheets'] == null) ? []:(dt['Spreadsheets'][0] == null || dt['Spreadsheets'][0] == '') ? []:dt['Spreadsheets'][0][1] == null || dt['Spreadsheets'][0][1] == '' ?[]:dt['Spreadsheets'][0][1];
                let new_article=articles.map(art=>{
                    let vid={};
                    vid.id=id;
                    vid.title=art.Title ? art.Title : art.title ? art.title: '';
                    vid.url=art.URL;
                    vid.author="";
                    vid.date=art.Uploaded;
                    vid.filename=''; 
                    vid.publish=1;
                    vid.paragraph=art.Description;

                    return vid;
                });

                //    CONVERTING FORMAL BUTTONS TO NEW ONE 
               let subbuton=new main_subbutton(); 
               subbuton._id=dt._id;
               subbuton.title=dt.foldername;
               subbuton.parent_id=dt.ParentId;
               subbuton.folderid=dt.folderid;
               subbuton.icon=dt.foldername+'.png';
               subbuton.images=[];
               subbuton.videos=new_videos;
               subbuton.documents=[];
               subbuton.articles=new_article;
               subbuton.publish=1;
               subbuton.save();  

            //    console.log(dt._id);
               if(index  == data.length-1){
                    resolve(payload);
               }
           });
        }).then((payload)=>{
            return res.status(200).json(payload); 
        }); 

    }).catch((error)=>{
       res.status(200).json(error); 
    });

});

module.exports=router;