const express=require('express');
const app=express();
const path=require('path');

const multer=require('multer');
var passport = require('passport');

const leftpanel=require('./controllers/leftpanel');
const admin=require('./controllers/adminControllers/index');

const mongoose=require('mongoose');
const bodyparser=require('body-parser');
var passport=require('passport');

app.use(passport.initialize());
app.use(passport.session());

var upload = multer({ dest: 'assets/' })

mongoose.connect(
    "mongodb://highdee:aladesiun11@cluster0-shard-00-00-8grvt.mongodb.net:27017,cluster0-shard-00-01-8grvt.mongodb.net:27017,cluster0-shard-00-02-8grvt.mongodb.net:27017/dishingtrump?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true",
    {
         useNewUrlParser: true 
    }
);

app.use(bodyparser.urlencoded({ extended: false}));
app.use(bodyparser.json());

//PREVENTING CORS ERRORS
app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin','*'); 
    res.header('Access-Control-Allow-Headers','Content-Type ,  X-Requested-With , Origin , Authorization , Accept');
    
    if(req.method == 'OPTIONS'){
        res.header('Access-Control-Allow-Methods','GET , POST , DELETE , POST');
        // res.status().json({});
    }
    next();
});


app.use(express.static(path.join(__dirname,'assets/ftpimages/')));
app.use('/button_images',express.static(path.join(__dirname,'assets/buttonimages/')));
app.use('/button_videos',express.static(path.join(__dirname,'assets/buttonvideos/')));
app.use('/button_articleimages',express.static(path.join(__dirname,'assets/buttonarticleimages/')));
app.use('/chatbuttoncontent',express.static(path.join(__dirname,'assets/chatbuttoncontent/')));

// FRONTEND API'S
app.use('/leftButtons',leftpanel);

//ADMIN API'S
app.use('/admin',admin);

var port=process.env.port || 2000;
app.listen(port);
console.log('server live...');    