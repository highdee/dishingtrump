const mongoose=require('mongoose');


const subbuttons=mongoose.Schema({
    '_id':mongoose.Schema.Types.ObjectId,
    'foldername':String,
    'name':Array,
    'Images/Photos':Array,
    'Spreadsheets':Array,
    'PlainText/Document':Array,
    'ParentId':mongoose.Schema.Types.ObjectId,
    'folderid':String,
});

module.exports=mongoose.model('subbuttons',subbuttons);