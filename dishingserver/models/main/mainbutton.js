const mongoose=require('mongoose');


const mainbutton=mongoose.Schema({
    // '_id':mongoose.Schema.Types.ObjectId,
    'folderid':String,
    'title':String, 
    'images':Array,
    'icon':String, 
    'videos':Array,
    'articles':Array,
    'documents':Array,
    'data':Array,
    'publish':Number,
    'date':{type: Date, default: Date.now}
});

module.exports=mongoose.model('mainbutton',mainbutton);