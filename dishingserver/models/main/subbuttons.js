const mongoose=require('mongoose');


const sub_buttons=mongoose.Schema({
    // '_id':mongoose.Schema.Types.ObjectId,
    'title':String, 
    'images':Array,
    'icon':String, 
    'videos':Array,
    'articles':Array,
    'documents':Array,
    'parent_id':{type:mongoose.Schema.Types.ObjectId, ref:'sub_buttons'},
    'folderid':String,
    'data':Array,
    'publish':Number,
    'date':{type: Date, default: Date.now}
});

module.exports=mongoose.model('sub_buttons',sub_buttons);