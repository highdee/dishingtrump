let mongoose=require('mongoose');

let chatButton=mongoose.Schema({
    'title':String, 
    'image':String,
    'imageurl':String,
    'code':String, 
    'video':String, 
    'data':Array,
    'mobile':Object,
    'publish':Number,
    'date':{type: Date, default: Date.now}
});


module.exports=mongoose.model('chatButton',chatButton);